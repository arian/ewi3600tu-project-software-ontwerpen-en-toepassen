#!/bin/bash

for file in `find . -wholename './src/*.java'`
do

	firstline=`head -1 ${file}`

	if [ "$firstline" != "/*" ]; then
	
		
		echo "/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\\__  \\
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\\_/\_\\\____/\____/\_/   \____//____/
*/
`cat ${file}`" > $file

	fi

	whitespace=`cat $file | sed 's/[ \t]*$//'`
	if [ -n "$whitespace" ]; then
		echo "$whitespace" > $file
	fi

done


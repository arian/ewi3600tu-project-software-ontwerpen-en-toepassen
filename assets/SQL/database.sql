CREATE TABLE levels (
	id INTEGER PRIMARY KEY AUTOINCREMENT, 
	leveldata TEXT
);

CREATE TABLE highscores (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "level_id" INTEGER,
    "time" INTEGER NOT NULL,
    "name" VARCHAR,
    FOREIGN KEY (level_id) REFERENCES levels(id)
);

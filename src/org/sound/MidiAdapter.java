/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.sound;

import java.io.File;
import java.io.IOException;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Transmitter;

public class MidiAdapter extends Adapter {

	public MidiAdapter(File file) {
		super(file);
	}

	@Override
	public void play(int options) {

		// can't get it working on Linux :(
		/*
			javax.sound.midi.MidiUnavailableException
			at javax.sound.midi.MidiSystem.getDefaultDeviceWrapper(MidiSystem.java:1078)
			at javax.sound.midi.MidiSystem.getReceiver(MidiSystem.java:240)
			at javax.sound.midi.MidiSystem.getSequencer(MidiSystem.java:442)
			at javax.sound.midi.MidiSystem.getSequencer(MidiSystem.java:348)
			at org.sound.MidiAdapter.play(MidiAdapter.java:31)
			at org.sound.SoundPlayer.run(SoundPlayer.java:45)
		Caused by: java.lang.IllegalArgumentException: Requested device not installed
			at javax.sound.midi.MidiSystem.getDefaultDevice(MidiSystem.java:1130)
			at javax.sound.midi.MidiSystem.getDefaultDeviceWrapper(MidiSystem.java:1076)
			... 5 more
		*/

		try {

			Sequence sequence = MidiSystem.getSequence(file);
			final Sequencer sequencer = MidiSystem.getSequencer();
			sequencer.open();
			sequencer.setSequence(sequence);

			final Synthesizer synthesizer = MidiSystem.getSynthesizer();
			synthesizer.open();
			Receiver synthReceiver = synthesizer.getReceiver();
			Transmitter seqTransmitter = sequencer.getTransmitter();
			seqTransmitter.setReceiver(synthReceiver);

			sequencer.addMetaEventListener(new MetaEventListener() {
				@Override
				public void meta(MetaMessage event) {
					if (event.getType() == 47) {
						sequencer.close();
						synthesizer.close();
					}
				}
			});

			if ((options & SoundPlayer.LOOP) != 0) {
				// sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
			}

			sequencer.start();

		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		} catch (MidiUnavailableException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	@Override
	public void stop() {
	}

	@Override
	public boolean isPlaying() {
		return false;
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.sound;

import java.io.File;

public class SoundPlayer extends Thread {

	public final static int LOOP = 1;

	private File soundFile;
	private Adapter adapter;
	private int options = 0;
	private boolean started = false;

	/**
	 * Create a new player for some sound file
	 * @param file
	 */
	public SoundPlayer(String file) {
		int mid = file.lastIndexOf(".");
		String type = file;
		type = type.substring(mid + 1).toLowerCase();

		soundFile = new File(file);
		if (!soundFile.exists()) {
			System.err.println("Sound file not found: " + file);
			return;
		}

		if ("wav".equals(type)) {
			adapter = new WaveAdapter(soundFile);
		} else if ("mid".equals(type)) {
			adapter = new MidiAdapter(soundFile);
		} else if ("mp3".equals(type)) {
			adapter = new MP3Adapter(soundFile);
		} else {
			System.err.println("Unknown file type " + type);
			return;
		}

	}

	/**
	 * Set the loop options, true for continues looping, false for no looping
	 * Default to false
	 * @param loop
	 * @return this instance
	 */
	public SoundPlayer setLoop(boolean loop) {
		if (loop)
			this.options |= LOOP;
		else
			this.options ^= LOOP;
		return this;
	}

	/**
	 * Start playing the sound
	 * @return this instance
	 */
	public SoundPlayer startPlayer() {
		if (!adapter.isPlaying()) {
			if (!started) {
				started = true;
				this.start();
			} else {
				adapter.play(options);
			}
		}
		return this;
	}

	/**
	 * Stop playing the sound
	 * @return this instance
	 */
	public SoundPlayer stopPlayer() {
		if (adapter.isPlaying()){
			adapter.stop();
		}
		return this;
	}

	/**
	 * @return Whether the sound is currently playing
	 */
	public boolean isPlaying() {
		return adapter.isPlaying();
	}

	@Override
	public void run() {
		adapter.play(options);
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.sound;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class MP3Adapter extends Adapter {

	private boolean isPlaying = false;
	private Player player;

	public MP3Adapter(File file) {
		super(file);
	}

	@Override
	public void play(final int options) {
		if (!isPlaying) {
			isPlaying = true;
			boolean loop = (options & SoundPlayer.LOOP) != 0;
			try {

				do {

					// TODO see if we could reuse the stream
					InputStream stream = new FileInputStream(file);
					player = new Player(stream);
					// player is blocking
					player.play();

				} while (isPlaying && loop);

			} catch (IOException e) {
				e.printStackTrace();
			} catch (JavaLayerException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void stop() {
		if (isPlaying) {
			isPlaying = false;
			if (player != null) {
				player.close();
			}
		}
	}

	@Override
	public boolean isPlaying() {
		return isPlaying;
	}

}

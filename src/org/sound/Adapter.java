/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.sound;

import java.io.File;

public abstract class Adapter {

	protected final File file;

	public Adapter(File file) {
		this.file = file;
	}

	public abstract void play(int options);
	public abstract void stop();

	public abstract boolean isPlaying();

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cuboid;

import javax.vecmath.Vector3f;

import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.cuboid.Cuboid.CuboidHook;

/**
 * Defines the physics model of a Cuboid
 */
public class CuboidRigidBodyHook extends RigidBodyHook implements CuboidHook {

	Cuboid cuboid;

	public CuboidRigidBodyHook(Shape s) {
		super(s);
		cuboid = (Cuboid) shape;
		setSize(cuboid.getSize());
	}

	@Override
	public void setSize(Vector3f v) {
		colShape = cuboid.createCollisionShape();
	}

}

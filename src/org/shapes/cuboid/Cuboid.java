/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cuboid;

import javax.vecmath.Vector3f;

import org.shapes.Hook;
import org.shapes.Shape;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;

/**
 * A Cuboid is some rectangular beam or superset of a cube.
 */
public class Cuboid extends Shape {

	public interface CuboidHook extends Hook {
		public void setSize(Vector3f v);
	}

	private Vector3f size;

	public Cuboid(Vector3f size) {
		this.size = size;
	}

	public Cuboid() {
		this(new Vector3f(4, 8, 5));
	}

	public Cuboid(float x, float y, float z) {
		this(new Vector3f(x, y, z));
	}

	/**
	 * Set the width of the cube and propagate this to the hooks
	 * @param v The new size of the Cuboid
	 */
	public void setSize(Vector3f v) {
		size = v;
		for (Hook hook : hooks) {
			if (hook instanceof CuboidHook) {
				CuboidHook cuboidHook = (CuboidHook) hook;
				cuboidHook.setSize(v);
			}
		}
	}

	public Vector3f getSize() {
		return size;
	}

	@Override
	public CollisionShape createCollisionShape() {
		return new BoxShape(size);
	}

}

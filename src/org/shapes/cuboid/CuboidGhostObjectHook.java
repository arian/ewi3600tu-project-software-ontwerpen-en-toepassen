/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cuboid;

import javax.vecmath.Vector3f;

import org.shapes.GhostObjectHook;
import org.shapes.Shape;
import org.shapes.cuboid.Cuboid.CuboidHook;

/**
 * Defines the ghost object model of a Cube
 */
public class CuboidGhostObjectHook extends GhostObjectHook implements CuboidHook {

	Cuboid cuboid;

	public CuboidGhostObjectHook(Shape s) {
		super(s);
		cuboid = (Cuboid) s;
		setSize(cuboid.getSize());
	}

	@Override
	public void setSize(Vector3f size) {
		colShape = cuboid.createCollisionShape();
	}

}

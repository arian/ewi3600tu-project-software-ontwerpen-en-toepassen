/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cuboid;

import javax.media.opengl.GL;
import javax.vecmath.Vector3f;

import org.shapes.RendererHook;
import org.shapes.cuboid.Cuboid.CuboidHook;

/**
 * This class renders the cuboid with GLUT
 */
public class CuboidRendererHook extends RendererHook implements CuboidHook {

	Cuboid cuboid;

	public CuboidRendererHook(Cuboid s) {
		super(s);
		cuboid = (Cuboid) shape;
	}

	@Override
	public void setSize(Vector3f v){
	}

	// borrowed some code from http://www.dreamincode.net/forums/topic/204553-multiple-instantiation-from-the-same-class/
	@Override
	public void render(GL gl) {

		Vector3f size = cuboid.getSize();
		double width = size.x;
		double height = size.y;
		double depth = size.z;

		gl.glBegin (GL.GL_QUADS);

		gl.glNormal3d(0.0, 0.0, 1.0);
		gl.glVertex3d (-width,  height, depth);
		gl.glVertex3d (-width, -height, depth);
		gl.glVertex3d ( width, -height, depth);
		gl.glVertex3d ( width,  height, depth);

		gl.glNormal3d(1.0,0.0,0.0);
		gl.glVertex3d ( width,  height,  depth);
		gl.glVertex3d ( width, -height,  depth);
		gl.glVertex3d ( width, -height, -depth);
		gl.glVertex3d ( width,  height, -depth);

		gl.glNormal3d(0.0, 1.0, 0.0);
		gl.glVertex3d (-width, height,  depth);
		gl.glVertex3d ( width, height,  depth);
		gl.glVertex3d ( width, height, -depth);
		gl.glVertex3d (-width, height, -depth);

		gl.glNormal3d(0.0, 0.0, -1.0);
		gl.glVertex3d (-width,  height, -depth);
		gl.glVertex3d ( width,  height, -depth);
		gl.glVertex3d ( width, -height, -depth);
		gl.glVertex3d (-width, -height, -depth);

		gl.glNormal3d(0.0, -1.0, 0.0);
		gl.glVertex3d (-width, -height, -depth);
		gl.glVertex3d ( width, -height, -depth);
		gl.glVertex3d ( width, -height,  depth);
		gl.glVertex3d (-width, -height,  depth);

		gl.glNormal3d(-1.0, 0.0, 0.0);
		gl.glVertex3d (-width,  height,  depth);
		gl.glVertex3d (-width,  height, -depth);
		gl.glVertex3d (-width, -height, -depth);
		gl.glVertex3d (-width, -height,  depth);

		gl.glEnd ();

	}

}

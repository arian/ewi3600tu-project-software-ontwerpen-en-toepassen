/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cuboid;

import javax.media.opengl.GL;
import javax.vecmath.Vector3f;

import org.shapes.RendererHook;
import org.shapes.cuboid.Cuboid.CuboidHook;

import com.sun.opengl.util.GLUT;

/**
 * This class renders the cuboid with GLUT
 */
public class CheckpointRendererHook extends RendererHook implements CuboidHook {

	Cuboid cuboid;

	public CheckpointRendererHook(Cuboid s) {
		super(s);
		cuboid = (Cuboid) shape;
	}

	@Override
	public void setSize(Vector3f v){
	}

	@Override
	public void render(GL gl) {

		GLUT glut = new GLUT();
		glut.glutWireDodecahedron();
		glut.glutSolidIcosahedron();

	}

}

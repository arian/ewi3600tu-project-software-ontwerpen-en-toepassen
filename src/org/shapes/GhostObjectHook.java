/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes;

import java.util.HashMap;

import javax.media.opengl.GL;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import com.bulletphysics.collision.dispatch.CollisionFlags;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.GhostObject;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.linearmath.Transform;

/**
 * Defines the (ghost object) physics model of a shape
 * Usually the player can fly through the ghost objects, which can be used as
 * a Pick-up for extra features
 */
public abstract class GhostObjectHook implements Hook {

	public interface CollisionCallback {
		public void callback(GhostObjectHook ghost, Shape shape,
				GhostObject ghostObject, CollisionObject rb);
	}

	private Shape shape;
	private GhostObject ghost;
	private DynamicsWorld dynamicsWorld;
	private int collisionFlags = CollisionFlags.NO_CONTACT_RESPONSE;
	protected CollisionShape colShape;

	private final HashMap<CollisionObject, CollisionCallback> callbacks
		= new HashMap<CollisionObject, CollisionCallback>();

	public GhostObjectHook(Shape s) {
		shape = s;
	}

	/**
	 * Add a callback when a collision object collides with this ghost object
	 * @param obj
	 * @param callback
	 */
	public void addCallback(CollisionObject obj, CollisionCallback callback) {
		callbacks.put(obj, callback);
	}

	/**
	 * Add a callback when a collision object collides with this ghost object
	 * @param rb
	 * @param callback
	 */
	public void addCallback(RigidBodyHook rb, CollisionCallback callback) {
		callbacks.put(rb.getBody(), callback);
	}

	/**
	 * Removes the collision callback between the ghost and this collision object
	 * @param obj
	 */
	public void removeCallback(CollisionObject obj) {
		callbacks.remove(obj);
	}

	/**
	 * Removes the collision callback between the ghost and this collision object
	 * @param rb
	 */
	public void removeCallback(RigidBodyHook rb) {
		callbacks.remove(rb.getBody());
	}

	/**
	 * Set the dynamics world the ghost should be added to
	 * @param dynamicsWorld
	 */
	public void setDynamicsWorld(DynamicsWorld dynamicsWorld) {
		this.dynamicsWorld = dynamicsWorld;
	}

	/**
	 * Get the dynamics World where the ghost was added to.
	 * @return get the dynamicsWorld object
	 */
	public DynamicsWorld getDynamicsWorld() {
		return dynamicsWorld;
	}

	/**
	 * Set CollisionFlags
	 * This defines how other objects collide with this ghost
	 * @return the collisionFlags as BulletPhysics defined them
	 */
	public int getCollisionFlags() {
		return collisionFlags;
	}

	/**
	 * Set CollisionFlags
	 * This defines how other objects collide with this ghost
	 */
	public void setCollisionFlags(int collisionFlags) {
		this.collisionFlags  = collisionFlags;
	}

	@Override
	public void display(GL gl) throws ShapeException {

		if (ghost == null) {
			createGhostObject();
		}

		for (int i = 0; i < ghost.getNumOverlappingObjects(); i++) {

			CollisionObject co = ghost.getOverlappingObject(i);
			CollisionCallback cc = callbacks.get(co);
			if (cc != null) {
				cc.callback(this, shape, ghost, co);
			}

		}

	}

	public void createGhostObject() {
		ghost = new GhostObject();
		ghost.setCollisionShape(colShape);
		ghost.setCollisionFlags(collisionFlags);
		addToDynamicsWorld();
	}

	private void addToDynamicsWorld() {
		if (ghost != null) {
			dynamicsWorld.removeCollisionObject(ghost);
		}

		// Create Dynamic Objects
		Transform trans = new Transform();
		trans.setIdentity();
		trans.origin.set(shape.getPosition());
		trans.setRotation(shape.getRotation());

		ghost.setWorldTransform(trans);

		dynamicsWorld.addCollisionObject(ghost);
	}

	@Override
	public void setPosition(Vector3f p) {
		if (ghost != null) {
			addToDynamicsWorld();
		}
	}

	@Override
	public void setRotation(Quat4f r) {
		if (ghost != null) {
			addToDynamicsWorld();
		}
	}

}

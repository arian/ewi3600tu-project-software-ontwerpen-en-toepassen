/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.ship;

import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.ship.Ship.ShipHook;

/**
 * Defines the physics model of a Cube
 */
public class ShipRigidBodyHook extends RigidBodyHook implements ShipHook {

	Ship ship;

	public ShipRigidBodyHook(Shape s) {
		super(s);
		ship = (Ship) shape;
		setSize(ship.getSize());
	}

	@Override
	public void setSize(double size) {
		colShape = ship.createCollisionShape();
	}

}

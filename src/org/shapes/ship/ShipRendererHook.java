/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.ship;

import javax.media.opengl.GL;

import org.TextureManager;
import org.shapes.RendererHook;
import org.shapes.ship.Ship.ShipHook;

/**
 * This class renders the cube with GLUT
 */
public class ShipRendererHook extends RendererHook implements ShipHook {

	Ship ship;
	String texture = "steel.bmp";

	public ShipRendererHook(Ship s) {
		super(s);
		ship = (Ship) shape;
	}

	@Override
	public void setSize(double s) {
		// empty method, is not necessary for the renderer because the
		// size property is used in the display method
	}

	public void dop(GL gl, double x, double y, double z, double radius, int res, double hoekbocht){
		// doppen op de tube
		double y1, z1, y2, z2;
		double hoek = 0;

		gl.glBindTexture(GL.GL_TEXTURE_2D, TextureManager.getInstance(gl).getTexture(texture));
		gl.glEnable(GL.GL_TEXTURE_2D);

		gl.glNormal3d(-1, 0, 0);
		gl.glBegin(GL.GL_TRIANGLE_FAN);

		for (int i = 0; i < res; i++) {

			// gare manier om textures in een driehoekvorm te krijgen. (werkt niet).
			// gl.glTexCoord2d(0.5, 0.5);
			// gl.glTexCoord2d(0.5, 0.5);	gl.glVertex3d(x,y,z);

			// coordinaten van de buitenste baan
	    	y1 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z1 = Math.cos(Math.toRadians(hoek)) * radius;

	    	hoek += hoekbocht / res;

	    	// coordinaten van de buitenste baan
	    	y2 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z2 = Math.cos(Math.toRadians(hoek)) * radius;

	    	gl.glTexCoord2d(0.5 + (y1 / radius), 0.5 + (z1 / radius));
	    	gl.glVertex3d(x, y + y1, z + z1);

	    	gl.glTexCoord2d(0.5 + (y2 / radius), 0.5 + (z1 / radius));
	    	gl.glVertex3d(x, y + y2, z + z2);

		}

		gl.glEnd();
		gl.glDisable(GL.GL_TEXTURE_2D);
	}

	public void tube(GL gl, double beginx, double beginy, double beginz, double radius, double lengte){
		double hoek = 0;
		double hoekbocht = 360;
		double y1,z1;
		int res = 48;
		double coord;

		gl.glBindTexture(GL.GL_TEXTURE_2D, TextureManager.getInstance(gl).getTexture(texture));
		gl.glEnable(GL.GL_TEXTURE_2D);

		gl.glBegin(GL.GL_QUADS);

		for (int i = 0; i < res; i++) {
			gl.glNormal3d(0, Math.sin(Math.toRadians(hoek)), Math.cos(Math.toRadians(hoek)));

			// coordinaten van de buitenste baan
	    	y1 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z1 = Math.cos(Math.toRadians(hoek)) * radius;

	    	coord = (double) i / (double) res;

	    	gl.glTexCoord2d( coord, 1);
	    	gl.glVertex3d(beginx, beginy + y1, beginz + z1);

	    	gl.glTexCoord2d( coord, 0);
	    	gl.glVertex3d(beginx + lengte, beginy + y1, beginz + z1);

	    	hoek += hoekbocht / res;

	    	// coordinaten van de buitenste baan
	    	y1 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z1 = Math.cos(Math.toRadians(hoek)) * radius;

	    	coord = (double) (i + 1) / (double) res;

	    	gl.glTexCoord2d(coord, 0);
	    	gl.glVertex3d(beginx + lengte, beginy + y1, beginz + z1);

	    	gl.glTexCoord2d(coord, 1);
	    	gl.glVertex3d(beginx, beginy + y1, beginz + z1);
		}

		gl.glEnd();
		gl.glDisable(GL.GL_TEXTURE_2D);

		// teken de vlakke kant van de cylinder
		dop(gl, beginx, beginy, beginz, radius, res, hoekbocht);
	}

	public void tubeTip(GL gl, double beginx, double beginy,double beginz, double radius, double lengte, double lengtepunt){
		double hoek = 0;
		double hoekbocht = 360;
		double y1,z1,y2,z2;
		int res = 48;
		double xNormaal = Math.tan(radius / lengtepunt);

		// teken de cylinder
		tube(gl, beginx, beginy,beginz, radius, lengte);

		gl.glBegin(GL.GL_TRIANGLE_FAN);
		gl.glVertex3d(beginx + lengte + lengtepunt, beginy, beginz);

		// teken de punt van de cylinder
		for (int i = 0; i < res; i++) {

			gl.glNormal3d(xNormaal, Math.sin(Math.toRadians(hoek)), Math.cos(Math.toRadians(hoek)));

			// coordinaten van de buitenste baan
	    	y1 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z1 = Math.cos(Math.toRadians(hoek)) * radius;

	    	hoek += hoekbocht / res;

	    	// coordinaten van de buitenste baan
	    	y2 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z2 = Math.cos(Math.toRadians(hoek)) * radius;

	    	gl.glVertex3d(beginx + lengte, beginy + y2, beginz + z2);
	    	gl.glVertex3d(beginx + lengte, beginy + y1, beginz + z1);
		}
		gl.glEnd();
	}

	@Override
	public void render(GL gl) {
		// teken de cylinder
		tubeTip(gl, -1.0, 0, 0, 0.6, 2.0, 0.5);
	}

}

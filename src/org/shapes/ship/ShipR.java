/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.ship;

/**
 * A cube with a CubeRendererHook attached
 */
public class ShipR extends Ship {

	public ShipR() {
		super();
		addHook(new ShipRendererHook(this));
	}

}

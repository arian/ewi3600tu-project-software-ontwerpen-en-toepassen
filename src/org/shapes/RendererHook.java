/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes;

import javax.media.opengl.GL;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import org.util.Time;

/**
 * Abstract class for rendering shape hooks.
 * By extending this class and implementing the render method, the shape will
 * be rendered at the right position and with the right rotation
 */
public abstract class RendererHook implements Hook {

	protected float[] color = { 0.5f, 0.0f, 0.7f, 1.0f };

	protected Shape shape;

	protected Vector4f oscillation;
	protected long startTime = Time.getMillis();

	public RendererHook(Shape s) {
		shape = s;
	}

	/**
	 * Set the color of shape
	 * @param color
	 */
	public void setColor(float[] color) {
		this.color = color;
	}

	/**
	 * Set a oscillation effect. The 4th value in the vector is the
	 * oscillation-time in seconds
	 * @param oscillation
	 */
	public void setOscillation(Vector4f oscillation) {
		this.oscillation = oscillation;
	}

	/**
	 * Get the oscillation. The 4th value in the vector is the
	 * oscillation-time. If the value is null, the shape will not oscillate
	 * @return oscillation vector
	 */
	public Vector4f getOscillation() {
		return oscillation;
	}

	@Override
	public void display(GL gl) {

		// Setting the wall color and material.
		gl.glMaterialfv( GL.GL_FRONT, GL.GL_DIFFUSE, color, 0);

		gl.glPushMatrix();

		// Set position
		Vector3f pos = shape.getPosition();
		Vector3f center = shape.getCenterOfBody();
		gl.glTranslatef(pos.x - center.x, pos.y - center.y, pos.z - center.z);

		// oscillation
		if (oscillation != null) {
			float time = ((float) (Time.getMillis() - startTime)) / 1000;
			float scale = (float) Math.sin(oscillation.z * time);
			gl.glTranslatef(oscillation.x * scale, oscillation.y * scale, oscillation.z * scale);
		}

		// rotation, from Quaternions rotation to AxisAngle rotation
		AxisAngle4f rot = new AxisAngle4f();
		rot.set(shape.getRotation());

		gl.glRotatef((float) (rot.angle * 180 / Math.PI), rot.x, rot.y, rot.z);

		// Call render method
		render(gl);

		gl.glPopMatrix();

	}

	protected abstract void render(GL gl);

	// Unused methods

	@Override
	public void setPosition(Vector3f p) {
	}

	@Override
	public void setRotation(Quat4f r) {
	}


}

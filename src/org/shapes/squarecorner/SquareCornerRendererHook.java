/*
 *  _____ ____  ____  _     ____  ____ ____ftw.
 * /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
 * | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
 * | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
 * \____\\_/\_\\____/\____/\_/   \____//____/
 */
package org.shapes.squarecorner;

import javax.media.opengl.GL;

import org.TextureManager;
import org.shapes.RendererHook;
import org.shapes.squarecorner.SquareCorner.SquareCornerHook;

/**
 * This class renders the Square Corner with GLUT
 */
public class SquareCornerRendererHook extends RendererHook implements
		SquareCornerHook {

	protected SquareCorner squarecorner;

	private final String texture = "futurefloor.jpg";
	// resolution of the texture
	private int res;

	public SquareCornerRendererHook(SquareCorner s) {
		super(s);
		squarecorner = (SquareCorner) shape;
		res = squarecorner.getRes();
	}

	@Override
	public void setSize(double s) {
		// empty method, is not necessary for the renderer because the
		// size property is used in the display method
	}

	/**
	 * Draws the outer curve
	 * @param gl
	 * @param x
	 * @param y1
	 * @param z1
	 * @param y2
	 * @param z2
	 * @param size
	 * @param i
	 * @param length
	 */
	public void drawPlaneOut(GL gl, double x, double y1, double z1, double y2,
			double z2, double size, int i, double length) {
		// draw ceiling, i.e. the outer curve
		// TexCoord2d are defined here. y between 1 and 0. X is divided in many
		// small pieces
		float xa = 0;
		float ya = ((float) i / (float) res) / ((float) (size / length));
		float xb = 1;
		float yb = ((float) (i + 1) / (float) res) / ((float) (size / length));

		// variables for the normal vectors
		double /* a = (y2 - y1) + y1 + 50 */a = y2 + (size * 1.5);
		double /* b = (z2 - z1) + z1 + 50 */b = z2 + (size * 1.5);
		double uni = Math.hypot(a, b);

		gl.glNormal3d(0.0, -a / uni, -b / uni);

		gl.glBegin(GL.GL_QUADS);

		gl.glTexCoord2d(xa, ya);
		gl.glVertex3d(x - size, y1, z1);

		gl.glTexCoord2d(xa, yb);
		gl.glVertex3d(x - size, y2, z2);

		gl.glTexCoord2d(xb, yb);
		gl.glVertex3d(x, y2, z2);

		gl.glTexCoord2d(xb, ya);
		gl.glVertex3d(x, y1, z1);

		gl.glEnd();

	}

	/**
	 * Draws the inner curve
	 * @param gl
	 * @param x
	 * @param y1
	 * @param z1
	 * @param y2
	 * @param z2
	 * @param size
	 * @param i
	 * @param length
	 */
	public void drawPlaneIn(GL gl, double x, double y1, double z1, double y2,
			double z2, double size, int i, double length) {
		// draw floor, i.e. the inner curve
		float xa = 0;
		float ya = ((float) i / (float) res) / ((float) (size / length));
		float xb = 1;
		float yb = ((float) (i + 1) / (float) res) / ((float) (size / length));

		// variables for the normal vectors
		double /* a = (y2 - y1) + y1 + 50 */a = y2 + (size * 1.5);
		double /* b = (z2 - z1) + z1 + 50 */b = z2 + (size * 1.5);
		double uni = Math.hypot(a, b);

		gl.glNormal3d(0.0, a / uni, b / uni);

		gl.glBegin(GL.GL_QUADS);

		gl.glTexCoord2d(xb, yb);
		gl.glVertex3d(x, y2, z2);

		gl.glTexCoord2d(xb, ya);
		gl.glVertex3d(x, y1, z1);

		gl.glTexCoord2d(xa, ya);
		gl.glVertex3d(x + size, y1, z1);

		gl.glTexCoord2d(xa, yb);
		gl.glVertex3d(x + size, y2, z2);

		gl.glEnd();
	}

	/**
	 * Draws the side planes of the tube
	 * @param gl
	 * @param x
	 * @param y1
	 * @param z1
	 * @param y2
	 * @param z2
	 * @param y3
	 * @param z3
	 * @param y4
	 * @param z4
	 * @param i
	 * @param lengthOut
	 * @param size
	 */
	public void drawPlane(GL gl, double x, double y1, double z1, double y2,
			double z2, double y3, double z3, double y4, double z4, int i,
			double lengthOut, double size) {

		gl.glBegin(GL.GL_QUADS);

		float xa = 0;
		float ya = ((float) i / (float) res) / ((float) (size / lengthOut));
		float xb = 1;
		float yb = ((float) (i + 1) / (float) res)
				/ ((float) (size / lengthOut));

		gl.glTexCoord2d(ya, xa);
		gl.glVertex3d(x, y1, z1);

		gl.glTexCoord2d(ya, xb);
		gl.glVertex3d(x, y3, z3);

		gl.glTexCoord2d(yb, xb);
		gl.glVertex3d(x, y4, z4);

		gl.glTexCoord2d(yb, xa);
		gl.glVertex3d(x, y2, z2);

		gl.glEnd();
	}

	@Override
	public void render(GL gl) {

		// shape variables
		double size = squarecorner.getSize(); // dimensions of the shape; b*h
		int res = 48; // resolution of the curve (number of polygons)
		double outerRadius = size * (2.0 / 3.0);
		double innerRadius = size * (1.0 / 3.0); // the radius of the corner
		double hoekbocht = 90; // the angle of the corner
		double y1, z1, y2, z2, y3, z3, y4, z4;
		double angle = 0;
		double shift = size / 2.0;

		double hbottom = innerRadius - shift; // trucje =)
		double htop = outerRadius - shift; // trucje =)

		double lengthOut = outerRadius * Math.PI * 0.5;
		double lengthIn = innerRadius * Math.PI * 0.5;
		res = squarecorner.getRes();

		// Set the materials used by the floor.
		float wallColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, wallColor, 0);

		// texture enabling and binding
		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glBindTexture(GL.GL_TEXTURE_2D, TextureManager.getInstance(gl)
				.getTexture(texture));

		// draw the corner
		for (int i = 0; i < res; i++) {

			y1 = (Math.sin(Math.toRadians(angle)) * outerRadius) - shift;
			z1 = (Math.cos(Math.toRadians(angle)) * outerRadius) - shift;
			y3 = (Math.sin(Math.toRadians(angle)) * innerRadius) - shift;
			z3 = (Math.cos(Math.toRadians(angle)) * innerRadius) - shift;

			angle += hoekbocht / res;

			y2 = (Math.sin(Math.toRadians(angle)) * outerRadius) - shift;
			z2 = (Math.cos(Math.toRadians(angle)) * outerRadius) - shift;

			y4 = (Math.sin(Math.toRadians(angle)) * innerRadius) - shift;
			z4 = (Math.cos(Math.toRadians(angle)) * innerRadius) - shift;

			drawPlaneOut(gl, htop, y1, z1, y2, z2, size / 3.0, i, lengthOut);

			gl.glNormal3d(1.0, 0.0, 0.0);
			drawPlane(gl, hbottom, y1, z1, y2, z2, y3, z3, y4, z4, i, lengthOut, size / 3.0);

			gl.glNormal3d(-1.0, 0.0, 0.0);
			drawPlane(gl, htop, y3, z3, y4, z4, y1, z1, y2, z2, i, lengthOut, size / 3.0);

			drawPlaneIn(gl, hbottom, y3, z3, y4, z4, size / 3.0, i, lengthIn);

		}

		gl.glDisable(GL.GL_TEXTURE_2D);
	}

}

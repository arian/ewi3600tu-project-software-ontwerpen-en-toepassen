/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.squarecorner;

//import javax.media.opengl.GL;
import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.squarecorner.SquareCorner.SquareCornerHook;

/**
 * Defines the physics model of a Square Corner
 */
public class SquareCornerRigidBodyHook extends RigidBodyHook implements SquareCornerHook {

	protected SquareCorner squarecorner;

	public SquareCornerRigidBodyHook(Shape s) {
		super(s);
		squarecorner = (SquareCorner) shape;
		setSize(squarecorner.getSize());
	}

	@Override
	public void setSize(double size) {
		colShape = squarecorner.createCollisionShape();
	}

}

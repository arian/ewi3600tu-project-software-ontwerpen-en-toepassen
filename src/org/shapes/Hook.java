/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes;

import javax.media.opengl.GL;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

/**
 * A hook can be added to a shape. The shape will call the added hooks
 * with the display method. In the display method a hook can do interesting
 * things, like rendering the shapes
 */
public interface Hook {

	public void display(GL gl) throws ShapeException;

	public void setPosition(Vector3f p);

	public void setRotation(Quat4f r);

}

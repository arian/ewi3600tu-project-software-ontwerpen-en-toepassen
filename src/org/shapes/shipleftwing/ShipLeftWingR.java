/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.shipleftwing;

/**
 * A cube with a CubeRendererHook attached
 */
public class ShipLeftWingR extends ShipLeftWing {

	public ShipLeftWingR() {
		super();
		addHook(new ShipLeftWingRendererHook(this));
	}

}
/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/

// TODO I believe this package is obsolete!
package org.shapes.shipleftwing;

import javax.vecmath.Vector3f;

import org.shapes.Hook;
import org.shapes.Shape;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;

/**
 * Not used yet, it would look like some sort of rocket thing...
 */
public class ShipLeftWing extends Shape {

	public interface ShipLeftWingHook extends Hook {
		public void setSize(double s);
	}

	private double size = 5;

	public double getSize() {
		return size;
	}

	/**
	 * Set the size of the cube and propagate this to the hooks
	 * @param s
	 */
	public void setSize(double s) {
		size = s;
		for (Hook hook : hooks) {
			if (hook instanceof ShipLeftWingHook) {
				ShipLeftWingHook shipleftwingHook = (ShipLeftWingHook) hook;
				shipleftwingHook.setSize(s);
			}
		}
	}

	@Override
	public CollisionShape createCollisionShape() {
		return new BoxShape(new Vector3f(
				0.5f,
				1.0f,
				0.15f
		));
	}

}

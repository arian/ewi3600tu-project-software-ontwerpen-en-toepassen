/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.shipleftwing;

import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.shipleftwing.ShipLeftWing.ShipLeftWingHook;

/**
 * Defines the physics model of a Cube
 */
public class ShipLeftWingRigidBodyHook extends RigidBodyHook implements ShipLeftWingHook {

	ShipLeftWing shiplw;

	public ShipLeftWingRigidBodyHook(Shape s) {
		super(s);
		shiplw = (ShipLeftWing) shape;
		setSize(shiplw.getSize());
	}

	@Override
	public void setSize(double size) {
		colShape = shiplw.createCollisionShape();
	}

}

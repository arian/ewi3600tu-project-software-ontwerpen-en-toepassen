/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.sphere;

import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.sphere.Sphere.SphereHook;

/**
 * Defines the physics model of a Cube
 */
public class SphereRigidBodyHook extends RigidBodyHook implements SphereHook {

	Sphere shpere;

	public SphereRigidBodyHook(Shape s) {
		super(s);
		shpere = (Sphere) shape;
		setRadius(shpere.getRadius());
	}

	@Override
	public void setRadius(double r) {
		colShape = shpere.createCollisionShape();
	}

}

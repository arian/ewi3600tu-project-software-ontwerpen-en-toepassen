/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.sphere;

/**
 * A cube with a CubeRendererHook attached
 */
public class SphereR extends Sphere {

	public SphereR(float radius) {
		super();
		setRadius(radius);
		addHook(new SphereRendererHook(this));
	}

}

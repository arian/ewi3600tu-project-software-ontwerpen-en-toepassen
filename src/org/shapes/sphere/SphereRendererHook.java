/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.sphere;

import javax.media.opengl.GL;

import org.shapes.RendererHook;
import org.shapes.sphere.Sphere.SphereHook;

import com.sun.opengl.util.GLUT;

/**
 * This class renders the cube with GLUT
 */
public class SphereRendererHook extends RendererHook implements SphereHook {

	Sphere sphere;

	public SphereRendererHook(Sphere s) {
		super(s);
		sphere = (Sphere) shape;
	}

	@Override
	public void setRadius(double r) {
		// empty method, is not necessary for the renderer because the
		// radius property is used in the display method
	}

	/**
	 * http://www.opengl.org/documentation/specs/glut/spec3/node82.html
	 */
	@Override
	public void render(GL gl) {

		GLUT glut = new GLUT();
		glut.glutSolidSphere(sphere.getRadius(), sphere.getSlices(), sphere.getStacks());
	}

}

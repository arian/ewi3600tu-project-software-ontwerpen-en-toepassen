/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.sphere;

import org.shapes.Hook;
import org.shapes.Shape;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.SphereShape;

public class Sphere extends Shape {

	public interface SphereHook extends Hook {
		public void setRadius(double s);
	}

	// http://www.opengl.org/documentation/specs/glut/spec3/node81.html
	protected float radius = 3;
	protected int slices = 20, stacks = 20;

	public float getRadius() {
		return radius;
	}

	/**
	 * Set the radius of the sphere and propagate this to the hooks
	 * @param r
	 */
	public void setRadius(float r) {
		radius = r;
		for (Hook hook : hooks) {
			if (hook instanceof SphereHook) {
				SphereHook sphereHook = (SphereHook) hook;
				sphereHook.setRadius(r);
			}
		}
	}

	public int getSlices() {
		return slices;
	}

	public void setSlices(int slices) {
		this.slices = slices;
	}

	public int getStacks() {
		return stacks;
	}

	public void setStacks(int stacks) {
		this.stacks = stacks;
	}


	@Override
	public CollisionShape createCollisionShape() {
		return new SphereShape(radius);
	}

}

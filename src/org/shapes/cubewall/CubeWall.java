/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cubewall;

import javax.media.opengl.GL;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import org.shapes.Shape;
import org.shapes.cube.Cube;
import org.shapes.cube.CubeRendererHook;
import org.shapes.cube.CubeRigidBodyHook;
import org.util.MathUtil;

import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.linearmath.Transform;

/**
 * The CubeWall is a wall of many smaller cubes
 */
public class CubeWall extends Shape {

	private Cube[] cubes;
	private CubeRigidBodyHook[] crbhs;
	private float mass = 0;
	private Vector4f size;
	private float[] color = {0.1f, 0.1f, 0.1f, 1};
	private float[] sizeRandomBounds = null;

	/**
	 * Get the size of the wall.
	 * @return The last 'w' property of the Vector4f is the cube size
	 */
	public Vector4f getSize() {
		return size;
	}

	/**
	 * Set the size. The last, w, value of the Vector4f is the cube size
	 */
	public void setSize(Vector4f size) {
		this.size = size;
	}

	/**
	 * Get the mass of each individual cube
	 * @return the mass of each block
	 */
	public float getMass() {
		return mass;
	}

	/**
	 * Set the mass of each individual cube
	 */
	public void setMass(float mass) {
		if (crbhs != null) {
			for (CubeRigidBodyHook cube : crbhs) {
				cube.setMass(mass);
			}
		}
		this.mass = mass;
	}

	/**
	 * Set the color of the cubes
	 * @param color
	 */
	public void setColor(float[] color) {
		this.color = color;
	}

	/**
	 * Sets the lower and upper as values between 0 and 1
	 */
	public void setCubeSizeRandomBounds(float[] bounds) {
		this.sizeRandomBounds = bounds;
	}

	/**
	 * Sets the lower and upper as values between 0 and 1
	 * @param lower
	 * @param upper
	 */
	public void setCubeSizeRandomBounds(float lower, float upper) {
		this.sizeRandomBounds = new float[]{lower, upper};
	}

	@Override
	public void setPosition(Vector3f pos) {
		if (cubes != null) {
			Vector3f relative = new Vector3f();
			Vector3f newPos = new Vector3f();
			for (Cube cube : cubes) {
				relative.sub(position, cube.getPosition());
				newPos.add(pos, relative);
				cube.setPosition(newPos);
			}
		}
		super.setPosition(pos);
	}

	@Override
	public void setRotation(Quat4f rot) {
		if (cubes != null) {
			// Rotate the whole wall, this means that some cubes will be moved
			// to another position. This is easily done with a transformation
			Vector3f relative = new Vector3f();
			Vector3f newPos = new Vector3f();
			Vector3f pos = getPosition();
			Transform trans = new Transform();
			trans.setRotation(rot);
			for (Cube cube : cubes) {
				relative.sub(position, cube.getPosition());
				trans.transform(relative);
				newPos.add(pos, relative);
				cube.setPosition(newPos);
				cube.setRotation(rot);
			}
		}
		super.setRotation(rot);
	}

	public void build(DynamicsWorld dynamicsWorld) {

		float cubeSize = size.w;
		int cubesX = (int) (size.x / cubeSize);
		int cubesY = (int) (size.y / cubeSize);
		int cubesZ = (int) (size.z / cubeSize);

		cubes = new Cube[cubesX * cubesY * cubesZ];
		crbhs = new CubeRigidBodyHook[cubesX * cubesY * cubesZ];

		Vector3f locPos = new Vector3f();

		Transform trans = new Transform();
		trans.setIdentity();
		trans.setRotation(rotation);

		int i = 0;

		for (int x = 0; x < cubesX; x++) {
			locPos.x = ((cubeSize * (x - cubesX / 2)) + cubeSize / 2);
			for (int y = 0; y < cubesY; y++) {
				locPos.y = ((cubeSize * (y - cubesY / 2)) + cubeSize / 2);
				for (int z = 0; z < cubesZ; z++) {
					locPos.z = ((cubeSize * (z - cubesZ / 2)) + cubeSize / 2);

					Cube cube = new Cube();

					float thisCubeSize = cubeSize;
					if (sizeRandomBounds != null) {
						thisCubeSize *= MathUtil.randomBetween(sizeRandomBounds);
					}
					cube.setSize(thisCubeSize);

					Vector3f pos = (Vector3f) locPos.clone();
					trans.transform(pos);
					// World position
					pos.add(position);

					cube.setPosition(pos);
					cube.setRotation(rotation);

					CubeRendererHook crh = new CubeRendererHook(cube);
					crh.setColor(color);
					crh.setTexture("target.bmp");
					crh.setTexPosX(y + 1);
					crh.setTexPosY(z + 1);
					crh.setNumberOfTextureBlocks(cubesY);
					cube.addHook(crh);

					CubeRigidBodyHook crbh = new CubeRigidBodyHook(cube);
					crbh.setMass(mass);
					crbh.setDynamicsWorld(dynamicsWorld);
					cube.addHook(crbh);

					cubes[i] = cube;
					crbhs[i++] = crbh;

				}
			}
		}

	}

	/**
	 * Display all the cubes!
	 */
	@Override
	public void display(GL gl, int dt) {
		super.display(gl, dt);
		if (cubes != null) {
			for (Cube cube : cubes) {
				cube.display(gl, dt);
			}
		}
	}

}

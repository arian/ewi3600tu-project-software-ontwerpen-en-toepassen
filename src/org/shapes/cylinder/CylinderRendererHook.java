/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cylinder;

import javax.media.opengl.GL;

import org.shapes.RendererHook;
import org.shapes.cylinder.Cylinder.CylinderHook;

import com.sun.opengl.util.GLUT;

/**
 * This class renders the cube with GLUT
 */
public class CylinderRendererHook extends RendererHook implements CylinderHook {

	Cylinder cylinder;

	protected int slices = 20, stacks = 20;

	public CylinderRendererHook(Cylinder s) {
		super(s);
		cylinder = (Cylinder) shape;
	}

	@Override
	public void setRadius(double r) {
		// empty method, is not necessary for the renderer because the
		// radius property is used in the display method
	}

	@Override
	public void setHeight(double h) {
	}


	@Override
	public void render(GL gl) {

		// TODO glutSolidCylinder doesn't draw the base plane
		GLUT glut = new GLUT();
		glut.glutSolidCylinder(cylinder.getRadius(), cylinder.getHeight(), slices, stacks);

	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cylinder;

import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.cylinder.Cylinder.CylinderHook;

/**
 * Defines the physics model of a Cube
 */
public class CylinderRigidBodyHook extends RigidBodyHook implements CylinderHook {

	Cylinder cylinder;

	public CylinderRigidBodyHook(Shape s) {
		super(s);
		cylinder = (Cylinder) shape;
		colShape = cylinder.createCollisionShape();
	}

	@Override
	public void setRadius(double r) {
		colShape = cylinder.createCollisionShape();
	}

	@Override
	public void setHeight(double h) {
		colShape = cylinder.createCollisionShape();
	}

}

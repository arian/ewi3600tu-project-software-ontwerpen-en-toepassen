/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cylinder;

/**
 * A cube with a CubeRendererHook attached
 */
public class CylinderR extends Cylinder {

	public CylinderR() {
		super();
		addHook(new CylinderRendererHook(this));
	}

	public CylinderR(double r) {
		super(r);
		addHook(new CylinderRendererHook(this));
	}

	public CylinderR(double r, double h) {
		super(r, h);
		addHook(new CylinderRendererHook(this));
	}

}

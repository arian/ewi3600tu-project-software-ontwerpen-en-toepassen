/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cylinder;

import javax.vecmath.Vector3f;

import org.shapes.Hook;
import org.shapes.Shape;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CylinderShapeZ;

public class Cylinder extends Shape {

	public interface CylinderHook extends Hook {
		public void setRadius(double r);
		public void setHeight(double h);
	}

	private double radius = 1;
	private double height = 1;

	public Cylinder() {
	}

	public Cylinder(double r) {
		setRadius(r);
	}

	public Cylinder(double r, double h) {
		setRadius(r);
		setHeight(h);
	}

	public double getRadius() {
		return radius;
	}

	/**
	 * Set the radius of the cylinder and propagate this to the hooks
	 * @param r
	 */
	public void setRadius(double r) {
		radius = r;
		for (Hook hook : hooks) {
			if (hook instanceof CylinderHook) {
				CylinderHook cylinderHook = (CylinderHook) hook;
				cylinderHook.setRadius(r);
			}
		}
	}

	public double getHeight() {
		return height;
	}

	/**
	 * Set the height of the cylinder and propagate this to the hooks
	 * @param h
	 */
	public void setHeight(double h) {
		for (Hook hook : hooks) {
			if (hook instanceof CylinderHook) {
				CylinderHook cylinderHook = (CylinderHook) hook;
				cylinderHook.setRadius(h);
			}
		}
		height = h;
	}

	@Override
	public CollisionShape createCollisionShape() {
		return new CylinderShapeZ(new Vector3f((float) height, (float) radius, (float) radius));
	}

}

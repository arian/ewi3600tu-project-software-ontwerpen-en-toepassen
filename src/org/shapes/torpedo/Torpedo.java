/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.torpedo;

import java.util.Timer;
import java.util.TimerTask;

import javax.media.opengl.GL;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import org.VisibleObject;
import org.VisibleObjectList;
import org.gm.game.player.Player;
import org.shapes.RendererHook;
import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.cuboid.Cuboid;
import org.shapes.cuboid.CuboidRendererHook;
import org.shapes.cuboid.CuboidRigidBodyHook;

import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;

/**
 * Torpedo shape
 */
public class Torpedo extends Shape {

	private RigidBodyHook trbh;
	private RendererHook trh;
	private Player player;
	private boolean isFired = false;

	private final float maxSpeed = 75; // 50
	private final float absAcceleration = 500; // 200

	private final Cuboid torpedo;
	private VisibleObjectList<VisibleObject> myList;
	private DynamicsWorld dynamicsWorld;

	public Torpedo(Player iPlayer) {
		player = iPlayer;
		torpedo = new Cuboid(1.f, 0.2f, 0.2f);
	}

	@Override
	public void setPosition(Vector3f pos) {
		super.setPosition(pos);
		torpedo.setPosition(pos);
	}

	/**
	 * Create the Renderer Hook
	 */
	public void addRendererHook() {
		trh = new CuboidRendererHook(torpedo);
		float[] color3 = {0, 0, 1f, 1f};
		trh.setColor(color3);
		torpedo.addHook(trh);
	}

	/**
	 * Fire the torpedo in the dynamicsWorld!
	 * @param dynamicsWorld
	 */
	public void fire(DynamicsWorld dynamicsWorld, VisibleObjectList<VisibleObject> myList) {

		if (!isFired) {
			isFired = true;
			this.myList = myList;
			this.dynamicsWorld = dynamicsWorld;

			// create the rigid body
			trbh = new CuboidRigidBodyHook(torpedo);
			trbh.setMass(1);
			trbh.setDynamicsWorld(dynamicsWorld);
			torpedo.addHook(trbh);

			// set cleanup timer
			Timer timer = new Timer();
			timer.schedule(new CleanupTask(), 10000);

		}

	}

	/**
	 * After about 10 seconds, the torpedo becomes useless, so try to remove
	 * it from the world
	 */
	private class CleanupTask extends TimerTask {

		@Override
		public void run() {
			remove();
		}

	}

	public void remove() {
		myList.remove(this);
		if (trbh != null && dynamicsWorld != null) {
//			causes NullPointerException
//			at com.bulletphysics.collision.dispatch.SimulationIslandManager.buildAndProcessIslands(SimulationIslandManager.java:284)
//			when the player is in a GhostObject

//			RigidBody tbody = trbh.getBody();
//			if (tbody != null) {
//				dynamicsWorld.removeRigidBody(tbody);
//			}
		}
	}

	@Override
	public void display(GL gl, int dt) {
		super.display(gl, dt);
		torpedo.display(gl, dt);


		Quat4f orientation = player.getOrientation();

		Transform trans = new Transform();
		trans.setRotation(orientation);

		if (!isFired) {
			// the torpedo moves with the player
			torpedo.setRotation(orientation);

			Vector3f pos = new Vector3f(0, 0, 1);
			trans.transform(pos);
			pos.add(player.getPosition());

			torpedo.setPosition(pos);

		} else if (trbh != null) {

			// torpedo is a rigid body now.

			RigidBody tbody = trbh.getBody();

			if (tbody != null) {

				Quat4f tOrientation = new Quat4f();
				tbody.getOrientation(tOrientation);
				Transform tTrans = new Transform();
				tTrans.setRotation(tOrientation);

				Vector3f force = new Vector3f(absAcceleration, 0, 0);
				tTrans.transform(force);
				tbody.applyForce(force, new Vector3f(0, 0, 0));

				// Limit velocity
				Vector3f velocity = new Vector3f();
				tbody.getLinearVelocity(velocity);
				float currentVelocity = velocity.length();
				if (currentVelocity > maxSpeed) {
					velocity.scale(maxSpeed / currentVelocity);
					tbody.setLinearVelocity(velocity);
				}

			}

		}

	}

}

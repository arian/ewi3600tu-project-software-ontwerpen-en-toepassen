/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cube;

import javax.vecmath.Vector3f;

import org.shapes.Hook;
import org.shapes.Shape;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;

public class Cube extends Shape {

	public interface CubeHook extends Hook {
		public void setSize(double s);
	}

	// default size
	private double size = 2;

	public double getSize() {
		return size;
	}

	/**
	 * Set the size of the cube and propagate this to the hooks
	 * @param s
	 */
	public void setSize(double s) {
		size = s;
		for (Hook hook : hooks) {
			if (hook instanceof CubeHook) {
				CubeHook cubeHook = (CubeHook) hook;
				cubeHook.setSize(s);
			}
		}
	}

	@Override
	public CollisionShape createCollisionShape() {
		float size = (float) getSize();
		return new BoxShape(new Vector3f(
				size / 2,
				size / 2,
				size / 2
		));
	}

}

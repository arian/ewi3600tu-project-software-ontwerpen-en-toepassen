/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cube;

import org.shapes.RendererHook;

/**
 * A cube with a CubeRendererHook attached
 */
public class CubeR extends Cube {

	protected RendererHook rh;

	public CubeR() {
		super();
		rh = new CubeRendererHook(this);
		addHook(rh);
	}

}

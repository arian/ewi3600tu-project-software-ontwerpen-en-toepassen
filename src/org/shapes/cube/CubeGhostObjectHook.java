/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cube;

import org.shapes.GhostObjectHook;
import org.shapes.Shape;
import org.shapes.cube.Cube.CubeHook;

/**
 * Defines the ghost object model of a Cube
 */
public class CubeGhostObjectHook extends GhostObjectHook implements CubeHook {

	Cube cube;

	public CubeGhostObjectHook(Shape s) {
		super(s);
		cube = (Cube) s;
		setSize(cube.getSize());
	}

	@Override
	public void setSize(double size) {
		colShape = cube.createCollisionShape();
	}

}

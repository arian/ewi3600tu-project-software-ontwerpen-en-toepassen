/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cube;

import javax.media.opengl.GL;

import org.shapes.RendererHook;
import org.shapes.cube.Cube.CubeHook;

import com.sun.opengl.util.GLUT;

/**
 * This class renders the cube with GLUT
 */
public class PickUpRendererHook extends RendererHook implements CubeHook {

	Cube cube;

	public PickUpRendererHook(Cube s) {
		super(s);
		cube = (Cube) shape;
	}

	@Override
	public void setSize(double s) {
		// empty method, is not necessary for the renderer because the
		// size property is used in the display method
	}

	/**
	 * http://www.opengl.org/documentation/specs/glut/spec3/node82.html
	 */
	@Override
	public void render(GL gl) {

		GLUT glut = new GLUT();
		glut.glutWireCube((float) cube.getSize());
		glut.glutWireOctahedron();
		glut.glutSolidCube((float) 5/4); // for most pickups this equals: cube.getSize()/4

	}

}

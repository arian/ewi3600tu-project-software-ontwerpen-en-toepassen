/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.cube;

import javax.media.opengl.GL;

import org.TextureManager;
import org.shapes.RendererHook;
import org.shapes.cube.Cube.CubeHook;
import org.shapes.cubewall.CubeWall;

/**
 * This class renders the cube with GLUT
 */
public class CubeRendererHook extends RendererHook implements CubeHook {

	Cube cube;
	CubeWall cubewall;

	String texture;

	private int textureposx;
	private int textureposy;
	private int numberOfBlocks;

	public CubeRendererHook(Cube s) {
		super(s);
		cube = (Cube) shape;
	}

	@Override
	public void setSize(double s) {
		// empty method, is not necessary for the renderer because the
		// size property is used in the display method
	}

	public void setTexture(String file) {
		texture = file;
	}

	public String getTexture() {
		return texture;
	}

	public void setTexPosX(int i) {
		textureposx = i;
	}

	public void setTexPosY(int i) {
		textureposy = i;
	}

	public int getTexPosX() {
		return textureposx;
	}

	public int getTexPosY() {
		return textureposy;
	}

	public void setNumberOfTextureBlocks(int i) {
		numberOfBlocks = i;
	}

	@Override
	public void render(GL gl) {

		double size = cube.getSize() / 2.0;

		double as = (1.0 / numberOfBlocks ) * (textureposx - 1);
		double a  = (1.0 / numberOfBlocks ) * textureposx;
		double bs = (1.0 / numberOfBlocks ) * (textureposy - 1);
		double b  = (1.0 / numberOfBlocks ) * textureposy;

		if (texture != null) {

			gl.glEnable(GL.GL_TEXTURE_2D);

			gl.glBindTexture(GL.GL_TEXTURE_2D,
					TextureManager.getInstance(gl).getTexture(texture));

		}

		gl.glBegin(GL.GL_QUADS);

		// font plane
		gl.glNormal3d(1, 0, 0);

		gl.glTexCoord2d(as, bs);	gl.glVertex3d(size, -size, -size);
		gl.glTexCoord2d(a,  bs);	gl.glVertex3d(size,  size, -size);
		gl.glTexCoord2d(a,  b);		gl.glVertex3d(size,  size,  size);
		gl.glTexCoord2d(as, b);		gl.glVertex3d(size, -size,  size);

		// back plane
		gl.glNormal3d(-1, 0, 0);

		gl.glTexCoord2d(as, bs);	gl.glVertex3d(-size, -size, -size);
		gl.glTexCoord2d(as, b);		gl.glVertex3d(-size, -size,  size);
		gl.glTexCoord2d(a,  b);		gl.glVertex3d(-size,  size,  size);
		gl.glTexCoord2d(a,  bs);	gl.glVertex3d(-size,  size, -size);

		// the sides of the cube will get textures too, but it isn't really
		// correct, it does look better though.

		// right side
		gl.glNormal3d(0, 1, 0);

		gl.glTexCoord2d(as, bs);	gl.glVertex3d(-size, size, -size);
		gl.glTexCoord2d(a,  bs);	gl.glVertex3d(-size, size,  size);
		gl.glTexCoord2d(a,  b);		gl.glVertex3d( size, size,  size);
		gl.glTexCoord2d(as, b);		gl.glVertex3d( size, size, -size);

		// left side
		gl.glNormal3d(0,-1,0);

		gl.glTexCoord2d(as, bs);	gl.glVertex3d(-size, -size, -size);
		gl.glTexCoord2d(as, b);		gl.glVertex3d( size, -size, -size);
		gl.glTexCoord2d(a,  b);		gl.glVertex3d( size, -size,  size);
		gl.glTexCoord2d(a,  bs);	gl.glVertex3d(-size, -size,  size);

		// bottom plane
		gl.glNormal3d(0, 0, -1);

		gl.glTexCoord2d(as, bs);	gl.glVertex3d(-size, -size, -size);
		gl.glTexCoord2d(a,  bs);	gl.glVertex3d(-size,  size, -size);
		gl.glTexCoord2d(a,  b);		gl.glVertex3d( size,  size, -size);
		gl.glTexCoord2d(as, b);		gl.glVertex3d( size, -size, -size);

		// upper plane
		gl.glNormal3d(0, 0, 1);

		gl.glTexCoord2d(as, bs);	gl.glVertex3d(-size, -size, size);
		gl.glTexCoord2d(as, b);		gl.glVertex3d( size, -size, size);
		gl.glTexCoord2d(a,  b);		gl.glVertex3d( size,  size, size);
		gl.glTexCoord2d(a,  bs);	gl.glVertex3d(-size,  size, size);

		gl.glEnd();

		if (texture != null) {
			gl.glDisable(GL.GL_TEXTURE_2D);
		}

	}

}

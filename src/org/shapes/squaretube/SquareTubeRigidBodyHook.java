/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.squaretube;

import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.squaretube.SquareTube.SquareTubeHook;

/**
 * Defines the physics model of a SquareTube
 */
public class SquareTubeRigidBodyHook extends RigidBodyHook implements SquareTubeHook {

	protected SquareTube squaretube;

	public SquareTubeRigidBodyHook(Shape s) {
		super(s);
		squaretube = (SquareTube) shape;
		setSize(squaretube.getSize());
	}

	@Override
	public void setSize(double size) {
		colShape = squaretube.createCollisionShape();
	}

}

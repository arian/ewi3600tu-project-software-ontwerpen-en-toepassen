/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.squaretube;

/**
 * A cube with a CubeRendererHook attached
 */
public class SquareTubeR extends SquareTube {

	public SquareTubeR() {
		super();
		addHook(new SquareTubeRendererHook(this));
	}

}

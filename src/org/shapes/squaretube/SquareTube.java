/*
 *  _____ ____  ____  _     ____  ____ ____ftw.
 * /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
 * | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
 * | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
 * \____\\_/\_\\____/\____/\_/   \____//____/
 */
package org.shapes.squaretube;

import javax.vecmath.Vector3f;

import org.gm.game.Game;
import org.shapes.Hook;
import org.shapes.Shape;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.ConvexHullShape;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;

public class SquareTube extends Shape {

	private double size = Game.tunnelSize;

	public interface SquareTubeHook extends Hook {
		public void setSize(double s);
	}

	public double getSize() {
		return size;
	}

	/**
	 * Set the size of the cube and propagate this to the hooks
	 *
	 * @param s
	 */
	public void setSize(double s) {
		size = s;
		for (Hook hook : hooks) {
			if (hook instanceof SquareTubeHook) {
				SquareTubeHook squaretubeHook = (SquareTubeHook) hook;
				squaretubeHook.setSize(s);
			}
		}
	}

	@Override
	public CollisionShape createCollisionShape() {

		ObjectArrayList<Vector3f> tubeCollisionPoints = new ObjectArrayList<Vector3f>();

		ConvexHullShape colTubeGround = new ConvexHullShape(tubeCollisionPoints);
		ConvexHullShape colTubeCeiling = new ConvexHullShape(tubeCollisionPoints);
		ConvexHullShape colTubeLeft = new ConvexHullShape(tubeCollisionPoints);
		ConvexHullShape colTubeRight = new ConvexHullShape(tubeCollisionPoints);

		float size = (float) this.size / 2f;
		float s13 = ((size * 2) / 3f) - size;
		float s23 = ((size * 4) / 3f) - size;

		// floor
		colTubeGround.addPoint(new Vector3f(s13, s13, -size));
		colTubeGround.addPoint(new Vector3f(s13, s13, size));
		colTubeGround.addPoint(new Vector3f(s23, s13, size));
		colTubeGround.addPoint(new Vector3f(s23, s13, -size));

		// ceiling
		colTubeCeiling.addPoint(new Vector3f(s13, s23, -size));
		colTubeCeiling.addPoint(new Vector3f(s23, s23, -size));
		colTubeCeiling.addPoint(new Vector3f(s23, s23, size));
		colTubeCeiling.addPoint(new Vector3f(s13, s23, size));

		// left plane
		colTubeLeft.addPoint(new Vector3f(s13, s13, -size));
		colTubeLeft.addPoint(new Vector3f(s13, s23, -size));
		colTubeLeft.addPoint(new Vector3f(s13, s23, size));
		colTubeLeft.addPoint(new Vector3f(s13, s13, size));

		// right plane
		colTubeRight.addPoint(new Vector3f(s23, s23, -size));
		colTubeRight.addPoint(new Vector3f(s23, s13, -size));
		colTubeRight.addPoint(new Vector3f(s23, s13, size));
		colTubeRight.addPoint(new Vector3f(s23, s23, size));

		// create the compound shape
		CompoundShape compound = new CompoundShape();
		Transform transform = new Transform();
		transform.setIdentity();
		compound.addChildShape(transform, colTubeGround);
		compound.addChildShape(transform, colTubeCeiling);
		compound.addChildShape(transform, colTubeLeft);
		compound.addChildShape(transform, colTubeRight);

		return compound;
	}

}

/*
 *  _____ ____  ____  _     ____  ____ ____ftw.
 * /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
 * | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
 * | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
 * \____\\_/\_\\____/\____/\_/   \____//____/
 */
package org.shapes.squaretube;

import javax.media.opengl.GL;

import org.TextureManager;
import org.shapes.RendererHook;
import org.shapes.squaretube.SquareTube.SquareTubeHook;

public class SquareTubeRendererHook extends RendererHook implements
		SquareTubeHook {

	private String texture = "futurefloor.jpg";
	private SquareTube squaretube;

	public SquareTubeRendererHook(SquareTube s) {
		super(s);
		squaretube = (SquareTube) shape;
	}

	@Override
	public void setSize(double s) {
		// empty method, is not necessary for the renderer because the
		// size property is used in the display method
	}

	@Override
	public void render(GL gl) {

		float wallColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		// Set the materials used by the floor.
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, wallColor, 0);

		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glBindTexture(GL.GL_TEXTURE_2D, TextureManager.getInstance(gl)
				.getTexture(texture));
		gl.glBegin(GL.GL_QUADS);

		gl.glNormal3d(0.0, 1.0, 0.0);

		double size = squaretube.getSize() / 2;
		double n = 1;
		double s13 = ((size * 2) / 3) - size;
		double s23 = ((size * 4) / 3) - size;

		gl.glTexCoord2d(0, 0);
		gl.glVertex3d(s13, s13, -size);

		gl.glTexCoord2d(n, 0);
		gl.glVertex3d(s13, s13, size);

		gl.glTexCoord2d(n, n);
		gl.glVertex3d(s23, s13, size);

		gl.glTexCoord2d(0, n);
		gl.glVertex3d(s23, s13, -size);

		gl.glNormal3d(0.0, -1.0, 0.0);

		// plafond
		gl.glTexCoord2d(0, 0);
		gl.glVertex3d(s13, s23, -size);

		gl.glTexCoord2d(n, 0);
		gl.glVertex3d(s23, s23, -size);

		gl.glTexCoord2d(n, n);
		gl.glVertex3d(s23, s23, size);

		gl.glTexCoord2d(0, n);
		gl.glVertex3d(s13, s23, size);

		// linkervlak (achterkant van fov begin)

		gl.glNormal3d(1.0, 0.0, 0.0);
		gl.glTexCoord2d(0, 0);
		gl.glVertex3d(s13, s13, -size);

		gl.glTexCoord2d(n, 0);
		gl.glVertex3d(s13, s23, -size);

		gl.glTexCoord2d(n, n);
		gl.glVertex3d(s13, s23, size);

		gl.glTexCoord2d(0, n);
		gl.glVertex3d(s13, s13, size);

		// rechtervlak
		gl.glNormal3d(-1.0, 0.0, 0.0);

		gl.glTexCoord2d(0, 0);
		gl.glVertex3d(s23, s23, -size);

		gl.glTexCoord2d(n, 0);
		gl.glVertex3d(s23, s13, -size);

		gl.glTexCoord2d(n, n);
		gl.glVertex3d(s23, s13, size);

		gl.glTexCoord2d(0, n);
		gl.glVertex3d(s23, s23, size);

		gl.glEnd();
		gl.glDisable(GL.GL_TEXTURE_2D);

	}

}

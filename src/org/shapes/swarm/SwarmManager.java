package org.shapes.swarm;

import javax.vecmath.Vector3f;

import org.VisibleObject;
import org.VisibleObjectList;

public class SwarmManager extends VisibleObjectList<Swarm> implements VisibleObject {

	private static final long serialVersionUID = -3441308557945139715L;

	/**
	 * This will get the closest swarm in the managers memory
	 * @param position The position to search from
	 */
	public Swarm getClosestSwarm(Vector3f position) {

		Swarm swarm = null;
		float min = Integer.MAX_VALUE;

		for (Swarm _swarm : this) {
			Vector3f distance = new Vector3f();
			distance.sub(position, _swarm.getPosition());
			if (distance.lengthSquared() < min) {
				swarm = _swarm;
			}
		}

		return swarm;
	}

}

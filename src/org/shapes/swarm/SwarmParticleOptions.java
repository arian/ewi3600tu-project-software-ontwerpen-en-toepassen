/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.swarm;

import org.util.MathUtil;

// class with options for the particles
public class SwarmParticleOptions {

	protected float mass = 2;
	protected float radius = 3;
	protected float maxSpeed = 25;
	protected float[] positionVariation = {-10, 10};
	protected float localBestScale = 4;
	protected float globalBestScale = 1;
	protected float[] explorationVariation = {-20, 20};

	/**
	 * Copies this object into a new SwarmParticleOptions object
	 * @return a copy of this object
	 */
	public SwarmParticleOptions copy() {
		SwarmParticleOptions copy = new SwarmParticleOptions();
		copy.mass = mass;
		copy.radius = radius;
		copy.maxSpeed = maxSpeed;
		copy.positionVariation = positionVariation;
		copy.localBestScale = localBestScale;
		copy.globalBestScale = globalBestScale;
		copy.explorationVariation = explorationVariation;
		return copy;
	}

	// chainable setters
	public SwarmParticleOptions setMass(float m) {
		mass = m;
		return this;
	}

	public SwarmParticleOptions setRadius(float r) {
		radius = r;
		return this;
	}

	public SwarmParticleOptions setMaxSpeed(float s) {
		maxSpeed = s;
		return this;
	}

	/**
	 * Set the maxSpeed with a certain randomness
	 * @param s The average max speed
	 * @param r The random interval
	 * @return this object
	 */
	public SwarmParticleOptions setMaxSpeed(float s, float r) {
		maxSpeed = (float) (s + MathUtil.randomBetween(-r, r));
		return this;
	}

	public SwarmParticleOptions setPositionVariation(float[] v) {
		positionVariation = v;
		return this;
	}

	public SwarmParticleOptions setLocalBestScale(float s) {
		localBestScale = s;
		return this;
	}

	public SwarmParticleOptions setGlobalBestScale(float s) {
		globalBestScale = s;
		return this;
	}

	public SwarmParticleOptions setExplorationVariation(float[] e) {
		explorationVariation = e;
		return this;
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.swarm;

import javax.media.opengl.GL;
import javax.vecmath.Vector3f;

import org.VisibleObjectList;
import org.gm.game.player.Player;
import org.shapes.Shape;
import org.util.Time;

import com.bulletphysics.dynamics.DynamicsWorld;

public class Swarm extends Shape {

	private VisibleObjectList<Particle> particles = new VisibleObjectList<Particle>();
	private DynamicsWorld dynamicsWorld;
	private Player player;
	protected boolean active;
	private int maxFPS = 40;
	private long prevTime;
	protected boolean update;

	// fitest particle
	protected Particle fittest;

	public Swarm(DynamicsWorld dynamicsWorld, Player player) {
		this.dynamicsWorld = dynamicsWorld;
		this.player = player;
	}

	/**
	 * Calculate the fitness of a particle. It will also set the
	 * particle.fitness property of the particle. If this particle is the
	 * fittest for this iteration, it will become the 'fittest' particle
	 * @param particle
	 * @return calculated fitness
	 */
	protected float fitness(Particle particle) {
		Vector3f distance = new Vector3f();
		distance.sub(player.getPosition(), particle.getPosition());
		float fitness = particle.fitness = distance.lengthSquared();
		if (fittest == null || fitness < fittest.fitness) {
			fittest = particle;
		}
		return particle.fitness;
	}

	/**
	 * Builds a number of particles for this swarm
	 * @param count
	 * @param options
	 */
	public void build(int count, SwarmParticleOptions options) {
		if (count < 2) {
			count = 2;
		}
		Particle particle = null, first;
		Particle previous = first = new Particle(options, this, dynamicsWorld);
		particles.add(previous);
		for (int i = 1; i < count; i++) {
			particle = new Particle(options, this, dynamicsWorld);
			particles.add(particle);
			previous.next = particle;
			previous = particle;
		}
		particle.next = first;
	}

	public void build(int count) {
		build(count, new SwarmParticleOptions());
	}

	public void setParticleOptions(SwarmParticleOptions options) {
		for (Particle particle : particles) {
			particle.setOptions(options);
		}
	}

	public void setMaxSpeed(float speed) {
		for (Particle particle : particles) {
			particle.getOptions().setMaxSpeed(speed, 5);
		}
	}

	/**
	 * Activate the swarm, particles will try to find the player
	 */
	public void activate() {
		active = true;
	}

	/**
	 * Deactivates the swarm, particles can rest now.
	 */
	public void deactivate() {
		active = false;
	}

	@Override
	public void display(GL gl, int dt) {
		super.display(gl, dt);
		long time = Time.getMillis();
		update = false;
		// only calculate the fitness of each particle once in ... time, no need
		// to do it too often.
		if ((time - prevTime) > (1000 / maxFPS)) {
			update = true;
			prevTime = time;
		}
		particles.display(gl, dt);
	}

}

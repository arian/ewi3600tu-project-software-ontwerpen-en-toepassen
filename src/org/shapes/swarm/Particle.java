/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.swarm;

import javax.media.opengl.GL;
import javax.vecmath.Vector3f;

import org.shapes.RigidBodyHook;
import org.shapes.ShapeException;
import org.shapes.sphere.SphereR;
import org.shapes.sphere.SphereRigidBodyHook;
import org.util.MathUtil;

import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;

public class Particle extends SphereR {

	// Parent swarm
	protected Swarm swarm;

	// This is the force that is applied to the particle body
	protected Vector3f force = new Vector3f();

	protected final RigidBodyHook rbh;

	// fitness will be minimized. This property will be set by the
	// Swarm.fitness() method
	protected float fitness = Integer.MAX_VALUE;

	protected Particle next;

	private SwarmParticleOptions options;

	/**
	 * Create particles as shapes and rigid bodies
	 * @param options all kind of options for this particle
	 * @param swarm the parent swarm
	 * @param dynamicsWorld the parent dynamicsWorld
	 */
	public Particle(SwarmParticleOptions options, Swarm swarm, DynamicsWorld dynamicsWorld) {
		super(options.radius);
		this.setOptions(options);
		this.swarm = swarm;

		Vector3f pos = new Vector3f();
		pos.add(swarm.getPosition(), new Vector3f(
				MathUtil.randomBetween(options.positionVariation),
				MathUtil.randomBetween(options.positionVariation),
				MathUtil.randomBetween(options.positionVariation)
				));
		this.setPosition(pos);

		rbh = new SphereRigidBodyHook(this);
		rbh.setMass(options.mass);
		rbh.setDynamicsWorld(dynamicsWorld);
		try {
			rbh.createRigidBody();
		} catch (ShapeException e) {
			e.printStackTrace();
		}
		this.addHook(rbh);
	}

	/**
	 * Set new options for this particle
	 * @param options
	 */
	public void setOptions(SwarmParticleOptions options) {
		this.options = options.copy();
	}

	/**
	 *
	 * @return the options object
	 */
	public SwarmParticleOptions getOptions() {
		return options;
	}

	@Override
	public void display(GL gl, int dt) {
		RigidBody body = rbh.getBody();
		body.setGravity(new Vector3f(0, 0, 0));
		if (swarm.active) {
			body.activate();

			if (swarm.update) {

				swarm.fitness(this);

				force = new Vector3f();

				// Local best
				if (fitness > next.fitness) {
					Vector3f d1 = new Vector3f();
					d1.sub(next.position, position);
					d1.scale(options.localBestScale);
					force.add(d1);
				}

				// Global best
				if (swarm.fittest != null && fitness > swarm.fittest.fitness) {
					Vector3f d2 = new Vector3f();
					d2.sub(swarm.fittest.position, position);
					// A big number here will make the particles converge very
					// quickly to the current global best solution. This is not
					// always desired if it should be harder for the player to
					// pass the swarm.
					d2.scale(options.globalBestScale);
					force.add(d2);
				} else {
					// this particle one is the best solution thus far, apply a
					// random force to 'explore' new places
					force.add(new Vector3f(
							MathUtil.randomBetween(options.explorationVariation),
							MathUtil.randomBetween(options.explorationVariation),
							MathUtil.randomBetween(options.explorationVariation)));
				}

			}

			// Apply force
			if (force.x != 0 && force.y != 0 && force.z != 0) {
				body.applyForce(force, new Vector3f(0, 0, 0));
			}

			// Limit velocity. This is not entirely according the theory, but
			// this is necessary because particles and the swarm might get
			// unstable behavior and will not converge at all to the player.
			Vector3f velocity = new Vector3f();
			body.getLinearVelocity(velocity);
			final float currentVelocity = velocity.length();
			if (currentVelocity > options.maxSpeed) {
				velocity.scale(options.maxSpeed / currentVelocity);
				body.setLinearVelocity(velocity);
			}

		} else {
			// Deactivate the rigid body simulation for this object when the
			// swarm is inactive.
			// body.setActivationState(CollisionObject.WANTS_DEACTIVATION);
		}

		super.display(gl, dt);
	}

}

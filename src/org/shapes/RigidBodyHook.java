/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes;

import javax.media.opengl.GL;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;

/**
 * Defines the (rigid body) physics model of a shape
 */
public abstract class RigidBodyHook implements Hook {

	protected CollisionShape colShape;
	protected RigidBody body;
	protected DynamicsWorld dynamicsWorld;
	// default 0 mass, which will become a static body
	protected float mass = 0f;
	protected Vector3f inertia = new Vector3f(0, 0, 0);
	protected Shape shape;

	public RigidBodyHook(Shape s) {
		shape = s;
	}

	/**
	 * Set the mass of the body. 0 will make the body static
	 * @param mass (some float value in imaginary kilograms)
	 */
	public void setMass(float mass) {
		this.mass = mass;
	}

	/**
	 * Get the mass of the body. If the mass is 0, the body is static
	 * @return the mass of the body.
	 */
	public float getMass() {
		return mass;
	}

	public void setInertia(Vector3f inertia) {
		this.inertia = inertia;
	}

	public Vector3f getInertia() {
		return inertia;
	}

	/**
	 * Set the DynamicsWorld the body will live in.
	 * @param dynamicsWorld
	 */
	public void setDynamicsWorld(DynamicsWorld dynamicsWorld) {
		this.dynamicsWorld = dynamicsWorld;
	}

	/**
	 * Returns the current DynamicsWorld object
	 * @return the dynamicsWorld object
	 */
	public DynamicsWorld getDynamicsWorld() {
		return dynamicsWorld;
	}

	/*
	 * Propagated setPosition method, called when shape.setPosition(pos) is
	 * called.
	 * @param p
	 */
	@Override
	public void setPosition(Vector3f p) {
		if (body != null) {
			// TODO probably can be done better, see page 24 of Bullet 2.76 Physics SDK Manual
			addRigidBodyToWorld();
		}
	}

	/*
	 * Propagated setRotation method, called when shape.setRotation(rot) is
	 * called
	 */
	@Override
	public void setRotation(Quat4f r) {
		if (body != null) {
			addRigidBodyToWorld();
		}
	}

	/**
	 * @return the RigidBody. It can be used to apply accelerations, velocities
	 * to the object
	 */
	public RigidBody getBody() {
		return body;
	}

	@Override
	public void display(GL gl) throws ShapeException {
		// Create body if none exists yet
		if (body == null) {
			createRigidBody();
		}

		// checking if the body is active, it actually slows things down
		// if (body.isActive())

		if (this.mass != 0) {
			Transform trans = new Transform();
			body.getMotionState().getWorldTransform(trans);
			shape.setPosition(trans.origin, Shape.NO_PROPAGATE);
			Quat4f rotation = new Quat4f();
			trans.getRotation(rotation);
			shape.setRotation(rotation, Shape.NO_PROPAGATE);
		}

	}

	/**
	 * Creates a body object with the mass and inertia and other properties
	 */
	public void createRigidBody() throws ShapeException {

		if (colShape == null) {
			throw new ShapeException("The collisionShape (= the colShape property) has to be set first, with something like .setSize()");
		}

		if (dynamicsWorld == null) {
			throw new ShapeException("The dynamicsWorld has to be set first, with something like .setDynamicsWorld(dw)");
		}

		// make sure the object can rotate
		colShape.calculateLocalInertia(mass, inertia);

		addRigidBodyToWorld();
	}

	/**
	 * This is in a separate method so the calculateLocalIntertia for the shape
	 * and other checks in the createRigidBody method is only executed once.
	 */
	private void addRigidBodyToWorld() {
		// remove if there is a body already
		if (body != null) {
			dynamicsWorld.removeRigidBody(body);
		}

		// Create Dynamic Objects
		Transform trans = new Transform();
		trans.setIdentity();

		Vector3f origin = shape.getPosition();
		origin.sub(shape.getCenterOfBody());
		trans.origin.set(origin);

		trans.setRotation(shape.getRotation());

		DefaultMotionState motionState = new DefaultMotionState(trans);

		RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(
				mass, motionState, colShape, inertia);

		// TODO add a getter/setter or something to set the 'restitution'
		// (= bouncing effect)
		// rbInfo.restitution = 1.3f;

		body = new RigidBody(rbInfo);

		dynamicsWorld.addRigidBody(body);
	}

//	Using a custom RigidBodyHookMotionState causes jittering
//
//	private class RigidBodyHookMotionState extends DefaultMotionState {
//
//		public RigidBodyHookMotionState(Transform trans) {
//			super(trans);
//		}
//
//		@Override
//		public void setWorldTransform(Transform worldTrans) {
//			super.setWorldTransform(worldTrans);
//			Transform trans = new Transform();
//			this.getWorldTransform(trans);
//			shape.setPosition(trans.origin, Shape.NO_PROPAGATE);
//			Quat4f rotation = new Quat4f();
//			trans.getRotation(rotation);
//			shape.setRotation(rotation, Shape.NO_PROPAGATE);
//		}
//
//	}

}

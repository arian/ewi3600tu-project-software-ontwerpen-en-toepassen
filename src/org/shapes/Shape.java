/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes;

import java.util.ArrayList;

import javax.media.opengl.GL;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import org.VisibleObject;
import org.util.Events;

import com.bulletphysics.collision.shapes.CollisionShape;

/**
 * Abstract shape class, defines the position and rotation of objects
 * It has the possibility to add hooks. These hooks can render the shape
 * or do some physics to calculate new coordinates and orientations.
 */
public abstract class Shape extends Events implements VisibleObject {

	public static final int NO_PROPAGATE = 0;
	public static final int PROPAGATE = 1;

	protected Vector3f position = new Vector3f();
	protected Quat4f rotation = new Quat4f(0, 0, 0, 1);
	protected ArrayList<Hook> hooks = new ArrayList<Hook>();

	/**
	 * Set the position of the object
	 * The position is usually the middle point of the shape
	 * @param pos
	 * @param options
	 */
	public void setPosition(Vector3f pos, int options) {
		position = pos;
		if ((options & Shape.PROPAGATE) == 1) {
			for (Hook hook : hooks) {
				hook.setPosition(position);
			}
		}
	}

	/**
	 * Set the position of the object
	 * The position is usually the middle point of the shape
	 * Also propagate this change to the added hooks
	 * @param pos
	 */
	public void setPosition(Vector3f pos) {
		setPosition(pos, Shape.PROPAGATE);
	}

	/**
	 * Gets the position of the object
	 * Usually the middle point of the shape
	 * @return the position vector
	 */
	public Vector3f getPosition() {
		return position;
	}

	/**
	 * Set the rotation with a Quaternion
	 * Calculations from AxisAngle to Quaternion:
	 * http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm
	 * and back:
	 * http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToAngle/index.htm
	 * @param r a Quaternion to define the rotation
	 */
	public void setRotation(Quat4f r, int options) {
		rotation = r;
		if ((options & Shape.PROPAGATE) == 1) {
			for (Hook hook : hooks) {
				hook.setRotation(rotation);
			}
		}
	}

	/**
	 * Set the rotation with a Quaternion and propagate this change to the
	 * added hooks
	 * @param r
	 */
	public void setRotation(Quat4f r) {
		setRotation(r, Shape.PROPAGATE);
	}

	/**
	 * The rotation about a certain vector
	 * @param r A AxisAngle to define the rotation
	 */
	public void setRotation(AxisAngle4f r) {
		rotation.set(r);
		setRotation(rotation);
	}

	/**
	 * Get the rotation as Quaternion
	 * @return rotation as quaternion
	 */
	public Quat4f getRotation() {
		return rotation;
	}

	/**
	 * Calls the display hooks for this shape
	 * Usually the RenderHook and/or RigidHook
	 */
	public void display(GL gl) {
		try {
			for (Hook hook : hooks) {
				hook.display(gl);
			}
		} catch (ShapeException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void display(GL gl, int dt) {
		display(gl);
	}

	/**
	 * Creates a collision shape
	 * For this to be useful this method should be overridden.
	 * @return the created collisionShape
	 */
	public CollisionShape createCollisionShape() {
		return null;
	}

	/**
	 * Add a Hook object.
	 * @param hook
	 */
	public void addHook(Hook hook) {
		hooks.add(hook);
	}

	/**
	 * Gets the center of body. Must be implemented by all child classes
	 * @return The center of the body as a vector
	 */
	public Vector3f getCenterOfBody() {
		return new Vector3f(0, 0, 0);
	}

}

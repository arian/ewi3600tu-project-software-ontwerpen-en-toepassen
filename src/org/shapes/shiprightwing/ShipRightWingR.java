/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.shiprightwing;

/**
 * A cube with a CubeRendererHook attached
 */
public class ShipRightWingR extends ShipRightWing {

	public ShipRightWingR() {
		super();
		addHook(new ShipRightWingRendererHook(this));
	}

}
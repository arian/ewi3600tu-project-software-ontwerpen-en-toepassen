/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.shiprightwing;

import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.shiprightwing.ShipRightWing.ShipRightWingHook;


/**
 * Defines the physics model of a Cube
 */
public class ShipRightWingRigidBodyHook extends RigidBodyHook implements ShipRightWingHook {

	ShipRightWing shiprw;

	public ShipRightWingRigidBodyHook(Shape s) {
		super(s);
		shiprw = (ShipRightWing) shape;
		setSize(shiprw.getSize());
	}

	@Override
	public void setSize(double size) {
		colShape = shiprw.createCollisionShape();
	}

}

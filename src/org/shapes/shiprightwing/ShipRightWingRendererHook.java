/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.shiprightwing;

import javax.media.opengl.GL;

import org.TextureManager;
import org.shapes.RendererHook;
import org.shapes.shiprightwing.ShipRightWing.ShipRightWingHook;

/**
 * This class renders the cube with GLUT
 */
public class ShipRightWingRendererHook extends RendererHook implements ShipRightWingHook {

	ShipRightWing shiprw;
	String texture = "urbancamo.jpg";

	public ShipRightWingRendererHook(ShipRightWing s) {
		super(s);
		shiprw = (ShipRightWing) shape;
	}

	@Override
	public void setSize(double s) {
		// empty method, is not necessary for the renderer because the
		// size property is used in the display method
	}

	public void dop(GL gl, double x, double y, double z, double radius, int res, double hoekbocht){
		// doppen op de tube
		double y1, z1, y2, z2;
		double hoek = 0;

		gl.glBegin(GL.GL_TRIANGLE_FAN);
		gl.glNormal3d(-1.0, 0, 0);

		for (int i = 0; i<res ; i++) {

			gl.glVertex3d(x,y,z);
			// coordinaten van de buitenste baan
	    	y1 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z1 = Math.cos(Math.toRadians(hoek)) * radius;

	    	hoek += hoekbocht/res;

	    	// coordinaten van de buitenste baan
	    	y2 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z2 = Math.cos(Math.toRadians(hoek)) * radius;

	    	gl.glVertex3d(x, y + y1, z+z1);
	    	gl.glVertex3d(x, y + y2, z+z2);
		}
		gl.glEnd();
	}

	public void tube(GL gl, double beginx, double beginy, double beginz, double radius, double lengte){

		double hoek = 0;
		double hoekbocht = 360;
		double y1,z1;
		int res = 48;

		gl.glBegin(GL.GL_QUADS);
		for (int i = 0; i<res ; i++) {

			gl.glNormal3d(0, Math.sin(Math.toRadians(hoek)), Math.cos(Math.toRadians(hoek)));

			// coordinaten van de buitenste baan
	    	y1 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z1 = Math.cos(Math.toRadians(hoek)) * radius;

	    	gl.glVertex3d(beginx, beginy + y1, beginz+z1);
	    	gl.glVertex3d(beginx + lengte, beginy + y1, beginz+z1);

	    	hoek += hoekbocht / res;

	    	// coordinaten van de buitenste baan
	    	y1 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z1 = Math.cos(Math.toRadians(hoek)) * radius;

	    	gl.glVertex3d(beginx + lengte, beginy + y1, beginz+z1);
	    	gl.glVertex3d(beginx, beginy + y1, beginz + z1);
		}

		gl.glEnd();
		dop(gl, beginx, beginy, beginz, radius, res, hoekbocht);
	}

	public void tubeTip(GL gl, double beginx, double beginy,double beginz, double radius, double lengte, double lengtepunt){
		double hoek = 0;
		double hoekbocht = 360;
		double y1, z1, y2, z2, xn, yn, zn, uni;
		int res = 48;

		xn = Math.tan(Math.toRadians(45));
		tube(gl, beginx, beginy,beginz, radius, lengte);

		gl.glBegin(GL.GL_TRIANGLE_FAN);
		for (int i = 0; i < res; i++) {
			yn = Math.sin(Math.toRadians(hoek));
			zn = Math.cos(Math.toRadians(hoek));
			uni = Math.sqrt((xn*xn)+(yn*yn)+(zn*zn));

			gl.glNormal3d(
					(xn / uni) * (xn / uni),
					(yn / uni) * (yn / uni),
					(zn / uni) * (zn / uni));

			// coordinaten van de buitenste baan
	    	y1 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z1 = Math.cos(Math.toRadians(hoek)) * radius;

	    	hoek += hoekbocht/res;

	    	// coordinaten van de buitenste baan
	    	y2 = Math.sin(Math.toRadians(hoek)) * radius;
	    	z2 = Math.cos(Math.toRadians(hoek)) * radius;

	    	gl.glVertex3d(beginx + lengte + lengtepunt, beginy, beginz);
	    	gl.glVertex3d(beginx + lengte, beginy + y2, beginz + z2);
	    	gl.glVertex3d(beginx + lengte, beginy + y1, beginz + z1);
		}

		gl.glEnd();
	}

	@Override
	public void render(GL gl) {

		gl.glBindTexture(GL.GL_TEXTURE_2D, TextureManager.getInstance(gl).getTexture(texture));
		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glBegin(GL.GL_QUADS);

		// bottom plane
		gl.glNormal3d(0.0, -0.0375, 1.0);

		gl.glTexCoord2d(0, 0);		gl.glVertex3d(0.6, 1.0, 0.15);
		gl.glTexCoord2d(1, 0);		gl.glVertex3d(-0.6, 1.0, 0.15);
		gl.glTexCoord2d(0.5, 1);	gl.glVertex3d(-0.6, -1.0, 0.075);
		gl.glTexCoord2d(0, 1);		gl.glVertex3d(0.0, -1.0, 0.075);

		// upper plane
		gl.glNormal3d(0, -0.0375, -1.0);

		gl.glTexCoord2d(0, 0);		gl.glVertex3d(0.6, 1.0, -0.15);
		gl.glTexCoord2d(0, 1);		gl.glVertex3d(0.0, -1.0, -0.075);
		gl.glTexCoord2d(0.5, 1);	gl.glVertex3d(-0.6, -1.0, -0.075);
		gl.glTexCoord2d(1, 0);		gl.glVertex3d(-0.6, 1.0, -0.15);

		// back plane
		gl.glNormal3d(-1.0, 0, 0);

		gl.glTexCoord2d(0, 0);		gl.glVertex3d(-0.6, 1.0, -0.15);
		gl.glTexCoord2d(2, 0.2);	gl.glVertex3d(-0.6, -1.0, -0.075);
		gl.glTexCoord2d(2, 0);		gl.glVertex3d(-0.6, -1.0, 0.075);
		gl.glTexCoord2d(0, 0.2);	gl.glVertex3d(-0.6, 1.0, 0.15);

		// front plane
		gl.glNormal3d(0.958, -0.287, 0);

		gl.glTexCoord2d(0, 0);		gl.glVertex3d(0.6, 1.0, 0.15);
		gl.glTexCoord2d(2, 0.2);	gl.glVertex3d(0.0, -1.0, 0.075);
		gl.glTexCoord2d(2, 0);		gl.glVertex3d(0.0, -1.0, -0.075);
		gl.glTexCoord2d(0, 0.2);	gl.glVertex3d(0.6, 1.0, -0.15);

		// teken de cylinder aan het puntje van de vleugel
		tubeTip(gl, -0.7, -1, 0, 0.1, 0.8, 0.1);
	}

}
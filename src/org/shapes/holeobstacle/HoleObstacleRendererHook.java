/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.holeobstacle;

import javax.media.opengl.GL;

import org.TextureManager;
import org.shapes.RendererHook;
import org.shapes.holeobstacle.HoleObstacle.HoleObstacleHook;

// import com.sun.opengl.util.GLUT;


/**
 * This class renders the Hole Obstacle with GLUT
 */
public class HoleObstacleRendererHook extends RendererHook implements HoleObstacleHook {

	HoleObstacle holeobstacle;

	// texture file
	String texture = "purple_floor.png";
	// texture resolution
	float n = 1;

	public HoleObstacleRendererHook(HoleObstacle s) {
		super(s);
		holeobstacle = (HoleObstacle) shape;
	}

	@Override
	public void setSize(double s) {
		// empty method, is not necessary for the renderer because the
		// size property is used in the display method
	}

	private void drawPlane(GL gl, float[][] points, int start) {
		gl.glTexCoord2f(0, 0);
		gl.glVertex3fv(points[start    ], 0);
		gl.glTexCoord2f(0, n);
		gl.glVertex3fv(points[start + 1], 0);
		gl.glTexCoord2f(n, n);
		gl.glVertex3fv(points[start + 2], 0);
		gl.glTexCoord2f(n, 0);
		gl.glVertex3fv(points[start + 3], 0);
	}

	/**
	 * http://www.opengl.org/documentation/specs/glut/spec3/node82.html
	 */
	@Override
	public void render(GL gl) {

		// set the wall color
		float wallColor[] = {1, 1, 1, 1.0f };
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, wallColor, 0);

		// texture selecting and binding
		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glBindTexture(GL.GL_TEXTURE_2D, TextureManager.getInstance(gl).getTexture(texture));

		float[][] points = holeobstacle.getPoints();

		gl.glBegin(GL.GL_QUADS);

		// front
		gl.glNormal3f(-1, 0, 0);
		drawPlane(gl, points, 0);
		drawPlane(gl, points, 4);
		drawPlane(gl, points, 8);
		drawPlane(gl, points, 12);

		// normal to the left
		gl.glNormal3f(0, -1, 0);
		drawPlane(gl, points, 16);
		drawPlane(gl, points, 36);

		// normal to the right
		gl.glNormal3f(0, 1, 0);
		drawPlane(gl, points, 20);
		drawPlane(gl, points, 32);

		// normal up
		gl.glNormal3f(0, 0, 1);
		drawPlane(gl, points, 24);
		drawPlane(gl, points, 44);

		// normal down
		gl.glNormal3f(0, 0, -1);
		drawPlane(gl, points, 28);
		drawPlane(gl, points, 40);

		// front
		gl.glNormal3f(1, 0, 0);
		drawPlane(gl, points, 48);
		drawPlane(gl, points, 52);
		drawPlane(gl, points, 56);
		drawPlane(gl, points, 60);

		gl.glEnd();

		gl.glDisable(GL.GL_TEXTURE_2D);
	}

}

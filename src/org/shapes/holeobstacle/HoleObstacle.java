/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.holeobstacle;

import javax.vecmath.Vector3f;

import org.gm.game.Game;
import org.shapes.Hook;
import org.shapes.Shape;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.linearmath.Transform;

public class HoleObstacle extends Shape {

	public interface HoleObstacleHook extends Hook {
		public void setSize(double s);
	}

	private double size = Game.tunnelSize;

	// hole dimensions
	private double holeHeight = 10;
	private double holeWidth = 10;

	private float[][] points;

	public double getSize() {
		return size;
	}

	// hole height
	public double getHoleHeight() {
		return holeHeight;
	}

	public void setHoleHeight(double h) {
		this.holeHeight = h;
		points = calculatePoints();
	}

	// hole width
	public double getHoleWidth() {
		return holeWidth;
	}

	public void setHoleWidth(double w) {
		this.holeWidth = w;
		points = calculatePoints();
	}

	/**
	 * Set the size of the cube and propagate this to the hooks
	 * @param s
	 */
	public void setSize(double s) {
		size = s;
		points = calculatePoints();
		for (Hook hook : hooks) {
			if (hook instanceof HoleObstacleHook) {
				HoleObstacleHook holeobstacleHook = (HoleObstacleHook) hook;
				holeobstacleHook.setSize(s);
			}
		}
	}

	public float[][] getPoints() {
		if (points == null) {
			points = calculatePoints();
		}
		return points;
	}

	private float[][] calculatePoints() {

		//        +-------------+
		//        |\      1.    |\
		//        | \           | \
		//        +----+---+----+  \
		//        | 3. |\  |\ 4.|   |
		//        +----+---+|---+   |
		//        |     \__\|   | 5.|
		//        |  2.         |   |
		//        +-------------+   |
		//         \             \  |
		//          \       6.    \ |
		//           \_____________\|
		//
		// so in total 16 planes

		float size12 = (float) size / 2;
		float size16 = (float) size / 6;

		float holeh12 = (float) holeHeight / 2;
		float holew12 = (float) holeWidth / 2;

		return new float[][]{
				// 0
				// upper front plane (1.) clock-wise
				{-size12,  size16, size16},
				{-size12,  size16, holeh12},
				{-size12, -size16, holeh12},
				{-size12, -size16, size16},

				// 4
				// lower front plane (2.) clock-wise
				{-size12, -size16, -size16},
				{-size12, -size16, -holeh12},
				{-size12,  size16, -holeh12},
				{-size12,  size16, -size16},

				// 8
				// left side of the hole (3.)
				{-size12, -holew12,  holeh12},
				{-size12, -holew12, -holeh12},
				{-size12, -size16,  -holeh12},
				{-size12, -size16,   holeh12},

				// 12
				// right side of the hole (4.)
				{-size12, size16,   holeh12},
				{-size12, size16,  -holeh12},
				{-size12, holew12, -holeh12},
				{-size12, holew12,  holeh12},

				// 16
				// left side of the whole cube
				{ size12, -size16,  size16},
				{-size12, -size16,  size16},
				{-size12, -size16, -size16},
				{ size12, -size16, -size16},

				// 20
				// right side of the whole cube
				{ size12, size16, -size16},
				{-size12, size16, -size16},
				{-size12, size16,  size16},
				{ size12, size16,  size16},

				// 24
				// roof of the whole cube
				{ size12,  size16, size16},
				{-size12,  size16, size16},
				{-size12, -size16, size16},
				{ size12, -size16, size16},

				// 28
				// floor of the whole cube
				{ size12, -size16, -size16},
				{-size12, -size16, -size16},
				{-size12,  size16, -size16},
				{ size12,  size16, -size16},

				// 32
				// left side of the inner tube
				{ size12, -holew12, -holeh12},
				{-size12, -holew12, -holeh12},
				{-size12, -holew12,  holeh12},
				{ size12, -holew12,  holeh12},

				// 36
				// right side of the inner tube
				{ size12, holew12,  holeh12},
				{-size12, holew12,  holeh12},
				{-size12, holew12, -holeh12},
				{ size12, holew12, -holeh12},

				// 40
				// roof of the inner tube
				{ size12, -holew12, holeh12},
				{-size12, -holew12, holeh12},
				{-size12,  holew12, holeh12},
				{ size12,  holew12, holeh12},

				// 44
				// floor of the inner tube
				{ size12,  holew12, -holeh12},
				{-size12,  holew12, -holeh12},
				{-size12, -holew12, -holeh12},
				{ size12, -holew12, -holeh12},

				// 48
				// upper back plane (1.) counter-clock-wise
				{size12, -size16, size16},
				{size12, -size16, holeh12},
				{size12,  size16, holeh12},
				{size12,  size16, size16},

				// 52
				// lower back plane (2.) counter-clock-wise
				{size12,  size16, -size16},
				{size12,  size16, -holeh12},
				{size12, -size16, -holeh12},
				{size12, -size16, -size16},

				// 56
				// left side of the hole at the back (3.)
				{size12, -size16,   holeh12},
				{size12, -size16,  -holeh12},
				{size12, -holew12, -holeh12},
				{size12, -holew12,  holeh12},

				// 60
				// right side of the hole at the back (4.)
				{size12, holew12,  holeh12},
				{size12, holew12, -holeh12},
				{size12, size16,  -holeh12},
				{size12, size16,   holeh12},
		};

	}

	@Override
	public CollisionShape createCollisionShape() {
		CompoundShape compound = new CompoundShape();
		float sizef = (float) size;
		float size13 = sizef / 3;
		float size16 = sizef / 6;
		float holeh12 = (float) holeHeight / 2;
		float holew12 = (float) holeWidth / 2;

		Transform trans = new Transform();
		trans.setIdentity();

		// top
		BoxShape top = new BoxShape(new Vector3f(sizef / 2, size13 / 2, (size16 - holeh12) / 2));
		trans.origin.set(new Vector3f(0, 0, holeh12 + (size16 - holeh12) / 2));
		compound.addChildShape(trans, top);

		// bottom
		BoxShape bottom = new BoxShape(new Vector3f(sizef / 2, size13 / 2, (size16 - holeh12) / 2));
		trans.origin.set(new Vector3f(0, 0, -holeh12 - (size16 - holeh12) / 2));
		compound.addChildShape(trans, bottom);

		// left
		BoxShape left = new BoxShape(new Vector3f(sizef / 2, (size16 - holew12) / 2, holeh12));
		trans = new Transform();
		trans.setIdentity();
		trans.origin.set(new Vector3f(0, -holew12 - (size16 - holew12) / 2, 0));
		compound.addChildShape(trans, left);

		// right
		BoxShape right = new BoxShape(new Vector3f(sizef / 2, (size16 - holew12) / 2, holeh12));
		trans = new Transform();
		trans.setIdentity();
		trans.origin.set(new Vector3f(0, holew12 + (size16 - holew12) / 2, 0));
		compound.addChildShape(trans, right);

		return compound;
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.holeobstacle;

/**
 * A cube with a CubeRendererHook attached
 */
public class HoleObstacleR extends HoleObstacle {

	public HoleObstacleR() {
		super();
		addHook(new HoleObstacleRendererHook(this));
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes.holeobstacle;

import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.holeobstacle.HoleObstacle.HoleObstacleHook;

/**
 * Defines the physics model of a Hole Obstacle
 */
public class HoleObstacleRigidBodyHook extends RigidBodyHook implements HoleObstacleHook {

	HoleObstacle holeobstacle;

	public HoleObstacleRigidBodyHook(Shape s) {
		super(s);
		holeobstacle = (HoleObstacle) shape;
		setSize(holeobstacle.getSize());
	}

	@Override
	public void setSize(double size) {
		colShape = holeobstacle.createCollisionShape();
	}

}

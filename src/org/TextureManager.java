/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org;

import java.awt.Graphics2D;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.media.opengl.GL;

/**
 * The TextureManager creates textures from files and manages the memory
 * that comes with it.
 */
public class TextureManager {

	private final GL gl;
	private Map<String, Integer> texMap;
	private static TextureManager instance;
	private int envMode = GL.GL_REPLACE;
	private Map<Integer, Integer> envModeMap = new HashMap<Integer, Integer>();

	/**
	 * Creates a new texturemanager
	 * @param gl the OpenGL context to create textures with
	 */
	private TextureManager(GL gl) {
		this.gl = gl;
		this.texMap = new HashMap<String, Integer>();

		this.envModeMap.put(GL.GL_REPLACE, GL.GL_TEXTURE0);
		this.envModeMap.put(GL.GL_MODULATE, GL.GL_TEXTURE0);
		this.envModeMap.put(GL.GL_BLEND, GL.GL_TEXTURE0);
		this.envModeMap.put(GL.GL_DECAL, GL.GL_TEXTURE0);

	}

	/**
	 * Retrieves the instance of the TextureManager
	 * @param gl The GL to get the instance from
	 * @return The TextureManager-Object
	 */
	public static TextureManager getInstance(GL gl) {
		if (TextureManager.instance == null) {
			TextureManager.instance = new TextureManager(gl);
		}
		return TextureManager.instance;
	}

	/**
	 * Gets the texture from the manager.
	 * When the given image string does not exist it will automatically attempt
	 * to create the texture given the filename of the image.
	 * @param image The image file to get as texture
	 * @return The openGL texture id
	 */
	public int getTexture(String image) {
		return getTexture(image, this.envMode);
	}

	/**
	 * Gets the texture from the manager.
	 * When the given image string does not exist it will automatically attempt
	 * to create the texture given the filename of the image.
	 * And with the given envmode.
	 * @param image The image file to get as texture
	 * @param envmode to load the texture from
	 * @return The openGL texture id
	 */
	private int getTexture(String image, int envMode) {
		handleTextureMode(envMode);
		Integer texId = this.texMap.get(image);
		if (texId == null) {
			IntBuffer ints = IntBuffer.allocate(1);
			gl.glGenTextures(1, ints);
			texId = ints.get(0);
			loadTexture(image, texId, envMode);
			this.texMap.put(image, texId);
		}
		return texId;
	}

	/**
	 * Sets the mode in which the textures will be loaded
	 * Eg. GL.GL_REPLACE for replacement of the texture color
	 * and GL.GL_MODULATE to modulate the texture color.
	 * @param mode The mode to render the textures
	 */
	public void setEnvMode(int mode) {
		if (mode != envMode) {
			clear();
		}
		envMode = mode;
		gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, envMode);
	}

	/**
	 * Removes all textures from the manager
	 */
	public void clear() {
		// Cleanup
		int[] texes = new int[this.texMap.size()];
		int i = 0;
		for (Integer t : this.texMap.values()) {
			texes[i++] = t.intValue();
		}

		gl.glDeleteTextures(texes.length, IntBuffer.wrap(texes));

		// Reinitialize
		this.texMap = new HashMap<String, Integer>();
	}

	/**
	 * Unloads the texture from the map
	 * @param image The image file to unload
	 */
	public void unloadTexture(String image) {
		Integer texId = this.texMap.get(image);
		if (texId == null) {
			return;
		}

		int[] texes = {texId};
		gl.glDeleteTextures(1, IntBuffer.wrap(texes));
	}

	/**
	 * Creates a new envmode for the texture if needed
	 * @param envMode the envmode to handle the texture mode from
	 */
	protected int handleTextureMode(int envMode) {

		Integer texMode = this.envModeMap.get(envMode);
		gl.glActiveTexture(texMode);
		gl.glEnable(GL.GL_TEXTURE_2D);

		gl.glClientActiveTexture(texMode);

		return texMode;
	}

	/**
	 * Loads the texture into the texturemanagers map
	 * @param image the image file to load
	 * @param index the index to load the texture from
	 */
	protected void loadTexture(String image, int index, int envMode) {
		String ds = System.getProperty("file.separator");
		String basedir = System.getProperty("user.dir") + ds + "assets" + ds + "images" + ds;

		image = image.replace("/", ds);

		BufferedImage bufferedImage = null;
		int w = 0;
		int h = 0;

		try {
			bufferedImage = ImageIO.read(new File(basedir + image));
			w = bufferedImage.getWidth();
			h = bufferedImage.getHeight();
		} catch (IOException e) {
			System.out.println("Failed to open: " + basedir + image);
			e.printStackTrace();
		}

		WritableRaster raster = Raster.createInterleavedRaster (
			DataBuffer.TYPE_BYTE, w, h, 4, null
		);
		ComponentColorModel colorModel = new ComponentColorModel (
			ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] {8,8,8,8}, true, false, ComponentColorModel.TRANSLUCENT,
			DataBuffer.TYPE_BYTE
		);
		BufferedImage dukeImg = new BufferedImage (colorModel, raster, false, null);

		Graphics2D g = dukeImg.createGraphics();
		g.drawImage(bufferedImage, null, null);
		DataBufferByte dukeBuf = (DataBufferByte) raster.getDataBuffer();
		byte[] dukeRGBA = dukeBuf.getData();
		ByteBuffer bb = ByteBuffer.wrap(dukeRGBA);
		bb.position(0);
		bb.mark();

		gl.glBindTexture(GL.GL_TEXTURE_2D, index);
		gl.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, 1);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_GENERATE_MIPMAP, GL.GL_FALSE);
		gl.glTexImage2D (GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, w, h, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, bb);
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org;
import java.util.ArrayList;

import javax.media.opengl.GL;

public class VisibleObjectList<T extends VisibleObject> extends ArrayList<T> implements VisibleObject {

	private static final long serialVersionUID = -3509946989071222198L;

	@Override
	public void display(GL gl, int dt) {

		// Display all the visible objects of MazeRunner.
		// convert to a array to prevent "ConcurrentModificationException"
		VisibleObject[] visObjects = new VisibleObject[size()];
		visObjects = toArray(visObjects);
		for (int i = 0; i < visObjects.length; i++) {
			visObjects[i].display(gl, dt);
		}

	}

}

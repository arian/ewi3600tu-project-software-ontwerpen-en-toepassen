/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Util {

	/**
	 * Converts a database result as a static defaultTableModel
	 * @param res The ResultSet to convert
	 * @param close When true the resultSet is closed when done.
	 * @return The newly created defaultTableModel
	 */
	public static DefaultTableModel toTableModel(ResultSet res, boolean close) {
		try {
			int cc = res.getMetaData().getColumnCount();

			String[] cols = new String[cc];
			for (int i = 0; i < cc; i++) {
				cols[i] = res.getMetaData().getColumnLabel(i+1);
			}

			ArrayList<String[]> rowList = new ArrayList<String[]>();
			while (res.next()) {
				String[] row  = new String[cc];
				for (int i = 0; i < cc; i++) {
					row[i] = res.getString(i+1);
				}
				rowList.add(row);
			}

			if (close) {
				res.close();
			}

		    Object[][] data = new Object[rowList.size()][cc];
		    data = rowList.toArray(data);

		    return new DefaultTableModel(data, cols);

		} catch(SQLException e) {
			e.printStackTrace();
		}

		return new DefaultTableModel();
	}

	/**
	 * Creates a JTable from a resultset
	 * @param res res The resultset to convert
	 * @param close  When true the resultSet is closed when done.
	 * @return The newly created JTable
	 */
	public static JTable toJTable(ResultSet res, boolean close) {
		return new JTable(Util.toTableModel(res, close));
	}

	public static JTable toJTable(ResultSet res) {
		return Util.toJTable(res, true);
	}

	public static DefaultTableModel toTableModel(ResultSet res) {
		return Util.toTableModel(res, true);
	}

}

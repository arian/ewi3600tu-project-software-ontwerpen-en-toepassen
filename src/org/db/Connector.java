/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.db;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.TunnelRunner;

public class Connector {

	private static Connector instance;
	private Connection conn;

	/**
	 * Constructs an Connector obj with a SQL connection
	 * @throws ClassNotFoundException
	 */
	private Connector() throws ClassNotFoundException {
		try {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:mazebase.db");
			this.buildTables(TunnelRunner.resetDB);
		} catch (SQLException e) {
			System.out.println("Aiai, geen verbinding met de database");
			e.printStackTrace();
		}
	}

	/**
	 * Gets the instance of the Connector object
	 * @return The current instance for the Connector obj.
	 * @throws ClassNotFoundException
	 */
	public static Connector getInstance() throws ClassNotFoundException {
		if (Connector.instance == null) {
			Connector.instance = new Connector();
		}

		return Connector.instance;
	}


	/**
	 * Executes the given query with the given data
	 * @param sql The SQL query to execute
	 * @param data The data to bind to the SQL placeholders
	 * @return The ResultSet of the given query
	 */
	public ResultSet query(String sql, Object[] data) {
		try {
			PreparedStatement st = conn.prepareStatement(sql + ";");
			bindData(st, data);
			ResultSet res = st.executeQuery();
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Executes the given Query
	 * @param sql The SQL query to execute
	 * @return The resultSet of the query
	 */
	public ResultSet query(String sql) {
		return query(sql, null);
	}

	/**
	 * Executes the given query without expecting a result
	 * @param sql The SQL query to execute
	 * @param data The data to bind to the SQL placeholders
	 * @return True if the query was successful, false otherwise
	 */
	public boolean noResultQuery(String sql, Object[] data) {
		try {
			PreparedStatement st = conn.prepareStatement(sql);
			bindData(st, data);
			st.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Executes the given query without expecting a result
	 * @param sql The SQL query to execute.
	 * @return True if the query was successful, false otherwise
	 */
	public boolean noResultQuery(String sql) {
		return this.noResultQuery(sql, null);
	}

	/**
	 * Builds the table definitions from the database
	 * @param hard Drop all tables before building them.
	 */
	protected void buildTables(boolean hard) {

		HashMap<String, String> tableMap = new HashMap<String, String>();

		// LevelTable
		tableMap.put("levels", "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"start_x INTEGER NOT NULL," +
				"start_y INTEGER NOT NULL," +
				"start_z INTEGER NOT NULL," +
				"start_rot_w FLOAT NOT NULL," +
				"start_rot_x INTEGER NOT NULL," +
				"start_rot_y INTEGER NOT NULL," +
				"start_rot_z INTEGER NOT NULL," +
				"name VARCHAR(50)," +
				"leveldata VARCHAR(50000))"
		);
		tableMap.put("highscores", "(\"id\" INTEGER PRIMARY KEY AUTOINCREMENT," +
			    " \"level_id\" INTEGER, " +
			    " \"time\" INTEGER NOT NULL, " +
			    " \"name\" VARCHAR, " +
			    " FOREIGN KEY (level_id) REFERENCES levels(id))"
		);


		// Adding the tables to the database
		for (Map.Entry<String, String> entry : tableMap.entrySet()) {
			String table = entry.getKey();
			String defs = entry.getValue();

			if (hard) {
				this.noResultQuery("DROP TABLE IF EXISTS " + table);
				this.noResultQuery("CREATE TABLE " + table + " " + defs);
			} else {
				this.noResultQuery("CREATE TABLE IF NOT EXISTS " + table + " " + defs);
			}
		}
	}

	/**
	 * Binds the data to a prepared statement
	 * @param st The statement that needs data bound to
	 * @param data The data to bind. Integers bind to ints, and String bind to strings etc.
	 * @throws SQLException
	 */
	protected void bindData(PreparedStatement st, Object[] data) throws SQLException {
		if (data == null) {
			return;
		}

		for (int i = 0; i < data.length; i++) {

			if (data[i] instanceof Integer) {
				st.setInt(i + 1, (Integer) data[i]);
			}
			else if (data[i] instanceof Float) {
				st.setFloat(i + 1, (Float) data[i]);
			}
			else if (data[i] instanceof Double) {
				st.setDouble(i + 1, (Double) data[i]);
			}
			else if (data[i] instanceof Date) {
				st.setDate(i + 1, (Date) data[i]);
			}
			else if (data[i] instanceof String) {
				st.setString(i + 1, (String) data[i]);
			}
			else {
				st.setObject(i + 1, data[i]);
			}
		}
	}


}

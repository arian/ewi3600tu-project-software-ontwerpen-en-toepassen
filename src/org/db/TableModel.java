/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.db;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public class TableModel extends AbstractTableModel {

	private static final long serialVersionUID = -6168891995588343487L;

	public TableModel(String table, ArrayList<String> columns) {

		Object[] queryData = new Object[columns.size() + 1];
		String sql = "SELECT ";
		int i = 0;
		for (String col  : columns) {
			sql += "? , ";
			queryData[i++] = col;
		}
		sql = sql.substring(0, sql.length() - 3) + " FROM ?";
		queryData[i++] = table;

		try {
			Connector.getInstance().query(sql, queryData);
		} catch (ClassNotFoundException e) {
		}
	}

	@Override
	public int getColumnCount() {
		return 0;
	}

	@Override
	public int getRowCount() {
		return 0;
	}

	@Override
	public Object getValueAt(int row, int column) {
		return null;
	}

}

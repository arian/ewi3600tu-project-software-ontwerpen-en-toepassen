/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes2d;

public class Point2d {

	private float x;
	private float y;

	/**
	 * @param x The x coordinate to construct the point with
	 * @param y The y coordinate to construct the point with
	 */
	public Point2d(float x, float y) {
		this.setX(x);
		this.setY(y);
	}

	/**
	 * @param point Another point to clone the coordinates from.
	 */
	public Point2d(Point2d point) {
		this(point.getX(), point.getY());
	}

	/**
	 * Translates the point;
	 * @param x The amount to shift the x-coordinate of the point with
	 * @param y The amount to shift the y-coordinate of the point with
	 * @return The current point
	 */
	public Point2d translate(float x, float y)  {
		this.x += x;
		this.y += y;
		return this;
	}


	/**
	 * Rotates the point around the origin
	 * @param theta The amount in radians to rotate all points of the shape with as origin (0, 0)
	 * @return The current shape (Point2d)
	 */
	public Point2d rotate(double theta) {

		float x = this.x;
		float y = this.y;

		this.x = (float)  Math.cos(theta) * x + (float) Math.sin(theta) * y;
		this.y = (float) -Math.sin(theta) * x + (float) Math.cos(theta) * y;

		return this;
	}

	/**
	 * Scales the point from the origin
	 * @param factor the amount to multiply the x and y points by
	 * @return The current point (Point2d)
	 */
	public Point2d scale(float factor) {
		return scale(factor, factor);
	}

	/**
	 * Scales the point from the origin
	 * @param xFactor factor to multiply x by
	 * @param yFactor factor to multiply y by
	 * @return The current point (Point2d)
	 */
	public Point2d scale(float xFactor, float yFactor) {
		this.x *= xFactor;
		this.y *= yFactor;
		return this;
	}

	/**
	 * @return The x coordinate of the point
	 */
	public float getX() {
		return x;
	}

	/**
	 * @param x The x to set the y coordinate of the point to
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * @return The y coordinate of the point
	 */
	public float getY() {
		return y;
	}

	/**
	 * @param y The y to set the y coordinate of the point to
	 */
	public void setY(float y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "(" + this.x + ";" + this.y + ")";
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes2d;

public class Color {

	private float r;
	private float g;
	private float b;
	private float a = 1;

	/**
	 * Constructs a new color
	 * @param rgb The RGB color value of the color
	 * @param a The alpha component with 0 as fully transparent and 1 fully visible
	 */
	public Color(int rgb, float a) {
		this.r = ((rgb >> 16) & 0xff) / 255f;
		this.g = ((rgb >> 8) & 0xff) / 255f;
		this.b = ((rgb) & 0xff) / 255f;
		this.a = a;
	}

	/**
	 * Constructs a new color
	 * @param r The red component with 0 the least intensity and 1 the highest intensity
	 * @param g The green component with 0 the least intensity and 1 the highest intensity
	 * @param b The blue component with 0 the least intensity and 1 the highest intensity
	 */
	public Color(float r, float g, float b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	/**
	 * Constructs a new color
	 * @param r The red component with 0 the least intensity and 1 the highest intensity
	 * @param g The green component with 0 the least intensity and 1 the highest intensity
	 * @param b The blue component with 0 the least intensity and 1 the highest intensity
	 * @param a The alpha component with 0 as fully transparent and 1 fully visible
	 */
	public Color(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	/**
	 * Constructs a new color
	 * <p>This allows setting the color with an array of color components. The
	 * indices map to red, green, blue and alpha respectively with the alpha
	 * component being optional. An value of 0 at a given index will have the
	 * least color intensity and a value of 1 will have the highest intensity.
	 * For the alpha channel 0 means fully transparent and 1 means fully
	 * visible.</p>
	 * @param colors as an float array: {red, green, blue}
	 */
	public Color(float[] colors) {
		if (colors.length == 4) {
			a = colors[3];
		}

		this.r = colors[0];
		this.g = colors[1];
		this.b = colors[2];
	}


	/**
	 * Creates a new color, cloning another color
	 * @param color
	 */
	public Color(Color color) {
		this.a = color.getA();
		this.r = color.getR();
		this.g = color.getG();
		this.b = color.getB();
	}

	/**
	 * Gets the color as an array of floats
	 * @return The color as an array of floats with as indices red, green,
	 * blue and alpha respectively.
	 */
	public float[] asFloat() {
		float[] res = {r, g, b, a};
		return res;
	}

	/**
	 * apply(Gl gl)
	 * <p>Applies the color to a given GL handler</p>
	 * @param gl  gl to apply color to
	 */
	public void apply(javax.media.opengl.GL gl) {
		gl.glColor4f(r, g, b, a);
	}

	// A few functions to alter the current color, not that the class is not
	// immutable

	/**
	 * Lighten(float amount)
	 * <p>Increments the color with amount (makes it shift to white) </p>
	 * @param amount Increments color values by amount
	 */

	public void lighten(float amount) {
		this.r += amount;
		this.g += amount;
		this.b += amount;
	}

	/**
	 * Darken(float amount)
	 * <p>Decrements the color with amount (makes it shift to black) </p>
	 * @param amount Decrements color values by amount
	 */
	public void darken(float amount) {
		this.r -= amount;
		this.g -= amount;
		this.b -= amount;
	}

	/**
	 * fadeIn(float amount)
	 * <p>Increments alpha value of the color by amount </p>
	 * @param amount Amount to increment alpha value by
	 */
	public void fadeIn(float amount) {
		this.a += amount;
	}

	/**
	 * fadeOut(float amount)
	 * <p>Decrements alpha value of the color by amount </p>
	 * @param amount Amount to decrement alpha value by
	 */
	public void fadeOut(float amount) {
		this.a -= amount;
	}

	/**
	 * Returns the color as java.awt.Color color
	 * @return The current color in the awt color
	 */
	public java.awt.Color asAWTColor() {
		return new java.awt.Color(r, g, b, a);
	}

	/**
	 * Gets the red component
	 * @return the red component ranging from 0 to 1
	 */
	public float getR() {
		return r;
	}

	/**
	 * Sets the red component
	 * @param r the red component to set
	 * @return the current color
	 */
	public Color setR(float r) {
		this.r = r;
		return this;
	}

	/**
	 * Gets the green component
	 * @return the green component ranging from 0 to 1
	 */
	public float getG() {
		return g;
	}

	/**
	 * Sets the green component
	 * @param g the green component to set
	 * @return the current color
	 */
	public Color setG(float g) {
		this.g = g;
		return this;
	}

	/**
	 * Gets the blue component
	 * @return the blue component ranging from 0 to 1
	 */
	public float getB() {
		return b;
	}

	/**
	 * Sets the blue component
	 * @param b the blue component to set
	 * @return the current color
	 */
	public Color setB(float b) {
		this.b = b;
		return this;
	}

	/**
	 * Gets the alpha channel
	 * @return the alpha channel ranging from 0 to 1
	 */
	public float getA() {
		return a;
	}

	/**
	 * Sets the alpha channel
	 * @param a the alpha channel to set
	 * @return the current color
	 */
	public Color setA(float a) {
		this.a = a;
		return this;
	}

}

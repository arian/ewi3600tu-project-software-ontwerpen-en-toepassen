/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes2d;

public class Rectangle2d {

	private float x;
	private float y;
	private float width;
	private float height;

	/**
	 * @param x the most left coordinate of the rectangle
	 * @param y the most lower coordinate of the rectangle
	 * @param width the width of the rectangle
	 * @param height the height of the rectangle
	 */
	public Rectangle2d(float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		// Make sure the lengths are always positive
		if (this.width < 0) {
			this.x += this.width;
			this.width = -this.width;
		}
		if (this.height < 0) {
			this.y += this.height;
			this.height = -this.height;
		}
	}

	/**
	 * Creates a rectangle2d based on the Java AWT rectangular shape
	 * @param rect The java AWT rectangular shape to use
	 */
	public Rectangle2d(java.awt.geom.RectangularShape rect) {
		this((float) rect.getX(), (float) rect.getY(), (float) rect.getWidth(), (float) rect.getHeight());
	}

	/**
	 * @param point The point where the x, y starts
	 * @param width the width of the rectangle
	 * @param height the height of the rectangle
	 */
	public Rectangle2d(Point2d point, float width, float height) {
		this(point.getX(), point.getY(), width, height);
	}

	/**
	 * @param rect Anther rectangle to clone the coordinates from
	 */
	public Rectangle2d(Rectangle2d rect) {
		this(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
	}

	/**
	 * @param p1 The lower left corner of the rectangle
	 * @param p2 The upper right corner of the rectangle
	 */
	public Rectangle2d(Point2d p1, Point2d p2) {
		this(p1.getX(), p1.getY(), p1.getX()-p2.getX(), p1.getY()-p2.getY());
	}


	/**
	 * @return the center of the rectangle
	 */
	public Point2d getCenter() {
		return new Point2d(this.getX() + (this.getWidth()/2), this.getY() + (this.getHeight()/2));
	}

	/**
	 * @return The rectangle which center has been altered
	 */
	public Rectangle2d setCenter(Point2d point) {

		Point2d cc = this.getCenter();
		this.translate(point.getX() - cc.getX(), point.getY() - cc.getY());

		return this;
	}

	/**
	 * Translates the rectangle by (dx, dy)
	 * @param dx the amount the rectangle should be translated right
	 * @param dy the amount the rectangle should be translated up
	 * @return The rectangle
	 */
	public Rectangle2d translate(float dx, float dy) {
		this.x += x;
		this.y += y;
		return this;
	}

	/**
	 * Determines whether the given point is in the rectangle
	 * @param x The x coordinate of the point to determine
	 * @param y The y coordinate of the point to determine
	 * @return True if the rectangle contains the point
	 */
	public boolean contains(float x, float y) {
		return (
			x >= this.x && x <= (this.x + width) &&
			y >= this.y && y <= (this.y + height)
		);
	}

	/**
	 * Determines whether the given point is in the rectangle
	 * @param point The point to determine
	 * @return True if the rectangle contains the point
	 */
	public boolean contains(Point2d point) {
		return contains(point.getX(), point.getY());
	}

	// Getters and setters
	/**
	 * @return The x coordinate of the rectangle
	 */
	public float getX() {
		return x;
	}

	/**
	 * Sets a new x coordinate of the rectangle
	 * @param x
	 */
	public Rectangle2d setX(float x) {
		this.x = x;
		return this;
	}

	/**
	 * @return The y coordinate of the rectangle
	 */
	public float getY() {
		return y;
	}

	/**
	 * Sets a new y coordinate of the rectangle
	 * @param y The y coordinate of the rectangle
	 */
	public Rectangle2d setY(float y) {
		this.y = y;
		return this;
	}

	/**
	 * @return The width of the rectangle
	 */
	public float getWidth() {
		return width;
	}

	/**
	 * Sets the height of the rectangle
	 * @param width the new Width of the rectangle
	 */
	public Rectangle2d setWidth(float width) {

		if (width < 0) {
			this.x += width;
			width = -width;
		}
		this.width = width;
		return this;
	}

	/**
	 * @return The height of the current rectangle
	 */
	public float getHeight() {
		return height;
	}

	/**
	 * Sets the height of the rectangle
	 * @param height The new height of the rectangle
	 */
	public Rectangle2d setHeight(float height) {
		if (height < 0) {
			this.y += height;
			height = -height;
		}
		this.height = height;
		return this;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Rectangle2d)) {
			return false;
		}
		Rectangle2d sOther = (Rectangle2d) other;

		if (this.getX() == sOther.getX()
				&& this.getY() == sOther.getY()
				&& this.getWidth() == sOther.getWidth()
				&& this.getHeight() == sOther.getWidth()) {
			return true;
		}

		return false;
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes2d;

import javax.media.opengl.GL;

public class Point extends Shape2d {

	/**
	 * @param point The point to draw the point at
	 */
	public Point(Point2d point) {

		this.points = new Point2d[1];
		this.points[0] = point;

	}

	/**
	 * @param x The x coordinate of the point to draw at
	 * @param y The y coordinate of the point to draw at
	 */
	public Point(float x, float y) {
		this(new Point2d(x, y));
	}

	@Override
	public void display(GL gl, int dt) {
		this.color.apply(gl);
		gl.glLineWidth(this.strokeWidth);
		gl.glBegin(GL.GL_POINTS);
		gl.glVertex2f(this.points[0].getX(), this.points[0].getY());
		gl.glEnd();
	}

}

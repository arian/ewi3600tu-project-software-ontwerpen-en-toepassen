/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes2d;

import javax.media.opengl.GL;

public class Triangle extends Shape2d {

	/**
	 * @param p1 The first point of the triangle
	 * @param p2 The second point of the triangle
	 * @param p3 The third point of the triangle
	 */
	public Triangle(Point2d p1, Point2d p2, Point2d p3) {

		this.points = new Point2d[3];
		this.points[0] = p1;
		this.points[1] = p2;
		this.points[2] = p3;

	}

	/**
	 * @param x1 The x coordinate of the first point of the triangle
	 * @param y1 The y coordinate of the first point of the triangle
	 * @param x2 The x coordinate of the second point of the triangle
	 * @param y2 The y coordinate of the second point of the triangle
	 * @param x3 The x coordinate of the third point of the triangle
	 * @param y3 The y coordinate of the third point of the triangle
	 */
	public Triangle(float x1, float y1, float x2, float y2, float x3, float y3) {
		this(new Point2d(x1, y2), new Point2d(x2, y2), new Point2d(x3, y3));
	}

	@Override
	public void display(GL gl, int dt) {
		this.color.apply(gl);
		gl.glLineWidth(this.strokeWidth);
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex2f(this.points[0].getX(), this.points[0].getY());
		gl.glVertex2f(this.points[1].getX(), this.points[1].getY());
		gl.glVertex2f(this.points[2].getX(), this.points[2].getY());
		gl.glEnd();
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes2d;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.sun.opengl.util.j2d.TextRenderer;

public class FontManager {

	private Map<String, Font> fontMap;
	private Map<FontProps, TextRenderer> rendererMap;
	private static FontManager instance;

	/**
	 * A class that represent the font properties of a object.
	 */
	protected static class FontProps {
		public int mode;
		public int size;
		public String font;

		/**
		 * Creates a new FontProps object
		 * @param font the string of the font to create
		 * @param mode the font rendering mode. See also java.awt.Font for details
		 * @param size the vertical size of the font
		 */
		public FontProps(String font, int mode, int size) {
			this.font = font;
			this.mode = mode;
			this.size = size;
		}

		@Override
		public boolean equals(Object other) {
			if (other instanceof FontProps) {
				FontProps fpOther = (FontProps) other;
				if (fpOther.mode == this.mode && fpOther.size == this.size && this.font.equals(fpOther.font)) {
					return true;
				}

			}
			return false;
		}

		@Override
		public int hashCode() {
			// Probably very buggy implementation, but most likely to work
			return this.font.hashCode() + this.mode * 1000 + this.size;
		}

		@Override
		public String toString() {
			return "FontManager.FontProps[font=" + font + ",mode=" + mode + ",size=" + size + "]";
		}

	}

	/**
	 * Creates a new FontManager
	 */
	private FontManager() {
		fontMap = new HashMap<String, Font>();
		rendererMap = new HashMap<FontProps, TextRenderer>();
	}

	/**
	 * Retrieves the instance of the FontManager
	 * @return The FontManager-Object
	 */
	public static FontManager getInstance() {
		if (FontManager.instance == null) {
			FontManager.instance = new FontManager();
		}
		return FontManager.instance;
	}

	/**
	 * Gets the Font from the manager
	 * When the given font does not exist it will automatically attempt
	 * to create the font given the filename of the font.
	 * @param fontFileName the filename of the font
	 * @param mode The font rendering mode. See also: java.awt.Font
	 * @param size The size of the text (measured in pixel height)
	 * @return The current font
	 */
	public Font getFont(String fontFileName, int mode, int size) {
		Font fnt = this.fontMap.get(fontFileName);

		if (fnt == null) {
			if (!fontFileName.endsWith(".ttf")) {
				fnt = new Font(fontFileName, 0, 20);
			}
			else {
				fnt = this.loadFont(fontFileName);
			}
			this.fontMap.put(fontFileName, fnt);
		}
		return fnt.deriveFont(mode, size);
	}

	/**
	 * Gets a text renderer from the manager.
	 * When the given font does not exist it will automatically attempt
	 * to create the font given the filename of the font.
	 * If no matching renderer is created it will create a new one,
	 * otherwise it will create a new one
	 * @param fontFileName the filename of the font
	 * @param mode The font rendering mode. See also: java.awt.Font
	 * @param size The size of the text (measured in pixel height)
	 * @return The current font
	 */
	public TextRenderer getRenderer(String fontFileName, int mode, int size) {
		FontProps props = new FontProps(fontFileName, mode, size);
		TextRenderer ren = this.rendererMap.get(props);

		if (ren == null) {
			ren = new TextRenderer(this.getFont(fontFileName, mode, size));
			this.rendererMap.put(props, ren);
		}
		return ren;
	}

	/**
	 * Removes all fonts and renderers from the manager.
	 * Used for cleanup
	 */
	public void clear() {
		// Reinitialize
		fontMap = new HashMap<String, Font>();
		rendererMap = new HashMap<FontProps, TextRenderer>();
	}

	/**
	 * Removes all stored renderer objects
	 */
	public void clearRenderers() {
		// Reinitialize
		rendererMap.clear();
	}

	/**
	 * Unloads the font from the map
	 * @param fontFileName The font file to unload
	 */
	public void unloadFont(String fontFileName) {
		fontMap.remove(fontFileName);
	}

	/**
	 * Loads the font into the FontManagers
	 * @param image the image file to load
	 * @param index the index to load the texture from
	 */
	protected Font loadFont(String fontFileName) {

		try {

			String ds = System.getProperty("file.separator");
			String basedir = System.getProperty("user.dir") + ds + "assets" + ds + "fonts" + ds;
			return Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(basedir + fontFileName));

		} catch (IOException e) {
			e.printStackTrace();
		} catch (FontFormatException e) {
			e.printStackTrace();
		}
		return null;
	}

}

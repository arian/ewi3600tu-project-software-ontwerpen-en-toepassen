/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes2d;

import javax.media.opengl.GL;

public class Line extends Shape2d {

	/**
	 * @param p1 The start point of the line
	 * @param p2 The end point of the line
	 */
	public Line(Point2d p1, Point2d p2) {

		this.points = new Point2d[2];
		this.points[0] = p1;
		this.points[1] = p2;

	}

	/**
	 * @param x1 The x coordinate of the start point of the line
	 * @param y1 The y coordinate of the start point of the line
	 * @param x2 The x coordinate of the end point of the line
	 * @param y2 The y coordinate of the end point of the line
	 */
	public Line(float x1, float y1, float x2, float y2) {
		this(new Point2d(x1, y1), new Point2d(x2, y2));
	}

	/* (non-Javadoc)
	 * @see org.shapes2d.Shape2d#display(javax.media.opengl.GL)
	 */
	@Override
	public void display(GL gl, int dt) {
		this.color.apply(gl);
		gl.glLineWidth(this.strokeWidth);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex2f(this.points[0].getX(), this.points[0].getY());
		gl.glVertex2f(this.points[1].getX(), this.points[1].getY());
		gl.glEnd();
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes2d;

import javax.media.opengl.GL;

import org.TextureManager;

public class Tetragon extends Shape2d {

	private String texture;

	/**
	 * Construct a new Tetragon
	 * @param p1 The first point of the Tetrahegon
	 * @param p2 The second point of the Tetrahegon
	 * @param p3 The third point of the Tetrahegon
	 * @param p4 The fourth point of the Tetrahegon
	 */
	public Tetragon(Point2d p1, Point2d p2, Point2d p3, Point2d p4) {

		this.points = new Point2d[4];
		this.points[0] = p1;
		this.points[1] = p2;
		this.points[2] = p3;
		this.points[3] = p4;

	}

	/**
	 * Construct a new Tetragon
	 * @param x1 The x coordinate of the first point of the Tetrahegon
	 * @param y1 The y coordinate of the first point of the Tetrahegon
	 * @param x2 The x coordinate of the second point of the Tetrahegon
	 * @param y2 The y coordinate of the second point of the Tetrahegon
	 * @param x3 The x coordinate of the third point of the Tetrahegon
	 * @param y3 The y coordinate of the third point of the Tetrahegon
	 * @param x4 The x coordinate of the fourth point of the Tetrahegon
	 * @param y4 The y coordinate of the fourth point of the Tetrahegon
	 */
	public Tetragon(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
		this(new Point2d(x1, y2), new Point2d(x2, y2), new Point2d(x3, y3), new Point2d(x4, y4));
	}

	/**
	 * Construct a new Tetragon
	 * @param tetr another tetragon to copy the points from
	 */
	public Tetragon(Tetragon tetr) {
		this(tetr.getPoint(0), tetr.getPoint(1), tetr.getPoint(2), tetr.getPoint(3));
	}

	/**
	 * @param rect Set the points to match the gived rectangle
	 * @return The newly setted points
	 */
	public Tetragon setRectangle(Rectangle2d rect) {

		this.points[0] = new Point2d(rect.getX(), rect.getY());
		this.points[1] = new Point2d(rect.getX() + rect.getWidth(), rect.getY());
		this.points[2] = new Point2d(rect.getX() + rect.getWidth(), rect.getY() + rect.getHeight());
		this.points[3] = new Point2d(rect.getX(), rect.getY() + rect.getHeight());

		return this;
	}

	/**
	 * Helper method to quickly create a square
	 * @param corner The point where the lower left corner of the square;
	 * @param size The size of the square (in pixels usually)
	 * @return The newly create tetragon according to the given points
	 */
	public static Tetragon square(Point2d corner, float size) {

		return new Tetragon(
			corner.getX(), corner.getY(),
			corner.getX() + size, corner.getY(),
			corner.getX() + size, corner.getY() + size,
			corner.getX(), corner.getY() + size
		);
	}

	/**
	 * Helper method to quickly create a square
	 * @param x The x coordinate of the lower left corner of the square
	 * @param y The y coordinate of the lower left corner of the square
	 * @param size The size of the square (in pixels usually)
	 * @return The newly create tetragon according to the given points
	 */
	public static Tetragon square(float x, float y, float size) {
		return new Tetragon(
				x, y,
				x + size, y,
				x + size, y + size,
				x, y + size
		);
	}

	/**
	 * Helper method to quickly create a rectangle
	 * @param lowerLeft
	 * @param upperRight
	 * @return The newly create tetragon according to the given points
	 */
	public static Tetragon rectangle(Point2d lowerLeft, Point2d upperRight) {

		return new Tetragon(
			lowerLeft.getX(), lowerLeft.getY(),
			upperRight.getX(), lowerLeft.getY(),
			upperRight.getX(), upperRight.getY(),
			lowerLeft.getX(), upperRight.getY()
		);
	}

	/**
	 * Helper method to quickly create a rectangle
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @return The newly created tetragon according to the given points
	 */
	public static Tetragon rectangle(float x, float y, float width, float height) {
		return new Tetragon(
				x, y,
				x + width, y,
				x + width, y + height,
				x, y + height
		);
	}

	/**
	 * @param rect The rectangle to transform into tetrahegon
	 * @return The newly created tetragon according to the given points
	 */
	public static Tetragon rectangle(Rectangle2d rect) {
		return Tetragon.rectangle(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
	}

	public void setPoints(Point2d lowerLeft, Point2d upperRight) {

		this.points[0].setX(lowerLeft.getX());
		this.points[0].setY(lowerLeft.getY());

		this.points[1].setX(upperRight.getX());
		this.points[1].setY(lowerLeft.getY());

		this.points[2].setX(upperRight.getX());
		this.points[2].setY(upperRight.getY());

		this.points[3].setX(lowerLeft.getX());
		this.points[3].setY(upperRight.getY());

	}

	/* (non-Javadoc)
	 * @see org.shapes2d.Shape2d#display(javax.media.opengl.GL)
	 */
	@Override
	public void display(GL gl, int dt) {
		this.color.apply(gl);

		gl.glLineWidth(this.strokeWidth);

		if (texture != null) {
			 gl.glEnable(GL.GL_TEXTURE_2D);
			 gl.glBindTexture (GL.GL_TEXTURE_2D, TextureManager.getInstance(gl).getTexture(texture));
		}

		gl.glBegin(GL.GL_QUADS);

		if (texture != null) {gl.glTexCoord2d (0, 1); }
		gl.glVertex2f(this.points[0].getX(), this.points[0].getY());

		if (texture != null) {gl.glTexCoord2d (1, 1); }
		gl.glVertex2f(this.points[1].getX(), this.points[1].getY());

		if (texture != null) {gl.glTexCoord2d (1, 0); }
		gl.glVertex2f(this.points[2].getX(), this.points[2].getY());

		if (texture != null) {gl.glTexCoord2d (0, 0); }
		gl.glVertex2f(this.points[3].getX(), this.points[3].getY());

		gl.glEnd();

		if (texture != null) {
			gl.glDisable(GL.GL_TEXTURE_2D);
		}
	}

	/**
	 * Gets the texture file of the tetragon
	 * @return the current texture file
	 */
	public String getTexture() {
		return texture;
	}

	/**
	 * Sets the texture of the tetragon
	 * @param texture The texture file to apply
	 * @return The current tetragon
	 */
	public Tetragon setTexture(String texture) {
		this.texture = texture;
		return this;
	}

}

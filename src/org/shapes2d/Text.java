/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes2d;

import javax.media.opengl.GL;

import com.sun.opengl.util.j2d.TextRenderer;

public class Text extends Shape2d {

	// Constants
	public final static byte PLAIN = 0;
	public final static byte BOLD = 1;
	public final static byte ITALIC = 2;

	// Defaultvalues
	public static String defaultFont = "arial";
	public static int defaultSize = 20;
	public static Color defaultColor = new Color(0, 0, 0);
	public static byte defaultMode = 0;
	public static float defaultAlign = 0.5f;
	public static float defaultVerticalAlign = 0.5f;

	private String font;
	private int size;
	private String text;
	private byte mode;
	private float align;
	private float verticalAlign;

	private Shape2d parent;

	/**
	 * Construct a text at the given point
	 * @param point The point to take as origin for the text placement
	 * @param text The string to render
	 */
	public Text(Point2d point, String text) {
		this.text			= text;
		this.font			= Text.defaultFont;
		this.size			= Text.defaultSize;
		this.color			= Text.defaultColor;
		this.mode			= Text.defaultMode;
		this.align			= Text.defaultAlign;
		this.verticalAlign	= Text.defaultVerticalAlign;

		this.points = new Point2d[] {point};
	}

	/**
	 * Construct a text as origin the center of the given shape. The text will move along with the shape.
	 * @param parent The shape to center at
	 * @param text The string to render
	 */
	public Text(Shape2d parent, String text) {
		this.text			= text;
		this.font			= Text.defaultFont;
		this.size			= Text.defaultSize;
		this.color			= Text.defaultColor;
		this.mode			= Text.defaultMode;
		this.align			= Text.defaultAlign;
		this.verticalAlign	= Text.defaultVerticalAlign;

		this.parent = parent;
		this.points = new Point2d[] {parent.getCenter()};
	}

	@Override
	public void display(GL gl, int dt) {
		// Short way out
		if ("".equals(this.text)) {
			return;
		}

		TextRenderer renderer = FontManager.getInstance().getRenderer(this.font, this.mode, this.size);

		if (parent != null) {
			this.points = new Point2d[] {parent.getCenter()};
		}

		float x = this.points[0].getX();
		float y = this.points[0].getY();

		Rectangle2d tBounds = new Rectangle2d(renderer.getBounds(this.text));

		if (this.align != 0) {
			x -= tBounds.getWidth() * this.align;
		}
		if (this.verticalAlign != 0) {
			y -= tBounds.getHeight() * this.verticalAlign;
		}

		renderer.begin3DRendering();
		renderer.setColor(color.asAWTColor());
		renderer.draw3D(this.text, (int) x, (int) y, 0, 1);
		renderer.end3DRendering();
	}

	/**
	 * Gets the current Font
	 * @return the current font
	 */
	public String getFont() {
		return font;
	}

	/**
	 * Sets the current Font
	 * @param font the font to set
	 * @return the current Object
	 */
	public Text setFont(String font) {
		this.font = font;
		return this;
	}

	/**
	 * Gets the current text size
	 * @return the current text size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Sets the text size
	 * @param size the size in vertical pixels to set
	 * @return The current object
	 */
	public Text setSize(int size) {
		this.size = size;
		return this;
	}

	/**
	 * Gets the current text
	 * @return The current text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Set the text
	 * @param text the new text
	 * @return The current object
	 */
	public Text setText(String text) {
		this.text = text;
		return this;
	}

	/**
	 * Gets the current mode. Use the bitflags BOLD and ITALIC to extract
	 * the rendering mode
	 * @return The current mode
	 */
	public byte getMode() {
		return mode;
	}

	/**
	 * Sets the current rendering mode
	 * @param mode to set use the bitflags BOLD and ITALIC to set the mode
	 * @return the current object
	 */
	public Text setMode(byte mode) {
		this.mode = mode;
		return this;
	}

	/**
	 * Gets the the current align. 0 means left, 1 means right 0.5 means center
	 * @return The current align
	 */
	public float getAlign() {
		return align;
	}

	/**
	 * Sets the current align: 0 means left, 1 means right 0.5 means center.
	 * @param align The align
	 * @return The current object
	 */
	public Text setAlign(float align) {
		this.align = align;
		return this;
	}

	/**
	 * Gets the current vertical align: 0 means top, 1 means bottom 0.5 means middle.
	 * @return The current vertical align
	 */
	public float getVerticalAlign() {
		return verticalAlign;
	}

	/**
	 * Sets the current vertical align: 0 means top, 1 means bottom 0.5 means middle.
	 * @param verticalAlign The vertical align
	 * @return The current object
	 */
	public Text setVerticalAlign(float verticalAlign) {
		this.verticalAlign = verticalAlign;
		return this;
	}

}

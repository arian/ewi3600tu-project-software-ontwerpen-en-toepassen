/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.shapes2d;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

import org.VisibleObject;

public abstract class Shape2d implements VisibleObject {

	protected Color color = new Color(0, 0, 0);
	protected float strokeWidth = 1;

	// Things to be overridden
	protected Point2d[] points;

	@Override
	public abstract void display(GL gl, int dt);

	/**
	 * @param drawable The drawable object to draw the shape against.
	 */
	public void display(GLAutoDrawable drawable, int dt) {
		display(drawable.getGL(), dt);
	}

	/**
	 * @param index
	 * @return The point of the shape at given index, otherwise returns null
	 */
	public Point2d getPoint(int index) {
		if (index < 0 || index >= points.length) {
			return null;
		}

		return points[index];
	}

	/**
	 * @param index The index of the point to set at
	 * @param point The value of the point
	 * @return The current object
	 */
	public Shape2d setPoint(int index, Point2d point) {
		if (index < 0 || index >= points.length) {
			return null;
		}

		points[index] = point;
		return this;
	}

	/**
	 * @param index The index of the point to set at
	 * @param x The x coordinate of the point to set at
	 * @param y The y coordinate of the point to set at
	 * @return Returns newly set point, if index does not exists null
	 */
	public Point2d setPoint(int index, float x, float y) {
		if (index < 0 || index >= points.length) {
			return null;
		}

		points[index] = new Point2d(x, y);
		return points[index];
	}

	/**
	 * Determines whether the given point is in the shape.
	 * @param p A 2D Point instance with the x and y values
	 * @return true if the point is in the shape otherwise not
	 */
	public boolean pointInShape(Point2d p) {
		return pointInShape(p.getX(), p.getY());
	}


	/**
	 * Determines whether the given point is in the shape.
	 * This function is an altered version of http://learn2program.wordpress.com/2007/06/16/the-point-in-the-polygon-or-not/
	 * @param x the x of the point to check
	 * @param y the y of the point to check
	 * @return true if the point is in the shape otherwise not
	 */
	public boolean pointInShape(float x, float y) {
		int i, j;
		boolean res = false;

		for (i = 0, j = points.length-1; i < points.length; j = i++) {

			if (
					(
						(
							points[i].getY() <= y && y <points[j].getY()
						) || (
							points[j].getY() <= y && y < points[i].getY()
						)
					) && (x < (points[j].getX() - points[i].getX()) * (y - points[i].getY()) / (points[j].getY() - points[i].getY()) + points[i].getX())
				)


				res = !res;
		}
		return res;
	}

	/**
	 * @param x The X value to translate all values of this shape by
	 * @param y The Y value to translate all values of this shape by
	 * @return The current shape (Shape2d)
	 */
	public Shape2d translate(float x, float y) {
		for (int i = 0; i < points.length; i++) {
			points[i].translate(x, y);
		}
		return this;
	}

	/**
	 * @param theta The amount in radians to rotate all points of the shape with as origin (0, 0)
	 * @return The current shape (Shape2d)
	 */
	public Shape2d rotate(double theta) {
		for (int i = 0; i < points.length; i++) {
			points[i].rotate(theta);
		}
		return this;
	}

	/**
	 * @param theta The amount in radians to rotate all points of the shape
	 * @param center The origin of the rotation
	 * @return The current shape
	 */
	public Shape2d rotate(double theta, Point2d center) {
		return this
				.translate(-center.getX(), -center.getY())
				.rotate(theta)
				.translate(center.getX(), center.getY());
	}

	/**
	 * Scales all points in the shape by the given factors
	 * @param xFactor the factor to scale the x coordinates by
	 * @param yFactor the factor to scale the y coordinates by
	 * @return The current shape
	 */
	public Shape2d scale(float xFactor, float yFactor) {
		for (int i = 0; i < points.length; i++) {
			points[i].scale(xFactor, yFactor);
		}
		return this;
	}

	/**
	 * Scales all points in the shape by the given factors
	 * @param factor the factor to scale the x and y coordinates by
	 * @return The current shape
	 */
	public Shape2d scale(float factor) {
		return scale(factor, factor);
	}

	/**
	 * This will let the shape fit the given rectangle.
	 * IE it will scale the x and y so that the boundaries of the shape will match the rectangle
	 * @param rect The rectangle to fit to
	 * @return The current shape
	 */
	public Shape2d fit(Rectangle2d rect) {
		Rectangle2d bounds = this.getBoundaries();
		// Short way out if nothing is to be done
		if (bounds.equals(rect)) {
			return this;
		}
		if (bounds.getWidth() == 0 || bounds.getHeight() == 0) {
			return this;
		}

		Point2d center = bounds.getCenter();
		return this
			.translate(-center.getX(), -center.getY())
			.scale(rect.getWidth()/bounds.getWidth(), rect.getHeight()/bounds.getHeight())
			.translate(rect.getCenter().getX(), rect.getCenter().getY());
	}

	/**
	 * This will let the shape fit the given rectangle.
	 * IE it will scale the x and y so that the boundaries of the shape will match the rectangle
	 * @param x The x position of the rectangle
	 * @param y The y position of the rectangle
	 * @param width The width of the rectangle
	 * @param height The height of the rectangle
	 * @return The current shape
	 */
	public Shape2d fit(float x, float y, float width, float height) {
		return this.fit(new Rectangle2d(x, y, width, height));
	}


	/**
	 * Gets the boundaries in which the current shape fits
	 * @return The Rectangle that matches the boundaries
	 */
	public Rectangle2d getBoundaries() {
		float xMin = points[0].getX();
		float yMin = points[0].getY();
		float xMax = points[0].getX();
		float yMax = points[0].getY();

		for (int i = 1; i < points.length; i++) {
			if (points[i].getX() < xMin) {
				xMin = points[i].getX();
			} else if (points[i].getX() > xMax) {
				xMax = points[i].getX();
			}

			if (points[i].getY() < yMin) {
				yMin = points[i].getY();
			} else if (points[i].getY() > yMax) {
				yMax = points[i].getY();
			}
		}

		return new Rectangle2d(
				xMin,
				yMin,
				xMax - xMin,
				yMax - yMin);
	}

	/**
	 * Will return the center of the boundaries of the shape
	 * @return The center of the shape
	 */
	public Point2d getCenter() {
		return getBoundaries().getCenter();
	}

	/**
	 * Will translate all coordinates of the point so that the center of the boundary will match
	 * the given center
	 * @param center The new center of the point
	 * @return The current shape
	 */
	public Shape2d setCenter(Point2d center) {
		return setCenter(center.getX(), center.getY());
	}

	/**
	 * Will translate all coordinates of the point so that the center of the boundary will match
	 * the given center
	 * @param x The x coordinate of the new center
	 * @param y The y coordinate of the new center
	 * @return The current shape
	 */
	public Shape2d setCenter(float x, float y) {
		Point2d currentCenter = this.getCenter();
		this.translate(x - currentCenter.getX(),  y - currentCenter.getY());
		return this;
	}

	/**
	 * @return The color of the shape
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Sets the color of the shape
	 * @param color Sets the color of this shape to the given color
	 * @return the current object
	 */
	public Shape2d setColor(Color color) {
		this.color = color;
		return this;
	}

	/**
	 * Sets the color of the shape
	 * @param r The red component with 0 the least intensity and 1 the highest intensity
	 * @param g The green component with 0 the least intensity and 1 the highest intensity
	 * @param b The blue component with 0 the least intensity and 1 the highest intensity
	 * @return the current object
	 */
	public Shape2d setColor(float r, float g, float b) {
		this.color = new Color(r, g, b);
		return this;
	}

	/**
	 * Sets the color of the shape
	 * @param r The red component with 0 the least intensity and 1 the highest intensity
	 * @param g The green component with 0 the least intensity and 1 the highest intensity
	 * @param b The blue component with 0 the least intensity and 1 the highest intensity
	 * @param a The alpha component with 0 as fully transparent and 1 fully visible
	 * @return the current object
	 */
	public Shape2d setColor(float r, float g, float b, float a) {
		this.color = new Color(r, g, b, a);
		return this;
	}

	/**
	 * Sets the color of the shape
	 * <p>This allows setting the color with an array of color components. The indices map
	 * to red, green, blue and alpha respectively with the alpha component being optional.
	 * An value of 0 at a given index will have the least color intensity and a value of 1
	 * will have the highest intensity. For the alpha channel 0 means fully transparent and
	 * 1 means fully visible.</p>
	 * @param colorComponents as an float array
	 * @return the current object
	 */
	public Shape2d setColor(float[] colorComponents) {
		this.color = new Color(colorComponents);
		return this;
	}

	/**
	 * @return The stroke width of the lines of this shape
	 */
	public float getStrokeWidth() {
		return strokeWidth;
	}

	/**
	 * Sets the stroke width
	 * @param strokeWidth The width to stroke the lines of this shape to
	 * @return the current object
	 */
	public Shape2d setStrokeWidth(float strokeWidth) {
		this.strokeWidth = strokeWidth;
		return this;
	}

}

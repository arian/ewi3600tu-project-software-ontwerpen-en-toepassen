/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game;

import java.util.ArrayList;

import org.TunnelRunner;
import org.gm.BackgroundSound;
import org.gm.game.player.Player;
import org.sound.SoundPlayer;
import org.util.Events.Event;
import org.util.Events.Listener;

public class SoundManager implements Listener {

	private final ArrayList<SoundPlayer> sounds = new ArrayList<SoundPlayer>();

	private SoundPlayer backgroundMusic;

	public SoundManager(Game game, Player player) {
		player.addEventListener(this);
		game.addEventListener(this);
	}

	@Override
	public void update(Event evt) {
		if (TunnelRunner.soundEnabled) {
			String type = (String) evt.data.get("type");
			if ("MaxVelocity".equals(type)) {
				this.maxSpeed();
			} else if ("FireTorpedo".equals(type)) {
				this.fireTorpedo();
			} else if ("GameStart".equals(type)) {
				this.startGame();
			} else if ("GameStop".equals(type)) {
				this.stopGame();
			}
		}
	}

	private void startGame() {
		BackgroundSound.getInstance().play("assets/sounds/Flow_Square.mp3");
	}

	private void stopGame() {
		backgroundMusic.stopPlayer();
		sounds.remove(backgroundMusic);
	}

	private void maxSpeed() {
	}

	private void fireTorpedo() {
		new SoundPlayer("assets/sounds/rifle_shot.mp3").startPlayer();
	}

	public void stopAll() {
		for (SoundPlayer sound : sounds) {
			sound.stopPlayer();
		}
	}

	public void startAll() {
		for (SoundPlayer sound : sounds) {
			sound.startPlayer();
		}
	}

}

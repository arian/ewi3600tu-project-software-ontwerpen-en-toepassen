/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game;

import java.lang.reflect.InvocationTargetException;

import javax.media.opengl.GL;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import org.VisibleObject;
import org.VisibleObjectList;
import org.gm.BlockTypes;
import org.gm.BlockTypes.BlockType;
import org.gm.MazeStorageObjectOptions;
import org.gm.editor.PickupTypes;
import org.gm.game.pickups.Finish;
import org.gm.game.pickups.FlipControl;
import org.gm.game.pickups.LoseControl;
import org.gm.game.pickups.Pickup;
import org.gm.game.pickups.SpeedDown;
import org.gm.game.pickups.SpeedUp;
import org.gm.game.pickups.Start;
import org.gm.game.pickups.SwarmEnter;
import org.gm.game.pickups.SwarmLeave;
import org.gm.game.pickups.TorpedoPickUp;
import org.gm.game.tracks.Track;
import org.shapes.RendererHook;
import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.cubewall.CubeWall;
import org.shapes.swarm.Swarm;
import org.shapes.swarm.SwarmParticleOptions;

import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.linearmath.Transform;

public class GameObject implements VisibleObject {

	private Shape shape;
	private VisibleObjectList<Pickup> pickups = new VisibleObjectList<Pickup>();
	private VisibleObjectList<Shape> shapes = new VisibleObjectList<Shape>();

	private Track track;
	private MazeStorageObjectOptions opts;
	private DynamicsWorld dynamicsWorld;
	private int x;
	private int y;
	private int z;
	private Quat4f rotation;
	private BlockType blockType;

	/**
	 * Constructs a new GameObject
	 * @param shapeType The string name of the type
	 * @param dynamicsWorld The dynamics world to attach to
	 * @param x The x position to create the object at
	 * @param y The y position to create the object at
	 * @param z The z position to create the object at
	 * @param rotation
	 */
	public GameObject(MazeStorageObjectOptions opts, Track track,
			DynamicsWorld dynamicsWorld, int x, int y, int z) {

		this.opts = opts;
		this.track = track;
		this.dynamicsWorld = dynamicsWorld;
		this.x = x;
		this.y = y;
		this.z = z;

		blockType = BlockTypes.getInstance().getByCode(opts.type);

		rotation = new Quat4f();
		rotation.set(new AxisAngle4f(
				opts.rotationVector[0],  opts.rotationVector[1],
				opts.rotationVector[2],
				(opts.rotation + 90) * (float) (Math.PI / 180)
		));

	}

	public void createBlock() throws GameObjectException {

		Class<?> shapeType = blockType.className;

		Quat4f pre_rotated = new Quat4f();
		pre_rotated.set(BlockTypes.getInstance().getByCode(opts.type).preRotation);
		pre_rotated.mul(rotation, pre_rotated);
		AxisAngle4f axisAngle = new AxisAngle4f();
		axisAngle.set(pre_rotated);


		String className = shapeType.getName();
		String shapeTypeRendererHook = className + "RendererHook";
		String shapeTypeRigidBodyHook = className + "RigidBodyHook";

		try {
			Class<?>[] shapeRigidBodyParameters = {Shape.class};
			Class<?>[] shapeRendererParameters = {shapeType};

			Shape shp = (Shape) shapeType.newInstance();
			shp.setPosition(pos2vector(x, y, z));
			shp.setRotation(axisAngle);

			Object[] shpArray = {shp};

			RendererHook shpRenderer = (RendererHook)
					Class.forName(shapeTypeRendererHook).getConstructor(shapeRendererParameters)
					.newInstance(shpArray);
			shp.addHook(shpRenderer);

			RigidBodyHook shpBody = (RigidBodyHook)
					Class.forName(shapeTypeRigidBodyHook).getConstructor(shapeRigidBodyParameters)
					.newInstance(shpArray);
			shpBody.setMass(0);
			shpBody.setDynamicsWorld(dynamicsWorld);
			shp.addHook(shpBody);

			this.shape = shp;

		} catch (InstantiationException e) {
			throw new GameObjectException(e);
		} catch (IllegalAccessException e) {
			throw new GameObjectException(e);
		} catch (ClassNotFoundException e) {
			throw new GameObjectException(e);
		} catch (IllegalArgumentException e) {
			throw new GameObjectException(e);
		} catch (SecurityException e) {
			throw new GameObjectException(e);
		} catch (InvocationTargetException e) {
			throw new GameObjectException(e);
		} catch (NoSuchMethodException e) {
			throw new GameObjectException(e);
		}
	}

	/**
	 * Sets the pickup of this object
	 * @param type the type of the pickup to set
	 * @param x The x position of the pickup
	 * @param y the y position of the pickup
	 * @param z the z position of the pickup
	 */
	public void createPickups() {

		int type = opts.pickup;

		pickups.clear();
		shapes.clear();

		// make sure the pickups are across the track
		Transform trans = new Transform();
		trans.setIdentity();
		trans.setRotation(rotation);
		Vector3f up = new Vector3f(0, 0, 1);
		trans.transform(up);
		AxisAngle4f acrossAA = new AxisAngle4f(up.x, up.y, up.z, (float) Math.PI / 2);
		Quat4f acrossQ = new Quat4f();
		acrossQ.set(acrossAA);
		rotation.mul(acrossQ);

		if ((type & PickupTypes.PICKUP_START) != 0) {
			Start pck = new Start(dynamicsWorld);
			pck.setPosition(pos2vector(x, y, z));
			pck.setRotation(rotation);
			pck.setSize(new Vector3f(2, 16, 16));
			pickups.add(pck);
		}

		if ((type & PickupTypes.PICKUP_FINISH) !=  0) {
			Finish pck = new Finish(dynamicsWorld);
			pck.setPosition(pos2vector(x, y, z));
			pck.setRotation(rotation);
			pck.setSize(new Vector3f(2, 16, 16));
			pickups.add(pck);
		}

		if ((type & PickupTypes.PICKUP_TORPEDO) != 0) {
			TorpedoPickUp pck = new TorpedoPickUp(dynamicsWorld);
			pck.setPosition(pos2vector(x, y, z));
			pck.setRotation(rotation);
			pck.setSize(5);
			pickups.add(pck);
		}
		if ((type & PickupTypes.PICKUP_WALL) != 0) {
			CubeWall wall = new CubeWall();

			wall.setSize(new Vector4f(
					20, Game.tunnelSize / 3, Game.tunnelSize / 3,
					Game.tunnelSize / 12));
			wall.setColor(new float[]{0f, 0f, 0f, 1f});
			wall.setPosition(pos2vector(x, y, z));
			wall.setRotation(rotation);

			wall.setMass(1f);
			wall.build(dynamicsWorld);
			shapes.add(wall);
		}

		if ((type & PickupTypes.PICKUP_SWARMENTER) != 0) {
			SwarmEnter enter = new SwarmEnter(dynamicsWorld, null, track);
			enter.setPosition(pos2vector(x, y, z));
			enter.setSize(new Vector3f(2, 16, 16));
			pickups.add(enter);
		}

		if ((type & PickupTypes.PICKUP_SWARMLEAVE) != 0) {
			SwarmLeave leave = new SwarmLeave(dynamicsWorld, null, track);
			leave.setPosition(pos2vector(x, y, z));
			leave.setSize(new Vector3f(2, 16, 16));
			pickups.add(leave);
		}

		if ((type & PickupTypes.PICKUP_SWARM) != 0) {
			Swarm swarm = new Swarm(dynamicsWorld, track.getPlayer());
			track.getSwarms().add(swarm);
			swarm.setPosition(pos2vector(x, y, z));
			swarm.build(10,
					new SwarmParticleOptions()
						.setMass(1)
						.setGlobalBestScale(0.2f));
			shapes.add(swarm);
		}

		if ((type & PickupTypes.PICKUP_SPEEDUP) != 0) {
			SpeedUp pck = new SpeedUp(dynamicsWorld);
			pck.setPosition(pos2vector(x, y, z));
			pck.setRotation(rotation);
			pck.setSize(5);
			pickups.add(pck);
		}

		if ((type & PickupTypes.PICKUP_SPEEDDOWN) != 0) {
			SpeedDown pck = new SpeedDown(dynamicsWorld);
			pck.setPosition(pos2vector(x, y, z));
			pck.setRotation(rotation);
			pck.setSize(5);
			pickups.add(pck);
		}

		if ((type & PickupTypes.PICKUP_LOSECONTROL) != 0) {
			LoseControl pck = new LoseControl(dynamicsWorld);
			pck.setPosition(pos2vector(x, y, z));
			pck.setRotation(rotation);
			pck.setSize(10);
			pickups.add(pck);
		}

		if ((type & PickupTypes.PICKUP_FLIPCONTROL) != 0) {
			FlipControl pck = new FlipControl(dynamicsWorld);
			pck.setPosition(pos2vector(x, y, z));
			pck.setRotation(rotation);
			pck.setSize(10);
			pickups.add(pck);
		}

	}

	/**
	 * Gets the pickup of the current GameObject
	 * @return The pickup
	 */
	public VisibleObjectList<Pickup> getPickups() {
		return pickups;
	}

	@Override
	public void display(GL gl, int dt) {
		this.shape.display(gl, dt);
		this.shapes.display(gl, dt);
		this.pickups.display(gl, dt);
	}

	/**
	 * Transforms an xyz (maze position) to a real world position
	 * @param x the x maze position
	 * @param y the y maze position
	 * @param z the z maze position
	 * @return A vector3f representing the real world position
	 */
	public static Vector3f pos2vector(int x, int y, int z) {
		return new Vector3f((x) * Game.tunnelSize,
				(y) * Game.tunnelSize,
				(z) * Game.tunnelSize);
	}

	public static Vector3f pos2vector(Vector3f posVector) {
		return pos2vector((int) posVector.x,
				(int) posVector.y,
				(int) posVector.z);
	}

}

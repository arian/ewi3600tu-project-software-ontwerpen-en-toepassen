/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game;

public class GameObjectException extends Exception {

	private static final long serialVersionUID = -1692898193286744061L;

	public GameObjectException(String message) {
		super(message);
	}

	public GameObjectException(Exception e) {
		super(e);
	}

}

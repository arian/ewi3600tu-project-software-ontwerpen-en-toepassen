/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.glu.GLU;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Vector3f;

import org.ResponseListener;
import org.TextureManager;
import org.VisibleObject;
import org.VisibleObjectList;
import org.gm.Factory;
import org.gm.FactoryException;
import org.gm.FactoryOptions;
import org.gm.GameMode;
import org.gm.MazeDataException;
import org.gm.editor.LoadFrame;
import org.gm.game.joyride.JoyrideCamera;
import org.gm.game.player.Player;
import org.gm.game.player.PlayerCamera;
import org.gm.game.tracks.DemoTrack;
import org.gm.game.tracks.EditorTrack;
import org.gm.game.tracks.Track;

import com.bulletphysics.collision.broadphase.SimpleBroadphase;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.dispatch.GhostPairCallback;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;

public class Game extends GameMode {

	public static final int tunnelSize = 100;

	private GLCanvas canvas;

	// The player object.
	private Player player;

	private Track track;

	// A list of objects that will be displayed on screen.
	private VisibleObjectList<VisibleObject> visible3DObjects = new VisibleObjectList<VisibleObject>();
	private VisibleObjectList<VisibleObject> visible2DObjects = new VisibleObjectList<VisibleObject>();

	// This object can be passed to other objects to collect other visibleObjects.
	// This useful so we can nix these at once.
	private VisibleObjectList<VisibleObject> passed3DObjects = new VisibleObjectList<VisibleObject>();

	// The camera object.
	private Camera camera;

	// Check for success in the response listener
	public boolean hadResponse = false;

	// Possible camera modes
	public enum CameraMode {
		PLAYER,
		JOYRIDE
	}

	// Current camera mode
	private CameraMode cameraMode;

	// Our sound manager
	SoundManager soundmanager;

	// 2D dashboard
	Dashboard dashboard;

	// The user input object that controls the game.
	private GameUserInput input;

	// The JBullet DynamicsWorld
	private DiscreteDynamicsWorld dynamicsWorld;

	// The id of the maze to load
	private int levelID = 0;

	/**
	 * initMode is called each time we open the this 'Game' GameMode.
	 */
	@Override
	public void initMode(GLAutoDrawable drawable, FactoryOptions options) {
		super.initMode(drawable, options);

		canvas = options.getCanvas();

		hadResponse = false;
		// ask which level we will play.
		new LoadFrame(new ResponseListener<Integer>() {
			@Override
			public void respond(Integer res) {
				levelID = res;
				hadResponse = true;
			}
		}, true);

		// Create the player
		player = new Player(canvas, passed3DObjects);

		// Determine the start position
		Vector3f startPosition = new Vector3f(10, 0, -Game.tunnelSize / 6 + 1);
		AxisAngle4f startRotation = new AxisAngle4f(1, 0, 0, 0);
		if (levelID >= 1) {
			try {
				startPosition = GameObject.pos2vector(GameMazeStorage.getStartPosition(levelID));
				startRotation = GameMazeStorage.getStartRotation(levelID);
				startPosition.z -= Game.tunnelSize / 6 - 1;
			} catch (MazeDataException e) {
				e.printStackTrace();
			}
		}
		player.setPosition(startPosition);
		player.setRotation(startRotation);

		// reset trackTime
		options.put("trackTime", 0);
		options.put("level", levelID);

		player.addEventListener(new Listener() {

			@Override
			public void update(Event evt) {
				String type = (String) evt.data.get("type");
				if ("finished".equals(type)) {
					try {
						// We finished!
						Factory gameModeFactory = Factory.getInstance();
						gameModeFactory.getOptions().put("trackTime", evt.data.get("trackTime"));
						gameModeFactory.set("highscores");
					} catch (FactoryException e) {
						e.printStackTrace();
					}
				}
			}

		});

		// initialize the physics world
		initPhysics();

		// add physics to the player
		player.addRendererHook();
		player.addRigidBodyHook(dynamicsWorld);
		visible3DObjects.add(player);

		if (levelID < 1) {
			track = new DemoTrack(dynamicsWorld, player);
		} else {
			track = new EditorTrack(levelID, dynamicsWorld, player);
		}
		visible3DObjects.add(track);
		track.constructPickups();

		// game input
		input = new GameUserInput(canvas);

		// Dashboard
		dashboard = new Dashboard(player, canvas);
		visible2DObjects.add(dashboard);

		// Music
		soundmanager = new SoundManager(this, player);

		// fire event that the game starts
		fireEvent(new Event() {
			@Override
			public void onAdd() {
				this.data.put("type", "GameStart");
			}
		});

		// set texture mode
		TextureManager.getInstance(drawable.getGL()).setEnvMode(GL.GL_MODULATE);

		visible3DObjects.add(passed3DObjects);

	}

	@Override
	public void deInitMode(GLAutoDrawable drawable, FactoryOptions options) {
		super.deInitMode(drawable, options);

		levelID = 0;

		// Stop, remove and nullify these objects
		soundmanager.stopAll();
		soundmanager = null;

		// detach player
		player.detach();
		visible3DObjects.remove(player);
		player = null;

		if (camera instanceof JoyrideCamera) {
			JoyrideCamera jrCamera = (JoyrideCamera) camera;
			jrCamera.detach();
		}
		camera = null;

		input.detach();
		input = null;

		dynamicsWorld = null;

		dashboard.detach();
		visible2DObjects.remove(dashboard);
		dashboard = null;

		visible3DObjects.remove(track);
		track = null;

		visible3DObjects.remove(passed3DObjects);
		passed3DObjects.clear();

	}

	private void initPhysics(){

		SimpleBroadphase broadphase = new SimpleBroadphase();

		// collision configuration contains default setup for memory, collision
		// setup.
		CollisionConfiguration collisionConfiguration = new DefaultCollisionConfiguration();

		// use the default collision dispatcher. For parallel processing you
		// can use a different dispatcher (see Extras/BulletMultiThreaded)
		CollisionDispatcher dispatcher = new CollisionDispatcher(
				collisionConfiguration);

		// the default constraint solver. For parallel processing you can use a
		// different solver (see Extras/BulletMultiThreaded)
		SequentialImpulseConstraintSolver solver = new SequentialImpulseConstraintSolver();

		// DiscreteDynamicsWorld
		dynamicsWorld = new DiscreteDynamicsWorld(
				dispatcher, broadphase, solver,
				collisionConfiguration);


		dynamicsWorld.setGravity(new Vector3f(0, 0, -10));

		// Set GhostPairCallback for Ghost objects callbacks
		GhostPairCallback ghostPairCallback = new GhostPairCallback();
		dynamicsWorld.getBroadphase().getOverlappingPairCache().setInternalGhostPairCallback(ghostPairCallback);

	}

	/**
	 * This method is called on every drawing frame
	 */
	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);

		// return directly when this GameMode is not active, could happen when
		// this mode is deactivated, but the display method is still fired
		// because it's in another thread.
		if (!active) {
			return;
		}

		if (!hadResponse) {
			try {
				active = false;
				Factory.getInstance().set("menu");
				return;
			} catch (FactoryException e) {
				e.printStackTrace();
			}
		}

		// Do the physics things that need to happen on on every frame
		displayPhysics(deltaTime);

		GL gl = drawable.getGL();
		GLU glu = new GLU();

		// Background color
		gl.glClearColor(0, 0, 0, 0);

		// Alpha transparency
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL.GL_BLEND);

		int width = canvas.getWidth();
		int height = canvas.getHeight();
		if (height == 0) {
			// fix division by zero error...
			height = 1;
		}
		// 3D renderings
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(60, width / height, 1, 20000);
		gl.glMatrixMode(GL.GL_MODELVIEW);

		// Enable back-face culling.
		gl.glCullFace(GL.GL_BACK);
		gl.glEnable(GL.GL_CULL_FACE);

		// Enable Z-buffering.
		gl.glEnable(GL.GL_DEPTH_TEST);

		// Set and enable the lighting.

		// White light!
		float lightColour[] = {0.5f, 0.5f, 0.5f, 1f};

		// Properties for Light0.
		gl.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, lightColour, 0);
		gl.glEnable(GL.GL_LIGHTING);
		gl.glEnable(GL.GL_LIGHT0);

		// Set the shading model.
		gl.glShadeModel(GL.GL_SMOOTH);

		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
		gl.glLoadIdentity();

		// Display the 3D objects, set camera and so on
		gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_MODULATE);
		display3D(gl, glu);

		// disable 3D things
		gl.glDisable(GL.GL_CULL_FACE);
		gl.glDisable(GL.GL_DEPTH_TEST);
		gl.glDisable(GL.GL_LIGHT0);
		gl.glDisable(GL.GL_LIGHTING);

		// 2D renderings on the screen
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(0, width, 0, height, -1, 1);
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();

		gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_REPLACE);
		display2D(gl, glu);

		gl.glLoadIdentity();
		// Flush the OpenGL buffer.
		gl.glFlush();

	}

	/**
	 * Every display stuff that has to do with 3D rendering
	 * @param gl
	 * @param glu
	 */
	private void display3D(GL gl, GLU glu) {

		// should we change the input control / camera mode
		if (camera == null || input.getCameraMode() != cameraMode) {
			cameraMode = input.getCameraMode();
			player.detach();
			if (camera instanceof JoyrideCamera) {
				JoyrideCamera jrCamera = (JoyrideCamera) camera;
				jrCamera.detach();
			}
			if (cameraMode == CameraMode.JOYRIDE) {
				camera = new JoyrideCamera(canvas);
				camera.setVRP(player.getPosition());
				Vector3f cop = new Vector3f(player.getPosition());
				cop.add(new Vector3f(4, 4, 4));
				camera.setCOP(cop);
			} else {
				player.attach();
				camera = new PlayerCamera(player);
			}
		}

		// Camera
		Vector3f camCOP = camera.getCOP();
		Vector3f camVRP = camera.getVRP();
		Vector3f camVUV = camera.getVUV();

		glu.gluLookAt(
			camCOP.x, camCOP.y, camCOP.z,
			camVRP.x, camVRP.y, camVRP.z,
			camVUV.x, camVUV.y, camVUV.z
		);

		// Set light 0 position, which is always on the camera
		float lightPosition[] = {camCOP.x, camCOP.y, camCOP.z, 1.0f};
		gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, lightPosition, 0);

		// Display all objects
		visible3DObjects.display(gl, deltaTime);
		// Update the camera positions to follow this object

		if (camera != null) {
			// camera can be null, because in the display loop, the GameMode
			// was changed and camera was set to null.
			camera.update(deltaTime);
		}

	}

	/**
	 * 2D rendering, for current speed, lap times and so on
	 * @param gl
	 * @param glu
	 */
	private void display2D(GL gl, GLU glu) {

		// Display all the visible objects of MazeRunner.
		visible2DObjects.display(gl, deltaTime);

	}

	private void displayPhysics(double dt){

		if (!active) {
			return;
		}

		if (camera != null && camera instanceof PlayerCamera) {
			// TODO can be optimized, it's not necessary to set these each frame
			// also to 'steer' left or right, it probably should take the
			// perpendicular vector of the speed and gravity (cross product)
			if (input.getGravity() == GameUserInput.Gravity.DOWN) {
				dynamicsWorld.setGravity(new Vector3f(0, 0, -9.81f));
//				camera.setVUV(new Vector3f(0, 0, 1));
			}
			else if (input.getGravity() == GameUserInput.Gravity.UP) {
				dynamicsWorld.setGravity(new Vector3f(0, 0, 9.81f));
//				camera.setVUV(new Vector3f(0, 0, -1));
			}
			else if (input.getGravity() == GameUserInput.Gravity.LEFT) {
				dynamicsWorld.setGravity(new Vector3f(0, 9.81f, 0));
			}
			else if (input.getGravity() == GameUserInput.Gravity.RIGHT) {
				dynamicsWorld.setGravity(new Vector3f(0, -9.81f, 0));
			}
		}

		// see http://bulletphysics.org/mediawiki-1.5.8/index.php/Stepping_the_World#How_do_I_use_this.3F
		// in our case we want a step 1/60th of a second. The minimum FPS we
		// could to accept is 15. So we need a max substep of 4
		float timeStep = ((float) dt) / 1000;
		dynamicsWorld.stepSimulation(timeStep, 3, 1f / 45f);

	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {

		if (active) {
			GL gl = drawable.getGL();
			GLU glu = new GLU();

			gl.glMatrixMode(GL.GL_PROJECTION);
			gl.glLoadIdentity();
			glu.gluPerspective(60, width / height, 1, 20000);
			gl.glMatrixMode(GL.GL_MODELVIEW);
			gl.glLoadIdentity();
		}

	}

}

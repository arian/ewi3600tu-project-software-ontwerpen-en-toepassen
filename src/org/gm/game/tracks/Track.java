/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.tracks;

import org.VisibleObject;
import org.gm.game.player.Player;
import org.shapes.swarm.SwarmManager;

public interface Track extends VisibleObject {

	public void constructPickups();

	public Player getPlayer();

	public SwarmManager getSwarms();

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.tracks;

import javax.vecmath.AxisAngle4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import org.gm.game.pickups.Checkpoint;
import org.gm.game.pickups.Finish;
import org.gm.game.pickups.Start;
import org.gm.game.pickups.SwarmEnter;
import org.gm.game.pickups.SwarmLeave;
import org.gm.game.pickups.TorpedoPickUp;
import org.gm.game.player.Player;
import org.shapes.RendererHook;
import org.shapes.cubewall.CubeWall;
import org.shapes.swarm.Swarm;
import org.shapes.swarm.SwarmParticleOptions;

import com.bulletphysics.dynamics.DynamicsWorld;

public class DemoTrack extends EditorTrack implements Track {

	private static final long serialVersionUID = 3661047404456150520L;

	public final static String level = "uvw(T=2,RV=0;0;1,P=0,R=90.0)z z z z z z z z z zyw(T=1,RV=0;0;1,P=0,R=90.0)z z z z z z z z z zyw(T=1,RV=0;0;1,P=0,R=90.0)z z z z z z z z z zyw(T=3,RV=0;0;1,P=0,R=90.0)z z z z z z z z z zyw(T=2,RV=0;0;1,P=0,R=0.0)z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyxvw(T=2,RV=0;0;1,P=0,R=180.0)z z z z z z z z z zyw(T=2,RV=0;0;1,P=0,R=0.0)z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw(T=4,RV=0;0;1,P=0,R=180.0)z(T=5,RV=0;0;1,P=0,R=0.0)z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyxvw z z z z z z z z z zyw(T=2,RV=0;0;1,P=0,R=180.0)z z z z z z z z z zyw(T=4,RV=0;0;1,P=0,R=270.0)z(T=5,RV=0;0;1,P=0,R=90.0)z z z z z z z z zyw z(T=3,RV=0;0;1,P=0,R=90.0)z z z z z z z z zyw z(T=2,RV=0;0;1,P=0,R=270.0)z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyxvw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyxvw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyxvw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyxvw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyxvw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyxvw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyxvw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyw z z z z z z z z z zyx";

	public DemoTrack(DynamicsWorld dynamicsWorld, Player player) {
		super(level, dynamicsWorld, player);
	}

	@Override
	public void constructPickups() {

		// Torpedo pickup
		TorpedoPickUp torpedo = new TorpedoPickUp(dynamicsWorld);
		torpedo.setPosition(new Vector3f(30, 0, -14));
		RendererHook torpRH = torpedo.getRendererHook();
		torpRH.setColor(new float[] {1, 0, 0, 0.5f});
		torpRH.setOscillation(new Vector4f(0, 0, 1f, 4f));
		torpedo.setRotation(new AxisAngle4f(1, 1, 1, (float) Math.PI / 2));
		torpedo.setSize(3);
		this.add(torpedo);
		player.addPickup(torpedo);

		// Start-finish
		Checkpoint start = new Start(dynamicsWorld);
		start.setPosition(new Vector3f(50, 0, 0));
		start.setSize(new Vector3f(1, 100f / 6, 100 / 6));
		this.add(start);
		player.addPickup(start);

		Checkpoint finish = new Finish(dynamicsWorld);
		finish.setPosition(new Vector3f(0, 50, 0));
		finish.setSize(new Vector3f(1, 100f / 6, 100 / 6));
		finish.setRotation(new AxisAngle4f(0, 0, 1, (float) Math.PI / 2));
		this.add(finish);
		player.addPickup(finish);

		// Wall
		CubeWall wall = new CubeWall();
		wall.setSize(new Vector4f(20, 32, 32, 10f));
		wall.setCubeSizeRandomBounds(0.9f, 1);
		wall.setPosition(new Vector3f(0, 100, 0));
		wall.setRotation(new AxisAngle4f(0, 0, 1, (float) Math.PI / 2));
		wall.setMass(1f);
		wall.build(dynamicsWorld);
		this.add(wall);

		// Swarm
		Swarm swarm = new Swarm(dynamicsWorld, player);
		swarm.setPosition(new Vector3f(0, 50, 0));
		swarm.build(10,
				new SwarmParticleOptions()
					.setMass(1)
					.setGlobalBestScale(0.2f));
		this.add(swarm);

		SwarmEnter enter = new SwarmEnter(dynamicsWorld, swarm, this);
		enter.setPosition(new Vector3f(70, 0, 0));
		enter.setSize(new Vector3f(2, 16, 16));
		this.add(enter);
		player.addPickup(enter);

		SwarmLeave leave = new SwarmLeave(dynamicsWorld, swarm, this);
		leave.setPosition(new Vector3f(0, 150, 0));
		leave.setSize(new Vector3f(2, 16, 16));
		this.add(leave);
		player.addPickup(leave);

	}

}

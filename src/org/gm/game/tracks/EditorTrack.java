/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.tracks;

import org.VisibleObject;
import org.VisibleObjectList;
import org.gm.MazeBuilder;
import org.gm.MazeDataException;
import org.gm.MazeStorage;
import org.gm.MazeStorageObjectOptions;
import org.gm.game.GameMazeStorage;
import org.gm.game.GameObject;
import org.gm.game.GameObjectException;
import org.gm.game.pickups.Pickup;
import org.gm.game.player.Player;
import org.shapes.swarm.SwarmManager;

import com.bulletphysics.dynamics.DynamicsWorld;

public class EditorTrack extends VisibleObjectList<VisibleObject> implements Track  {

	private static final long serialVersionUID = -4419147373780760770L;

	protected SwarmManager swarms;
	protected DynamicsWorld dynamicsWorld;
	protected Player player;

	public EditorTrack(int levelID, DynamicsWorld dynamicsWorld, Player player) {
		super();
		try {
			String dataString = GameMazeStorage.getMazeDataString(levelID);
			deserializeMaze(dataString, dynamicsWorld, player);
		} catch (MazeDataException e) {
			e.printStackTrace();
		}
	}

	public EditorTrack(String data, DynamicsWorld dynamicsWorld, Player player) {
		super();
		deserializeMaze(data, dynamicsWorld, player);
	}

	private void deserializeMaze(String dataString,
			final DynamicsWorld dynamicsWorld, Player player) {

		this.dynamicsWorld = dynamicsWorld;
		this.player = player;

		swarms = new SwarmManager();
		this.add(swarms);

		try {

			MazeStorage.deserializeMaze(dataString, new MazeBuilder<EditorTrack>() {

				@Override
				public void createWithSize(int x, int y, int z)
						throws MazeDataException {
				}

				@Override
				public void setObjectAtLocation(int x, int y, int z,
						MazeStorageObjectOptions opts) {

					try {

						GameObject obj = new GameObject(
								opts, EditorTrack.this,
								dynamicsWorld, x, y, z);

						obj.createBlock();
						obj.createPickups();

						EditorTrack.this.add(obj);
					} catch (GameObjectException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void setName(String name) {
				}

				@Override
				public EditorTrack getResult() {
					return EditorTrack.this;
				}

				@Override
				public void close() {}

			}, "");

		} catch (MazeDataException e1) {
			e1.printStackTrace();
		}

	}

	@Override
	public void constructPickups() {
		for (VisibleObject obj : this) {
			if (obj instanceof GameObject) {
				GameObject gameObject = (GameObject) obj;
				VisibleObjectList<Pickup> picksups = gameObject.getPickups();
				for (Pickup pickup : picksups) {
					player.addPickup(pickup);
				}
			}
		}
	}

	@Override
	public SwarmManager getSwarms() {
		return swarms;
	}

	@Override
	public Player getPlayer() {
		return player;
	}

}

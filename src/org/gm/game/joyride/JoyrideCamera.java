/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.joyride;

import javax.media.opengl.GLCanvas;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

import org.gm.game.Camera;
import org.util.Attachable;

import com.bulletphysics.linearmath.Transform;


/**
 * The JoyrideCamera is a way to move freely, without constraints, through the
 * game space, which is very useful for development.
 */
public class JoyrideCamera extends Camera implements Attachable {

	JoyrideUserInput input;

	public JoyrideCamera(GLCanvas canvas) {
		input = new JoyrideUserInput(canvas);
		this.cop = new Vector3f(0, 0, 5);
		this.vrp = new Vector3f(100, 100, 0);
	}

	@Override
	public void attach() {
		input.attach();
	}

	@Override
	public void detach() {
		input.detach();
	}

	@Override
	public void update(int dt) {
		Vector2f direction = input.getDirection();
		Vector2f angle = input.getAngle();

		double speed = 0.05;

		// difference between COP and VRP
		Vector3f diff = new Vector3f();
		diff.sub(vrp, cop);
		// normalized difference vector
		Vector3f diffNormalized = new Vector3f(diff);
		diffNormalized.normalize();

		// Forward & backwards
		if (direction.x != 0) {
			Vector3f forward = new Vector3f(diffNormalized);
			forward.scale((float) (direction.x * speed * dt));
			cop.add(forward);
			vrp.add(forward);
		}

		// Left and right sideways
		if (direction.y != 0) {
			Vector3f sideways = new Vector3f();
			sideways.cross(new Vector3f(0, 0, 1), diffNormalized);
			sideways.normalize();
			sideways.scale((float) (direction.y * speed * dt));
			cop.add(sideways);
			vrp.add(sideways);
		}

		if (angle.x != 0 || angle.y != 0) {

			if (angle.y != 0) {
				Transform trans = new Transform();
				Quat4f rot = new Quat4f();
				// rotate about the normal vector of z and diff
				Vector3f norm = new Vector3f();
				norm.cross(new Vector3f(0, 0, 1), diffNormalized);
				rot.set(new AxisAngle4f(norm.x, norm.y, norm.z, - 0.003f * angle.y * dt));
				trans.setRotation(rot);
				// apply transformation
				trans.transform(diff);
			}

			// Rotate left and right
			if (angle.x != 0) {
				Transform trans = new Transform();
				Quat4f rot = new Quat4f();
				// rotate about the z axis
				rot.set(new AxisAngle4f(0, 0, 1, - 0.003f * angle.x * dt));
				trans.setRotation(rot);
				// apply transformation
				trans.transform(diff);
			}

			vrp.add(cop, diff);

		}

	}

}

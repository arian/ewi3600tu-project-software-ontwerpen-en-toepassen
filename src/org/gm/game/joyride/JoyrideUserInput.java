/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.joyride;

import java.awt.event.KeyEvent;

import javax.media.opengl.GLCanvas;
import javax.vecmath.Vector2f;

import org.gm.game.UserInput;

public class JoyrideUserInput extends UserInput {

	private final Vector2f direction = new Vector2f();
	private final Vector2f angle = new Vector2f();

	public JoyrideUserInput(GLCanvas canvas) {
		super(canvas);
	}

	public Vector2f getDirection() {
		return direction;
	}

	public Vector2f getAngle() {
		return angle;
	}

	// The actual event handlers

	@Override
	public void keyPressed(KeyEvent event) {
		switch (event.getKeyCode()) {
			case KeyEvent.VK_LEFT:  angle.x = -1; break;
			case KeyEvent.VK_RIGHT: angle.x =  1; break;
			case KeyEvent.VK_UP:    angle.y =  1; break;
			case KeyEvent.VK_DOWN:  angle.y = -1; break;
			case KeyEvent.VK_W:     direction.x =  1; break;
			case KeyEvent.VK_S:     direction.x = -1; break;
			case KeyEvent.VK_A:     direction.y =  1; break;
			case KeyEvent.VK_D:     direction.y = -1; break;
		}
	}

	@Override
	public void keyReleased(KeyEvent event) {
		switch (event.getKeyCode()) {
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_RIGHT: angle.x = 0; break;
			case KeyEvent.VK_UP:
			case KeyEvent.VK_DOWN:  angle.y = 0; break;
			case KeyEvent.VK_W:
			case KeyEvent.VK_S:     direction.x = 0; break;
			case KeyEvent.VK_A:
			case KeyEvent.VK_D:     direction.y = 0; break;
		}

	}

}

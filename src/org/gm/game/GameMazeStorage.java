/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.vecmath.AxisAngle4f;
import javax.vecmath.Vector3f;

import org.db.Connector;
import org.gm.MazeDataException;
import org.gm.MazeStorage;

public class GameMazeStorage extends MazeStorage {

	public static String getMazeDataString(int id) throws MazeDataException {

		try {

			Object[] querydata = {id};
			ResultSet res;
			Connector db = Connector.getInstance();
			res = db.query("SELECT leveldata FROM levels WHERE id = ? ", querydata);
			if (!res.next()) {
				throw new MazeDataException("Maze not found (no rows)");
			}
			String mazeString = res.getString(1);
			res.close(); // Free up resources

			return mazeString;

		} catch (SQLException e) {
			throw new MazeDataException("Maze not found (query failed: " + e.getMessage() + ")");
		} catch (ClassNotFoundException e) {
			throw new MazeDataException("Maze not found (failed to connecto to db)");
		}

	}

	public static Vector3f getStartPosition(int id) throws MazeDataException {

		try {

			Connector db = Connector.getInstance();
			ResultSet res = db.query("SELECT start_x, start_y, start_z FROM levels WHERE id = ? ", new Object[]{id});
			if (!res.next()) {
				res.close();
				throw new MazeDataException("Maze not found (no rows)");
			}
			Vector3f ret = new Vector3f(res.getInt(1), res.getInt(2), res.getInt(3));
			res.close();
			return ret;

		} catch (SQLException e) {
			throw new MazeDataException("Maze not found (query failed: " + e.getMessage() + ")");
		} catch (ClassNotFoundException e) {
			throw new MazeDataException("Maze not found (failed to connecto to db)");
		}
	}

	public static AxisAngle4f getStartRotation(int id) throws MazeDataException {

		try {

			Connector db = Connector.getInstance();
			ResultSet res = db.query("SELECT start_rot_w, start_rot_x, start_rot_y, start_rot_z FROM levels WHERE id = ? ", new Object[]{id});
			if (!res.next()) {
				res.close();
				throw new MazeDataException("Maze not found (no rows)");
			}
			AxisAngle4f rot = new AxisAngle4f(res.getInt(2), res.getInt(3), res.getInt(4), (res.getFloat(1)) * (float) (Math.PI / 180));
			res.close();
			return rot;

		} catch (SQLException e) {
			throw new MazeDataException("Maze not found (query failed: " + e.getMessage() + ")");
		} catch (ClassNotFoundException e) {
			throw new MazeDataException("Maze not found (failed to connecto to db)");
		}
	}


}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.pickups;

import javax.media.opengl.GL;
import javax.vecmath.Vector4f;

import org.gm.game.player.Player;

import com.bulletphysics.dynamics.DynamicsWorld;

public class Finish extends Checkpoint {

	public Finish(DynamicsWorld dynamicsWorld) {
		super(dynamicsWorld);
		rh.setColor(new float[]{1, 1, 0, 1f});
		rh.setOscillation(new Vector4f(0, 0, 2, 1));
	}

	@Override
	public void fired(Player player) {
		player.setFinished();
	}

	@Override
	public void display(GL gl, int dt) {
		super.display(gl, dt);
	}

}

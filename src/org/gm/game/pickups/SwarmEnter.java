/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.pickups;

import javax.vecmath.Vector4f;

import org.TunnelRunner;
import org.gm.game.player.Player;
import org.gm.game.tracks.Track;
import org.shapes.GhostObjectHook;
import org.shapes.cuboid.CheckpointRendererHook;
import org.shapes.cuboid.Cuboid;
import org.shapes.cuboid.CuboidGhostObjectHook;
import org.shapes.swarm.Swarm;
import org.sound.SoundPlayer;
import org.util.Time;

import com.bulletphysics.dynamics.DynamicsWorld;

public class SwarmEnter extends Cuboid implements Pickup {

	protected CuboidGhostObjectHook goh;
	protected CheckpointRendererHook rh;
	protected Swarm swarm;
	protected Track track;
	private long prevTime;

	public SwarmEnter(DynamicsWorld dynamicsWorld, Swarm swarm, Track track) {
		super();
		this.swarm = swarm;
		this.track = track;

		goh = new CuboidGhostObjectHook(this);
		goh.setDynamicsWorld(dynamicsWorld);

		rh = new CheckpointRendererHook(this);
		rh.setOscillation(new Vector4f(0, 0, 2, 1));
		rh.setColor(new float[]{1, 1, 1, 0.5f});

		this.addHook(goh);
		this.addHook(rh);
	}

	@Override
	public void fired(Player player) {
		if (swarm == null) {
			swarm = track.getSwarms().getClosestSwarm(position);
		}

		if (swarm != null) {

			swarm.activate();
			swarm.setMaxSpeed(player.getMaxSpeed() * 1.5f);

			player.fireEvent(new Event() {
				@Override
				public void onAdd() {
					this.data.put("type", "enterSwarm");
				}
			});

			if (TunnelRunner.soundEnabled) {
				long time = Time.getMillis();
				if ((time - prevTime) > 1500) {
					new SoundPlayer("assets/sounds/swarmalert.mp3").startPlayer();
					prevTime = time;
				}
			}
		}
	}

	@Override
	public GhostObjectHook getGhostObjectHook() {
		return goh;
	}

}

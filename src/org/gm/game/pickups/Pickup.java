/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.pickups;

import org.VisibleObject;
import org.gm.game.player.Player;
import org.shapes.GhostObjectHook;

public interface Pickup extends VisibleObject {

	/**
	 * Should return the GhostObjectHook
	 * @return the object's GhostObjectHook
	 */
	public GhostObjectHook getGhostObjectHook();

	/**
	 * The fired methods are fired on each frame if the player is inside the
	 * pickup object.
	 * @param player
	 */
	public void fired(Player player);

}

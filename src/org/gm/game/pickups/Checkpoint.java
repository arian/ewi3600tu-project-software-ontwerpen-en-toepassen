/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.pickups;

import org.shapes.GhostObjectHook;
import org.shapes.cuboid.CheckpointRendererHook;
import org.shapes.cuboid.Cuboid;
import org.shapes.cuboid.CuboidGhostObjectHook;

import com.bulletphysics.dynamics.DynamicsWorld;

public abstract class Checkpoint extends Cuboid implements Pickup {

	protected CuboidGhostObjectHook goh;
	protected CheckpointRendererHook rh;

	public Checkpoint(DynamicsWorld dynamicsWorld) {
		goh = new CuboidGhostObjectHook(this);
		goh.setDynamicsWorld(dynamicsWorld);

		rh = new CheckpointRendererHook(this);

		this.addHook(goh);
		this.addHook(rh);
	}

	@Override
	public GhostObjectHook getGhostObjectHook() {
		return goh;
	}

}

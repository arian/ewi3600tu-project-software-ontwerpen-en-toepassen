/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.pickups;

import javax.vecmath.Vector4f;

import org.gm.game.player.Player;

import com.bulletphysics.dynamics.DynamicsWorld;

public class Start extends Checkpoint {

	private long prevTime;
	private boolean soundOn;

	public Start(DynamicsWorld dynamicsWorld) {
		super(dynamicsWorld);
		rh.setOscillation(new Vector4f(0, 0, 2, 1));
		rh.setColor(new float[] {0, 1, 0, 1f});
	}

	@Override
	public void fired(Player player) {
		player.startCountDown();
/*
		if (TunnelRunner.soundEnabled) {
			long time = Time.getMillis();
			if ((time - prevTime) > 1500 && !soundOn) {

				new SoundPlayer("assets/sounds/321go.mp3").startPlayer();
				soundOn = true;

				prevTime = time;
			}
		}
*/
	}

}

package org.gm.game.pickups;

import javax.vecmath.Vector4f;

import org.TunnelRunner;
import org.gm.game.player.Player;
import org.shapes.GhostObjectHook;
import org.shapes.cube.Cube;
import org.shapes.cube.CubeGhostObjectHook;
import org.shapes.cube.PickUpRendererHook;
import org.sound.SoundPlayer;
import org.util.Time;

import com.bulletphysics.dynamics.DynamicsWorld;

public class FlipControl extends Cube implements Pickup {
	private CubeGhostObjectHook goh;
	private PickUpRendererHook rh;

	private long prevTime;
	// Player can pickup a new torpedo each second
	private long pickupInterval = 2000;

	public FlipControl(DynamicsWorld dynamicsWorld) {

		goh = new CubeGhostObjectHook(this);
		goh.setDynamicsWorld(dynamicsWorld);

		rh = new PickUpRendererHook(this);
		rh.setOscillation(new Vector4f(0, 3, 3, 1));
		rh.setColor(new float[]{1, 0.5f, 0, 0.5f});

		this.addHook(goh);
		this.addHook(rh);

	}

	@Override
	public GhostObjectHook getGhostObjectHook() {
		return goh;
	}

	public PickUpRendererHook getRendererHook() {
		return rh;
	}

	@Override
	public void fired(Player player) {
		long time = Time.getMillis();
		if ((time - prevTime) > pickupInterval) {
			// flip the roll control for a given amount of milliseconds
			player.flipRollControl(9000);
			prevTime = time;

			if (TunnelRunner.soundEnabled) {
				new SoundPlayer("assets/sounds/confusion.mp3").startPlayer();
			}

		}

	}
}

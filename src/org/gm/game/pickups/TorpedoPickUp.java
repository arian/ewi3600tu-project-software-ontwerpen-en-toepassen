/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.pickups;

import javax.vecmath.Vector4f;

import org.TunnelRunner;
import org.gm.game.player.Player;
import org.shapes.GhostObjectHook;
import org.shapes.cube.Cube;
import org.shapes.cube.CubeGhostObjectHook;
import org.shapes.cube.PickUpRendererHook;
import org.sound.SoundPlayer;
import org.util.Time;

import com.bulletphysics.dynamics.DynamicsWorld;

public class TorpedoPickUp extends Cube implements Pickup {

	private CubeGhostObjectHook goh;
	private PickUpRendererHook rh;

	private long prevTime;
	// Player can pickup a new torpedo each second
	private long pickupInterval = 1000;

	public TorpedoPickUp(DynamicsWorld dynamicsWorld) {

		goh = new CubeGhostObjectHook(this);
		goh.setDynamicsWorld(dynamicsWorld);

		rh = new PickUpRendererHook(this);
		rh.setOscillation(new Vector4f(0, 7, 1, 1));
		rh.setColor(new float[]{0, 0, 1f, 0.5f});

		this.addHook(goh);
		this.addHook(rh);

	}

	@Override
	public GhostObjectHook getGhostObjectHook() {
		return goh;
	}

	public PickUpRendererHook getRendererHook() {
		return rh;
	}

	@Override
	public void fired(Player player) {
		long time = Time.getMillis();
		if ((time - prevTime) > pickupInterval) {
			// push 3 torpedos
			player.pushTorpedo();
			player.pushTorpedo();
			player.pushTorpedo();
			prevTime = time;

			player.fireEvent(new Event() {
				@Override
				public void onAdd() {
					this.data.put("type", "torpedos");
				}
			});

			if (TunnelRunner.soundEnabled) {
				new SoundPlayer("assets/sounds/powerup_spawn.mp3").startPlayer();
			}

		}

	}

}

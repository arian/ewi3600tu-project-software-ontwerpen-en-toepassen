/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game;

import java.util.Timer;
import java.util.TimerTask;

import javax.media.opengl.GL;
import javax.media.opengl.GLCanvas;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import org.TextureManager;
import org.VisibleObject;
import org.gm.Button;
import org.gm.ButtonList;
import org.gm.Factory;
import org.gm.FactoryException;
import org.gm.game.player.Player;
import org.shapes2d.Color;
import org.shapes2d.Point2d;
import org.shapes2d.Rectangle2d;
import org.shapes2d.Tetragon;
import org.shapes2d.Text;
import org.util.Attachable;
import org.util.Events.Event;
import org.util.Events.Listener;
import org.util.Time;

import com.bulletphysics.linearmath.Transform;

public class Dashboard implements VisibleObject, Attachable {

	private Timer timer                   = new Timer();
	private TimerTask timerTask           = new Task();
	private final Text timeText           = new Text(new Point2d(230, 30), "");
	private final Text FPSText            = new Text(new Point2d(100, 190), "");

	private Tetragon velometer			= Tetragon.rectangle(10, 10, 200, 200);
	// Need to rotate it around 64, 50
	private Tetragon veloNeedle			= Tetragon.rectangle(46, 100, 72, 59);
	private Tetragon rotoMeter			= Tetragon.rectangle(10, 10, 10, 10);

	private Text countDownText;
	private final Player player;
	private String formattedTime;
	private String formattedFPS;
	private String formattedCountDown;
	private ButtonList buttons;
	private float fps = 0;
	private GLCanvas canvas;

	// count down
	private long fBCountDownStart;
	private long fBCountDownTogo = -1;
	private long fBCountDown = 2000;

	// feedback
	private String feedbackString;
	private Text feedbackText;

	public Dashboard(Player player, GLCanvas canvas) {
		this.player = player;
		this.canvas = canvas;

		Color white = new Color(1, 1, 1);
		timeText.setAlign(0).setColor(white);
		timeText.setAlign(0).setSize(60).setColor(white);

		velometer.setTexture("speedometer.png");
		veloNeedle.setTexture("needle.png");
		rotoMeter.setTexture("rotometer.png");

		countDownText = new Text(new Point2d(canvas.getWidth() / 2, canvas.getHeight() / 2), "");
		countDownText.setAlign(0.5f);
		countDownText.setVerticalAlign(0.5f);
		countDownText.setColor(white);
		countDownText.setSize(40);
		countDownText.setText(formattedCountDown);

		feedbackText = new Text(new Point2d(canvas.getWidth() / 2, canvas.getHeight() / 1.5f), "");
		feedbackText.setColor(white);
		feedbackText.setSize(40);

		buttons = new ButtonList(canvas);

		// Back to menu button
		Button nBut = new Button(0, 0, 100, 100, "Quit");
		nBut.getLabel().setColor(1, 1, 1);
		nBut.setEventListener(new Button.EventShim(){

			@Override
			public void mouseEnter(Button button) {
				((Tetragon) button.getShape()).setTexture("button-active.png");
			}

			@Override
			public void mouseLeave(Button button) {
				//TextureManager.getInstance(canvas.getGL()).
				((Tetragon) button.getShape()).setTexture("button-inactive.png");
			}

			@Override
			public void mouseClick(Button button) {
				try {
					Factory.getInstance().set("Menu");
				} catch (FactoryException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void realign(Button button, GLCanvas canvas) {
				button.getShape().fit(canvas.getWidth() - 190, 10, 180, 40);
			}
		});

		nBut.triggerInit(canvas);
		buttons.addButton(nBut);

		// A timer is only called once in 200 ms, while the display method is
		// called a lot more, so things that are not so important can be done
		// here in this separate timer thread
		scheduleTimer();

		player.addEventListener(new Listener() {

			@Override
			public void update(Event evt) {

				if ( "lowerMaxSpeed".equals(evt.data.get("type"))) {
					feedbackString = "Slow down dude!";
					startFBCountDown();

				} else if ( "higherMaxSpeed".equals(evt.data.get("type"))) {
					feedbackString = "Go! Go! Go!";
					startFBCountDown();

				} else if ( "flipControl".equals(evt.data.get("type"))) {
					feedbackString = "Woops! Control mix-up!";
					startFBCountDown();

				} else if ( "loseControl".equals(evt.data.get("type"))) {
					feedbackString = "Time out buddy!";
					startFBCountDown();

				} else if ( "started".equals(evt.data.get("type"))) {
					feedbackString = "Good luck!";
					startFBCountDown();

				} else if ( "enterSwarm".equals(evt.data.get("type"))) {
					feedbackString = "They're onto ya!";
					startFBCountDown();

				} else if ( "leaveSwarm".equals(evt.data.get("type"))) {
					feedbackString = "You got rid of them!";
					startFBCountDown();

				} else if ( "torpedos".equals(evt.data.get("type"))) {
					feedbackString = "Shoot some!";
					startFBCountDown();
				}

				if (feedbackString != null) {
					feedbackText.setText(feedbackString);
				}

			}
		});


	}

	@Override
	public void attach() {
		buttons.attach();
		scheduleTimer();
	}

	@Override
	public void detach() {
		buttons.detach();
		timer.cancel();
	}

	private void scheduleTimer() {
		timer.scheduleAtFixedRate(timerTask, 0, 50);
	}

	/**
	 * Class that is called upon each timer run.
	 */
	private class Task extends TimerTask {

		@Override
		public void run() {
			// velocity
			float velocity = player.getVelocity();

			/* Let's recalculate the the rotation of the velocity needle */
			veloNeedle.setRectangle(new Rectangle2d(41, 100, 72, 20));
			veloNeedle.rotate(Math.PI * (velocity - 8) / 30, new Point2d(110, 110));

			// time
			long elapsed = player.getElapsedTime();
			if (elapsed != 0) {
				formattedTime = Time.formatLong(elapsed);
				timeText.setText(formattedTime);
			} else {
				formattedTime = null;
			}

			float s = Math.min(canvas.getWidth(), canvas.getHeight()) - 200;

			Vector3f vuv = new Vector3f(0, 0, 1);
			Transform trans = new Transform();
			trans.setRotation(player.getOrientation());
			trans.transform(vuv);

			// Dankjewel
			// http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/
			Quat4f or = player.getOrientation();

			double heading;
			double attitude;
			double bank;
			double test = or.x*or.y + or.z*or.w;
			if (test > 0.499) { // singularity at north pole
				heading = 2 * Math.atan2(or.x,or.w);
				attitude = Math.PI/2;
				bank = 0;
				return;
			}
			else if (test < -0.499) { // singularity at south pole
				heading = -2 * Math.atan2(or.x,or.w);
				attitude = - Math.PI/2;
				bank = 0;
				return;
			}
			else {
				double sqx = or.x*or.x;
				double sqy = or.y*or.y;
				double sqz = or.z*or.z;
				heading = Math.atan2(2*or.y*or.w-2*or.x*or.z , 1 - 2*sqy - 2*sqz);
				attitude = Math.asin(2*test);
				bank = Math.atan2(2*or.x*or.w-2*or.y*or.z , 1 - 2*sqx - 2*sqz);
			}


			// Draw the rotometer

			rotoMeter.setRectangle(new Rectangle2d((canvas.getWidth() - s)/2, (canvas.getHeight() - s)/2, s, s));
			rotoMeter.rotate((float) -bank, new Point2d(canvas.getWidth() / 2f, canvas.getHeight() / 2f));



			// FPS
			formattedFPS = ((int) (fps + 0.5f)) + "";
			FPSText.setText(formattedFPS);

			// Countdown
			if (player.isCountingDown()) {
				formattedCountDown = "" + ((int) Math.ceil(player.getCountDown() / 1000));
				countDownText.setText(formattedCountDown);
			} else {
				formattedCountDown = null;
			}

			// Feedback
			if (feedbackString != null) {
				updateFBCountDown();
			}

		}
	}

	public void startFBCountDown() {
		if (fBCountDownTogo < 0) {
			fBCountDownStart = Time.getMillis();
			fBCountDownTogo = fBCountDown;
			updateFBCountDown();
		}
	}

	public void updateFBCountDown() {
		if (fBCountDownStart == 0) {
			return;
		}
		fBCountDownTogo = fBCountDown - (Time.getMillis() - fBCountDownStart);
		if (fBCountDownTogo <= 0) {
			fBCountDownTogo = -1;
			if (feedbackString != null) {
				feedbackString = null;
			}
		}
	}

	@Override
	public void display(GL gl, int dt) {

		fps = 1000f / dt;

		rotoMeter.display(gl, dt);

		if (formattedTime != null) {
			timeText.display(gl, dt);
		}

		if (formattedFPS != null) {
			FPSText.display(gl, dt);
		}

		if (formattedCountDown != null) {
			countDownText.display(gl, dt);
		}

		if (feedbackString != null) {
			feedbackText.display(gl,  dt);
		}

		velometer.display(gl, dt);
		veloNeedle.display(gl, dt);



		int torpedos = player.countTorpedos();
		gl.glBindTexture (GL.GL_TEXTURE_2D, TextureManager.getInstance(gl).getTexture("rocket.png"));

		gl.glBegin(GL.GL_QUADS);

		for (int i = 0; i < torpedos; i++) {
			gl.glVertex2f(120 + (i * 20), 240);
			gl.glTexCoord2d (1, 1);

			gl.glVertex2f(120 + (i * 20), 200);
			gl.glTexCoord2d (1, 0);

			gl.glVertex2f(80 + (i * 20), 200);
			gl.glTexCoord2d (0, 0);

			gl.glVertex2f(80 + (i * 20), 240);
			gl.glTexCoord2d (0, 1);
		}

		gl.glEnd();
		gl.glDisable(GL.GL_TEXTURE_2D);

		buttons.display(gl, dt);
	}

}

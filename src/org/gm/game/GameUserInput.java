/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game;

import java.awt.event.KeyEvent;

import javax.media.opengl.GLCanvas;

public class GameUserInput extends UserInput {

	public enum Gravity {
		DOWN, UP, LEFT, RIGHT
	}

	protected Gravity gravity = Gravity.DOWN;

	// TODO the initial camera mode should be defined in Game
	protected Game.CameraMode cameraMode = Game.CameraMode.PLAYER;

	public GameUserInput(GLCanvas canvas) {
		super(canvas);
	}

	public Game.CameraMode getCameraMode() {
		return cameraMode;
	}

	/**
	 * @return Returns Vector3f with the direction of the gravity
	 */
	public Gravity getGravity() {
		return gravity;
	}

	// The actual event handlers

	@Override
	public void keyPressed(KeyEvent event) {

		switch (event.getKeyCode()) {
			case KeyEvent.VK_SPACE:
				if (cameraMode == Game.CameraMode.PLAYER)
					cameraMode = Game.CameraMode.JOYRIDE;
				else
					cameraMode = Game.CameraMode.PLAYER;
			break;
			case KeyEvent.VK_I: this.gravity = Gravity.UP;   break;
			case KeyEvent.VK_J:	this.gravity = Gravity.LEFT; break;
			case KeyEvent.VK_K:	this.gravity = Gravity.DOWN; break;
			case KeyEvent.VK_L:	this.gravity = Gravity.RIGHT; break;
		}
	}

}

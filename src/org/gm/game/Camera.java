/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game;

import javax.vecmath.Vector3f;


/**
 * Camera represents the camera player in MazeRunner.
 */
abstract public class Camera {

	/**
	 * the eye point or Center of Projection (COP). In a camera model this is
	 * the position of the camera, which is used to make the image (take the
	 * photograph) of the scene.
	 * @see TI2710-B Computer Graphics lecture notes section 3.2
	 */
	protected Vector3f cop = new Vector3f();

	/**
	 * the "look at" point or View Reference Point (VRP). VRP is the point the
	 * camera is directed at. COP and VRP together determine the viewing
	 * direction.
	 */
	protected Vector3f vrp = new Vector3f();

	/**
	 * the View Up Vector (VUV). Given COP and VRP, the camera can still be
	 * rotated about the axis through COP and VRP. VUV fixes the up direction
	 * of the camera.
	 */
	protected Vector3f vuv = new Vector3f(0, 0, 1);

	/**
	 * Doing the calculations to follow the player
	 * it is possible to just walk through the spaces
	 */
	abstract public void update(int dt);

	/**
	 * Gets the Center Of Position, or the position of the camera or eye.
	 * @return the COP vector
	 */
	public Vector3f getCOP() {
		return cop;
	}

	/**
 	 * Gets the Center Of Position, or the position of the camera or eye.
	 * @param cop
	 */
	public void setCOP(Vector3f cop) {
		this.cop = cop;
	}

	/**
	 * Gets the Viewing Reference Point, the point the eye will look at
	 * @return the VRP vector
	 */
	public Vector3f getVRP() {
		return vrp;
	}

	/**
	 * Sets the Viewing Reference Point, the point the eye will look at
	 * @param vrp
	 */
	public void setVRP(Vector3f vrp) {
		this.vrp = vrp;
	}

	/**
	 * Gets the viewing up vector
	 * @return the VUV vector
	 */
	public Vector3f getVUV() {
		return vuv;
	}

	/**
	 * Sets a new Viewing Up Vector
	 * @param vuv
	 */
	public void setVUV(Vector3f vuv) {
		this.vuv = vuv;
	}


}

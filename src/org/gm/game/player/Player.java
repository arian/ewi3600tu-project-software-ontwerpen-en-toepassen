/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.player;

import java.util.LinkedList;
import java.util.Queue;

import javax.media.opengl.GL;
import javax.media.opengl.GLCanvas;
import javax.swing.event.EventListenerList;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import org.VisibleObject;
import org.VisibleObjectList;
import org.gm.game.pickups.Pickup;
import org.shapes.GhostObjectHook;
import org.shapes.GhostObjectHook.CollisionCallback;
import org.shapes.RendererHook;
import org.shapes.RigidBodyHook;
import org.shapes.Shape;
import org.shapes.ShapeException;
import org.shapes.ship.Ship;
import org.shapes.ship.ShipRendererHook;
import org.shapes.ship.ShipRigidBodyHook;
import org.shapes.shipleftwing.ShipLeftWing;
import org.shapes.shipleftwing.ShipLeftWingRendererHook;
import org.shapes.shipleftwing.ShipLeftWingRigidBodyHook;
import org.shapes.shiprightwing.ShipRightWing;
import org.shapes.shiprightwing.ShipRightWingRendererHook;
import org.shapes.shiprightwing.ShipRightWingRigidBodyHook;
import org.shapes.torpedo.Torpedo;
import org.util.Attachable;
import org.util.Time;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.GhostObject;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.constraintsolver.HingeConstraint;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;

/**
 * The model of the player
 */
public class Player extends Shape implements Attachable {

	public EventListenerList listeners = new EventListenerList();
	private PlayerUserInput input;

	// Limit speeds
	private float maxSpeed = 25;
	private final float absoluteMaxSpeed = 47;
	private final float absoluteMinSpeed = 10;
	private final float maxAngularVelocity = 0.7f;

	// parts of the player
	private final ShipLeftWing wings1;
	private final ShipRightWing wings2;
	private final Ship fuselage;
	private RigidBodyHook wrbh1;
	private RendererHook wrh1;
	private RigidBodyHook wrbh2;
	private RendererHook wrh2;
	private RigidBodyHook frbh;
	private RendererHook frh;

	private DynamicsWorld dynamicsWorld;

	// count down
	private long countDownStart;
	private long countDownTogo = -1;
	private long countDown = 5000;

	// flip control
	private boolean rollControlFlipped = false;
	private long flipTimeStart;
	private long flipTimeTogo = -1;
	private long flipTime;

	// Start and finished states
	private boolean started;
	private boolean finished;

	// lap times
	private long startTime;
	private long trackTime = Integer.MAX_VALUE;

	// player's torpedos
	private Queue<Torpedo> torpedos = new LinkedList<Torpedo>();
	private int maxTorpedos = 12;

	// global game visible objects, so the player can add or remove objects
	private final VisibleObjectList<VisibleObject> visibleObjects;

	public Player(GLCanvas canvas, VisibleObjectList<VisibleObject> visibleObjects) {
		input = new PlayerUserInput(canvas, this);
		this.visibleObjects = visibleObjects;

		wings1 = new ShipLeftWing();
		wings2 = new ShipRightWing();
		// fuselage = new Cuboid(1.f, 0.5f, 0.6f);
		fuselage = new Ship();
	}

	/**
	 * Attach the UserInput events to the player
	 */
	@Override
	public void attach() {
		input.attach();
	}

	/**
	 * Attach the UserInput events from the player
	 */
	@Override
	public void detach() {
		input.detach();
	}

	@Override
	public void setPosition(Vector3f pos) {
		if (wrbh1 != null) {
			System.out.println("You shouldn't set the position after the physics constaints are applied");
		}
		super.setPosition(pos);
		fuselage.setPosition(pos);
		wings1.setPosition(pos);
		wings2.setPosition(pos);
	}

	/**
	 * Create the Renderer Hook
	 */
	public void addRendererHook() {
		wrh1 = new ShipLeftWingRendererHook(wings1);
		float[] color = {1f, 1f, 1f, 1f};
		wrh1.setColor(color);
		wings1.addHook(wrh1);

		wrh2 = new ShipRightWingRendererHook(wings2);
		float[] color2 = {1f, 1f, 1f, 1f};
		wrh2.setColor(color2);
		wings2.addHook(wrh2);

		frh = new ShipRendererHook(fuselage);
		float[] color3 = {1f, 1f, 1f, 1f};
		frh.setColor(color3);
		fuselage.addHook(frh);
	}

	/**
	 * Create the physics and dynamics
	 * @param dynamicsWorld
	 */
	public void addRigidBodyHook(DynamicsWorld dynamicsWorld) {
		this.dynamicsWorld = dynamicsWorld;

		try {

			wrbh1 = new ShipLeftWingRigidBodyHook(wings1);
			wrbh1.setMass(0.1f);
			wrbh1.setDynamicsWorld(dynamicsWorld);
			wrbh1.createRigidBody();
			wings1.addHook(wrbh1);

			wrbh2 = new ShipRightWingRigidBodyHook(wings2);
			wrbh2.setMass(0.1f);
			wrbh2.setDynamicsWorld(dynamicsWorld);
			wrbh2.createRigidBody();
			wings2.addHook(wrbh2);

			frbh = new ShipRigidBodyHook(fuselage);
			frbh.setMass(1);
			frbh.setDynamicsWorld(dynamicsWorld);
			frbh.createRigidBody();
			fuselage.addHook(frbh);

			float PI = (float) Math.PI;

			// constrain 'em together

			Transform localA = new Transform(),
					localB = new Transform(),
					localC = new Transform(),
					localD = new Transform();

			localA.setIdentity();
			localB.setIdentity();
			localC.setIdentity();
			localC.setIdentity();

			// Right wing
			MatrixUtil.setEulerZYX(localA.basis, 0, PI / 2, 0);
			localA.origin.set(0.0f, 0.5f, 0.1f);
			MatrixUtil.setEulerZYX(localB.basis, 0, PI / 2, 0);
			localB.origin.set(0.0f, -1f, -0.1f);

			HingeConstraint constraint1 = new HingeConstraint(
					frbh.getBody(), wrbh1.getBody(),
					localA, localB);

			// Left wing
			MatrixUtil.setEulerZYX(localC.basis, 0, PI / 2, 0);
			localC.origin.set(0.0f, -0.5f, 0.05f);
			MatrixUtil.setEulerZYX(localD.basis, 0, PI / 2, 0);
			localD.origin.set(0.0f, 1f, -0.05f);

			HingeConstraint constraint2 = new HingeConstraint(
					frbh.getBody(), wrbh2.getBody(),
					localC, localD);

			// angle constraints of the hinge
			constraint1.setLimit(-PI / 8, PI / 8);
			constraint2.setLimit(-PI / 8, PI / 8);

			// the second argument is whether the two bodies can overlap or not.
			// if this argument is not set, the wings will vibrate and look
			// weird.
			dynamicsWorld.addConstraint(constraint1, true);
			dynamicsWorld.addConstraint(constraint2, true);

		} catch (ShapeException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Set the controls, to let the player know how to move
	 * @param input
	 */
	public void setUserInput(PlayerUserInput input) {
		this.input = input;
	}

	/**
	 * Gets the user input controls
	 * @return the PlayerUserInput instance, that listens to the user events
	 */
	public PlayerUserInput getUserInput() {
		return this.input;
	}

	/**
	 * Calculates the velocity of the physics body. It will return 0
	 * if there is no body yet.
	 * @return the current linear velocity of the player.
	 */
	public float getVelocity() {
		RigidBody body = frbh.getBody();
		if (body == null) {
			return 0;
		}
		Vector3f velocity = new Vector3f();
		body.getLinearVelocity(velocity);
		return velocity.length();
	}

	/**
	 * Calculates the orientation of the physics body.
	 * @return the orientation as Quaternion
	 */
	public Quat4f getOrientation() {
		RigidBody body = frbh.getBody();
		Quat4f orientation = new Quat4f(0,0,0,0);
		if (body == null) {
			return orientation;
		}
		body.getOrientation(orientation);
		return orientation;
	}

	/**
	 * Return true if a Collision object is the same object as the fuselage
	 * RigidBody object. Useful for collision detection when iterating over
	 * the ghost objects
	 * @param pickup
	 */
	public void addPickup(final Pickup pickup) {
		final Player player = this;
		pickup.getGhostObjectHook().addCallback(frbh, new CollisionCallback() {

			@Override
			public void callback(GhostObjectHook ghost, Shape shape, GhostObject ghostObject, CollisionObject rb) {
				pickup.fired(player);
			}

		});
	}

	/**
	 * Start counting down before starting the track.
	 */
	public void startCountDown() {
		if (countDownTogo < 0) {
			countDownStart = Time.getMillis();
			countDownTogo = countDown;
			updateCountDown();
		}
	}

	/**
	 * Update count down milliseconds
	 */
	public void updateCountDown() {
		if (countDownStart == 0) {
			return;
		}
		countDownTogo = countDown - (Time.getMillis() - countDownStart);
		if (countDownTogo <= 0) {
			countDownTogo = 0;
			if (!started) {
				setStarted();
			}
		}
	}

	/**
	 * Get the counting down time.
	 * @return the time in milliseconds counting down
	 */
	public long getCountDown() {
		return countDownTogo;
	}

	/**
	 * If it's still counting down
	 * @return true if the there is a countdown
	 */
	public boolean isCountingDown() {
		return countDownTogo > 0;
	}

	/**
	 * The player starts with the track
	 */
	public void setStarted() {
		this.fireEvent(new Event() {
			@Override
			public void onAdd() {
				this.data.put("type", "started");
			}
		});
		started = true;
		if (startTime == 0) {
			startTime = Time.getMillis();
		}
	}

	/**
	 *
	 * @return true if the player has started with the track
	 */
	public boolean isStarted() {
		return started;
	}

	/**
	 *
	 * @return true if the player has finished with the track
	 */
	public boolean isFinished() {
		return finished;
	}

	/**
	 * The player finished with the track
	 */
	public void setFinished() {
		if (!started) {
			return;
		}
		final long time = Time.getMillis();
		trackTime = time - startTime;
		finished = true;
		// fire finished event.
		this.fireEvent(new Event() {
			@Override
			public void onAdd() {
				this.data.put("type", "finished");
				this.data.put("startTime", startTime);
				this.data.put("finishTime", time);
				this.data.put("trackTime", trackTime);
			}
		});
		startTime = 0;
	}

	/**
	 * Gets the time the player is currently playing this track
	 * returns 0 if the player isn't started yet.
	 * @return time in milliseconds
	 */
	public long getElapsedTime() {
		if (startTime != 0) {
			long time = Time.getMillis();
			return time - startTime;
		}
		return 0;
	}

	/**
	 * Gets the time the player required to finish the track.
	 * Returns Intege.MAX_VALUE when the player hasn't finished yet.
	 * @return time in milliseconds
	 */
	public long getTrackTime() {
		return trackTime;
	}

	/**
	 * Pushes a new torpedo to the player's torpedo stack
	 */
	public void pushTorpedo() {
		int size = torpedos.size();
		if (size < maxTorpedos && dynamicsWorld != null) {
			Torpedo torpedo = new Torpedo(this);
			torpedo.addRendererHook();
			torpedos.add(torpedo);
			if (size == 0) {
				// Only one torpedo is added to the visibleObjects at any time.
				visibleObjects.add(torpedo);
			}
		}
	}

	/**
	 *
	 * @return the number of torpedos available
	 */
	public int countTorpedos() {
		return torpedos.size();
	}

	/**
	 * Getter and setter for the players maximum speed
	 */
	public float getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(float newSpeed) {
		maxSpeed = newSpeed;
	}

	/**
	 * Enable external countdown re-activation
	 * - takes input countdown time in milliseconds
	 */
	public void makeLoseControl(long waitTime) {
		countDown = waitTime;
		countDownTogo = -1;
		startCountDown();
		this.fireEvent(new Event() {
			@Override
			public void onAdd() {
				this.data.put("type", "loseControl");
			}
		});
	}

	/**
	 * Flipping the roll control
	 * - rollControlFlipped()
	 * - getFlipTime()
	 * - flipRollControl()
	 * - updateFlipTime()
	 */
	public boolean rollControlFlipped() {
		return rollControlFlipped;
	}

	public long getFlipTime() {
		return flipTime;
	}

	public void flipRollControl(long fTime) {
		flipTime = fTime;
		flipTimeStart = Time.getMillis();
		updateFlipTime();
		rollControlFlipped = true;
		this.fireEvent(new Event() {
			@Override
			public void onAdd() {
				this.data.put("type", "flipControl");
			}
		});
	}

	public void updateFlipTime() {
		if (flipTimeStart == 0) {
			return;
		}
		flipTimeTogo = flipTime - (Time.getMillis() - flipTimeStart);
		if (flipTimeTogo <= 0) {
			flipTimeTogo = 0;
			rollControlFlipped = false;
		}
	}

	/**
	 * Methods to heighten or lower the maximum speed in stead of setting it to an absolute value
	 */
	public void heightenMaxSpeed(float speedIncrement) {
		float newSpeed = maxSpeed + speedIncrement;
		if (newSpeed <= absoluteMaxSpeed) {
			maxSpeed = newSpeed;
			this.fireEvent(new Event() {
				@Override
				public void onAdd() {
					this.data.put("type", "higherMaxSpeed");
				}
			});
		} else {
			maxSpeed = absoluteMaxSpeed;
		}
	}

	public void lowerMaxSpeed(float speedDecrement) {
		float newSpeed = maxSpeed - speedDecrement;
		if (newSpeed >= absoluteMinSpeed) {
			maxSpeed = newSpeed;
			this.fireEvent(new Event() {
				@Override
				public void onAdd() {
					this.data.put("type", "lowerMaxSpeed");
				}
			});
		} else {
			maxSpeed = absoluteMinSpeed;
		}
	}

	/**
	 * Controls the player by applying forces.
	 */
	private void displayControls() {

		byte flipFactor = 1;
		if (rollControlFlipped) {
			flipFactor = -1;
		}

		RigidBody fbody = frbh.getBody();
		RigidBody wbody1 = wrbh1.getBody();
		RigidBody wbody2 = wrbh2.getBody();

		if (input != null && fbody != null && wbody1 != null && wbody2 != null) {
			input.update();

			// In case bullet de-activated the player
			fbody.activate();
			wbody1.activate();
			wbody2.activate();

			// input direction
			Quat4f orientation = new Quat4f();
			fbody.getOrientation(orientation);

			// transformation matrix to transform from local coordinates to globals
			Transform trans = new Transform();
			trans.setRotation(orientation);

			if (input.acceleration != 0
					|| input.yaw[0]
					|| input.yaw[1]
					|| input.pitch[0]
					|| input.pitch[1]
					|| input.roll[0]
					|| input.roll[1]
			) {

				// Newtons
				Vector3f lift = new Vector3f(0, 0, 2);
				Vector3f center = new Vector3f(0, 0, 0);

				// set lift of the wings
				trans.transform(lift);
				wbody1.applyForce(lift, center);
				wbody2.applyForce(lift, center);

				// force on the front of the player
				Vector3f forceFront = new Vector3f(0, 0, 0);
				Vector3f torque = new Vector3f(0, 0, 0);

				if (input.acceleration != 0) {
					forceFront.x += 10 * input.acceleration;
				}

				// Yaw
				if (input.yaw[0] && input.yaw[1]) {
					// apply extra force to accelerate
					forceFront.x += 3;
				} else if (input.yaw[0] && !input.yaw[1]) {
					// turn left
					forceFront.y += 1 * flipFactor;
				} else if (!input.yaw[0] && input.yaw[1]) {
					// turn right
					forceFront.y -= 1 * flipFactor;
				}

				// Pitch
				if (input.pitch[0] && !input.pitch[1]) {
					// turn up
					forceFront.z -= 1;
				} else if (!input.pitch[0] && input.pitch[1]) {
					// turn down
					forceFront.z += 1;
				}

				// Roll
				if (input.roll[0] && !input.roll[1]) {
					// roll counter-clockwise
					torque.x -= 1 * flipFactor;

				} else if (!input.roll[0] && input.roll[1]) {
					// roll clock-wise
					torque.x += 1 * flipFactor;
				}

				// limit angular velocity
				Vector3f angularVelocity = new Vector3f();
				fbody.getAngularVelocity(angularVelocity);

				float angularVelocityLength = angularVelocity.length();
				if (angularVelocityLength > maxAngularVelocity) {
					angularVelocity.scale(maxAngularVelocity / angularVelocityLength);
					fbody.setAngularVelocity(angularVelocity);
				}

				// apply the force at the front of the body.
				float forceSize = 5;
				forceFront.scale(forceSize);
				trans.transform(forceFront);

				float torqueSize = 10;
				torque.scale(torqueSize);
				trans.transform(torque);

				Vector3f front = new Vector3f(3, 0, 0);
				trans.transform(front);

				fbody.applyForce(forceFront, front);
				fbody.applyTorque(torque);

				fbody.setDamping(0.2f, 0.8f);

			}

			// Limit linear velocity
			Vector3f velocity = new Vector3f();
			fbody.getLinearVelocity(velocity);
			final float currentVelocity = velocity.length();
			if (currentVelocity > maxSpeed) {
				velocity.scale(maxSpeed / currentVelocity);
				fbody.setLinearVelocity(velocity);
				this.fireEvent(new Event() {
					@Override
					public void onAdd() {
						data.put("type", "MaxVelocity");
						data.put("velocity", currentVelocity);
					}
				});
			}

		}
	}

	/**
	 * Display player lighting
	 */
	private void displayLightning(GL gl) {
		// position relative to the center of the player
		Vector3f lightPos = new Vector3f(0.5f, 0, 0); // (0.5f, 0, 0)
		Vector3f lightDir = new Vector3f(1, 0, 0);
		// transform position and directions
		Quat4f orientation = getOrientation();
		Transform trans = new Transform();
		trans.setRotation(orientation);
		trans.transform(lightDir);
		trans.transform(lightPos);

		// Set a spotlight at the front of the player
		gl.glLightf(GL.GL_LIGHT1, GL.GL_SPOT_CUTOFF, 45); //15

		// set focusing strength
		gl.glLightf(GL.GL_LIGHT1, GL.GL_SPOT_EXPONENT, 7.0f); //7

		// input direction
		float[] lightDirection = new float[]{lightDir.x, lightDir.y, lightDir.z};
		gl.glLightfv(GL.GL_LIGHT1, GL.GL_SPOT_DIRECTION, lightDirection, 0);

		// set position
		Vector3f spot = new Vector3f();
		spot.set(this.getPosition());
		spot.add(lightPos);
		float lightPosition[] = {spot.x, spot.y, spot.z, 1.0f};
		gl.glLightfv(GL.GL_LIGHT1, GL.GL_POSITION, lightPosition, 0);

		// set color
		float spot_ambient[] =  {0.2f, 0.2f, 0.2f, 1.0f};
		float spot_diffuse[] =  {0.8f, 0.8f, 0.8f, 1.0f};
		float spot_specular[] = {0.8f, 0.8f, 0.8f, 1.0f};
		// set colors here and do the geometry in draw
		gl.glLightfv(GL.GL_LIGHT1, GL.GL_AMBIENT,  spot_ambient,0);
		gl.glLightfv(GL.GL_LIGHT1, GL.GL_DIFFUSE,  spot_diffuse,0);
		gl.glLightfv(GL.GL_LIGHT1, GL.GL_SPECULAR, spot_specular,0);

		// enable light
		gl.glEnable(GL.GL_LIGHT1);

	}

	private void displayTorpedos() {
		// torpedos
		int size = torpedos.size();
		if (input != null && input.wantFire() && size > 0) {
			Torpedo torpedo = torpedos.poll();
			torpedo.fire(dynamicsWorld, visibleObjects);
			if (size > 1) {
				// make the next torpedo visible
				torpedo = torpedos.peek();
				visibleObjects.add(torpedo);
			}
			input.isFired = false;

			this.fireEvent(new Event() {
				@Override
				public void onAdd() {
					data.put("type", "FireTorpedo");
				}
			});
		}
	}

	/**
	 * Display methods, will control the player, if it has to.
	 */
	@Override
	public void display(GL gl, int dt) {
		super.display(gl, dt);

		wings1.display(gl, dt);
		wings2.display(gl, dt);
		fuselage.display(gl, dt);

		this.setPosition(fuselage.getPosition(), NO_PROPAGATE);

		if (isCountingDown()) {
			updateCountDown();
		} else {
			displayControls();
		}

		if (rollControlFlipped()) {
			updateFlipTime();
		}

		displayLightning(gl);
		displayTorpedos();

	}

}

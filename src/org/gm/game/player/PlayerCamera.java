/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.player;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import org.gm.game.Camera;

import com.bulletphysics.linearmath.Transform;


/**
 * Camera represents the camera player in MazeRunner.
 */
public class PlayerCamera extends Camera {

	protected Player player;

	public PlayerCamera(Player player) {
		this.player = player;
	}

	/**
	 * Doing the calculations to follow the player
	 */
	@Override
	public void update(int dt) {

		// orientation of the player
		Quat4f orientation = player.getOrientation();
		Transform trans = new Transform();
		trans.setRotation(orientation);

		// STATIC offset of the camera
		Vector3f staticCOP = new Vector3f();
		{
			Vector3f sub = new Vector3f(7, 0, -2);
			trans.transform(sub);
			staticCOP.sub(player.getPosition(), sub);
		}

		// DYNAMIC offset of the camera.
		// It camera will follow the above calculated staticCOP

		// min and max distances
		double min = 0.5;
		double max = 3;

		// difference vector
		Vector3f camToObject = new Vector3f();
		camToObject.sub(staticCOP, cop);

		// now the new position lies on this line and we can subtract some
		// vector from the target object position, but we should get the length
		// right first. To do this we have to scale the vector.

		// get the distance to the target object
		float cameraDistance = camToObject.length();

		// This is the vector that will be subtracted from the object position
		Vector3f sub = new Vector3f(camToObject);
		// the limits
		if (cameraDistance < min) {
			sub.normalize();
			sub.scale((float) min);
		} else if (cameraDistance > max) {
			sub.normalize();
			sub.scale((float) max);
		}

		cop.sub(staticCOP, sub);

		// Set the VUV vector.
		vuv.x = 0;
		vuv.y = 0;
		vuv.z = 1;
		trans.transform(vuv);

		// Set the VRP, the point to look at
		vrp.set(player.getPosition());
	}

}

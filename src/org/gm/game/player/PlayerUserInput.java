/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.game.player;

import java.awt.event.KeyEvent;

import javax.media.opengl.GLCanvas;

import org.gm.game.UserInput;

public class PlayerUserInput extends UserInput {

	protected byte acceleration = 0;
	protected boolean[] yaw   = {false, false};
	protected boolean[] pitch = {false, false};
	protected boolean[] roll  = {false, false};

	protected boolean isFired = false;

	protected Player player;

	public PlayerUserInput(GLCanvas canvas, Player player) {
		super(canvas);
		this.player = player;
	}

	/**
	 * Wants to fire torpedos.
	 * @return true if the player can fire a torpedo
	 */
	public boolean wantFire() {
		return isFired;
	}

	// The actual event handlers

	@Override
	public void keyPressed(KeyEvent event) {
		switch (event.getKeyCode()) {
			case KeyEvent.VK_W:     acceleration =  1; break;
			case KeyEvent.VK_S:     acceleration = -1; break;
			case KeyEvent.VK_A:     yaw[0]   = true; break;
			case KeyEvent.VK_D:     yaw[1]   = true; break;
			case KeyEvent.VK_UP:    pitch[0] = true; break;
			case KeyEvent.VK_DOWN:  pitch[1] = true; break;
			case KeyEvent.VK_LEFT:  roll[0]  = true; break;
			case KeyEvent.VK_RIGHT: roll[1]  = true; break;
			case KeyEvent.VK_F:		isFired  = true; break;
		}
	}

	@Override
	public void keyReleased(KeyEvent event) {
		switch (event.getKeyCode()) {
			case KeyEvent.VK_W:
			case KeyEvent.VK_S:     acceleration = 0; break;
			case KeyEvent.VK_A:
			case KeyEvent.VK_D:     yaw = new boolean[]{false, false}; break;
			case KeyEvent.VK_UP:
			case KeyEvent.VK_DOWN:  pitch = new boolean[]{false, false}; break;
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_RIGHT: roll = new boolean[]{false, false}; break;
		}
	}


}

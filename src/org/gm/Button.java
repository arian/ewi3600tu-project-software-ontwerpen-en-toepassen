/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm;

import javax.media.opengl.GL;
import javax.media.opengl.GLCanvas;

import org.VisibleObject;
import org.VisibleObjectList;
import org.shapes2d.Shape2d;
import org.shapes2d.Tetragon;
import org.shapes2d.Text;

public class Button implements VisibleObject, DelegateEvents {

	public Shape2d baseShape;
	public VisibleObjectList<Shape2d> shapes;
	private boolean hover = false;
	public Events eventHandler;
	public Text label;

	/**
	 * @author Joan
	 *
	 */
	public static interface Events {
		/**
		 * Event that triggers when the pointer enters the button
		 * @param button The current button
		 */
		public void mouseEnter(Button button);

		/**
		 * Event that triggers when the pointer leaves the button
		 * @param button The current button
		 */
		public void mouseLeave(Button button);

		/**
		 * Event that triggers when the button is clicked
		 * @param button The current button
		 */
		public void mouseClick(Button button);

		/**
		 * Event that triggers when a re-align is needed
		 * @param canvas The canvas to realign to
		 * @param button The current button
		 */
		public void realign(Button button, GLCanvas canvas);
	}

	/**
	 * Helper class that implements an empty version of all mouse events
	 */
	public static class EventShim implements Events {

		@Override
		public void mouseEnter(Button button) {}
		@Override
		public void mouseLeave(Button button) {}
		@Override
		public void mouseClick(Button button) {}
		@Override
		public void realign(Button button, GLCanvas canvas) {}

	}

	/**
	 * Constructs the button with a given baseshape, and the single shape in
	 * attached visible objects list
	 * @param shape The baseshape for the new button.
	 * @param label the Label of the button to use
	 */
	public Button(Shape2d shape, String label) {

		this.baseShape = shape;
		shapes = new VisibleObjectList<Shape2d>();
		shapes.add(baseShape);
		this.label = new Text(baseShape, label);
	}

	/**
	 * Constructs the button with a given baseshape, and the single shape in
	 * attached visible objects list
	 * @param shape The baseshape for the new button.
	 */
	public Button(Shape2d shape) {
		this(shape, "");
	}

	/**
	 * Constructs a button with the given rectangle
	 * @param x The most left position of the button
	 * @param y The most right position of the button
	 * @param width The width of the button
	 * @param height The height of the button
	 */
	public Button(float x, float y, float width, float height) {
		this(Tetragon.rectangle(x, y, width, height), "");
	}

	/**
	 * Constructs a button with the given rectangle
	 * @param x The most left position of the button
	 * @param y The most right position of the button
	 * @param width The width of the button
	 * @param height The height of the button
	 * @param label the Label of the button to use
	 */
	public Button(float x, float y, float width, float height, String label) {
		this(Tetragon.rectangle(x, y, width, height), label);
	}

	/**
	 * Determines whether the given point is in the button
	 * @param x The x position of the point
	 * @param y The y position of the point
	 * @return True when the point is in the button false otherwise
	 */
	public boolean pointInButton(int x, int y) {
		return this.baseShape.pointInShape(x, y);
	}

	/**
	 * Sets the mouse listener of this object
	 * @param handler The object containing all mouse events
	 * @return the current button
	 */
	public Button setEventListener(Events handler) {
		eventHandler = handler;
		return this;
	}

	/**
	 * Gets the base shape
	 * @return The baseshape of this object
	 */
	public Shape2d getShape() {
		return baseShape;
	}

	/**
	 * Sets the baseshape
	 * @param shape Shape to set the baseshape to
	 * @return the current object
	 */
	public Button setShape(Shape2d shape) {
		shapes.remove(this.baseShape);
		shapes.add(shape);

		this.baseShape = shape;
		return this;
	}

	public Text getLabel() {
		return this.label;
	}

	public Button setLabel(Text label) {
		this.label = label;
		return this;
	}

	public Button setLabel(String label) {
		this.label = new Text(this.baseShape, label);
		return this;
	}


	/**
	 * Adds an extra shape to the button to render;
	 * @param shape the shape to add to the render list
	 * @return The current button
	 */
	public Button addShape(Shape2d shape) {
		shapes.add(shape);
		return this;
	}

	/**
	 * Removes a shape from the rendering list. This method can not
	 * remove the baseshape of the button
	 * @param shape The shape to remove
	 * @return the current button
	 */
	public Button removeShape(Shape2d shape) {
		if (shape == this.baseShape) {
			return this;
		}
		shapes.remove(shape);
		return this;
	}

	/**
	 * Removes a shape from the rendering list
	 * @param index the index of the shape to remove
	 * @return the current button
	 */
	public Button removeShape(int index) {
		if (index == 0) {
			return this;
		}
		shapes.remove(index);
		return this;
	}

	/**
	 * Triggers the realign and the mouseLeave events on the button.
	 * @param canvas The glcanvas to align the events to
	 * @return The current button
	 */
	public Button triggerInit(GLCanvas canvas) {
		if (this.eventHandler != null) {
			this.eventHandler.mouseLeave(this);
			this.eventHandler.realign(this, canvas);
		}
		return this;
	}

	/* (non-Javadoc)
	 * @see org.VisibleObject#display(javax.media.opengl.GL)
	 */
	@Override
	public void display(GL gl, int dt) {
		// Reverse iteration is preferable
		for(int i = shapes.size() - 1; i >= 0; i--){
			shapes.get(i).display(gl, dt);
		}

		if (label != null) {
			label.display(gl, dt);
		}
	}

	/* (non-Javadoc)
	 * @see org.gm.DelegateMouseEvents#mouseMoved(int, int)
	 */
	@Override
	public Button mouseMoved(int x, int y) {
		setHover(this.pointInButton(x, y));
		return this;
	}

	/* (non-Javadoc)
	 * @see org.gm.DelegateMouseEvents#mouseClicked(int, int)
	 */
	@Override
	public Button mouseClicked(int x, int y) {
		if (eventHandler != null && this.pointInButton(x, y)) {
			eventHandler.mouseClick(this);
		}
		return this;
	}

	/* (non-Javadoc)
	 * @see org.gm.DelegateEvents#realign(javax.media.opengl.GLCanvas)
	 */
	@Override
	public Button realign(GLCanvas canvas) {
		if (eventHandler != null) {
			eventHandler.realign(this, canvas);
		}
		return this;
	}

	/**
	 * Sets the hover state, and triggers events when it is changed
	 * @param status The current hover state (true for hover, false otherwise)
	 */
	protected Button setHover(boolean status) {
		if (eventHandler != null && status != hover) {
			if (!hover) {
				eventHandler.mouseEnter(this);
			}
			else {
				eventHandler.mouseLeave(this);
			}
			hover = status;
		}
		return this;
	}

}

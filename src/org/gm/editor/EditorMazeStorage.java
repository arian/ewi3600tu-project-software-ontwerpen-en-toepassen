/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.vecmath.AxisAngle4f;
import javax.vecmath.Vector3f;

import org.db.Connector;
import org.gm.MazeBuilder;
import org.gm.MazeDataException;
import org.gm.MazeStorage;
import org.gm.MazeStorageObjectOptions;

public class EditorMazeStorage extends MazeStorage {

	/**
	 * Gets an maze by a given id
	 * @param id the id to create the maze with
	 * @return The newly created maze
	 * @throws MazeDataException if the maze could not be created
	 */
	public static MazeData getMaze(int id) throws MazeDataException {

		try {
			Object[] querydata = {id};
			ResultSet res;
			Connector db = Connector.getInstance();
			res = db.query("SELECT leveldata, name FROM levels WHERE id = ? ", querydata);
			if (!res.next()) {
				throw new MazeDataException("Maze not found (no rows)");
			}
			String mazeString = res.getString(1);
			String name = res.getString(2);
			res.close(); // Free up resources

			return deserializeMaze(mazeString, name);

		} catch (SQLException e) {
			throw new MazeDataException("Maze not found (query failed: " + e.getMessage() + ")");
		} catch (ClassNotFoundException e) {
			throw new MazeDataException("Maze not found (failed to connecto to db)");
		}
	}

	/**
	 * Stores the maze
	 * @param id The id to store at
	 * @param maze The MazeData to store
	 */
	public static void storeMaze(int id, MazeData maze) {
		try {

			Vector3f start = getMazeStart(maze);
			AxisAngle4f startRot = getMazeRotation(maze);

			if (id == -1) {

				Object[] row = {maze.getName(), serializeMaze(maze), start.x, start.y, start.z, startRot.angle, startRot.x, startRot.y, startRot.z};
				org.db.Connector.getInstance().noResultQuery("INSERT INTO levels ("
						+ "name, leveldata, start_x, start_y, start_z, start_rot_w, start_rot_x, start_rot_y, start_rot_z"
						+ ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", row);
			}
			else {
				Object[] row = {id, maze.getName(), serializeMaze(maze),  start.x, start.y, start.z, startRot.angle, startRot.x, startRot.y, startRot.z};
				org.db.Connector.getInstance().noResultQuery("REPLACE INTO levels ("
						+ "id, name, leveldata, start_x, start_y, start_z, start_rot_w, start_rot_x, start_rot_y, start_rot_z"
						+ ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Deserializes the maze from a string
	 * @param dataString the data to deserialize
	 * @return the deserialized maze into a mazedata object
	 * @throws MazeDataException
	 */
	public static MazeData deserializeMaze(String dataString, String name) throws MazeDataException {
		return MazeStorage.deserializeMaze(dataString, new MazeBuilder<MazeData>() {

			MazeData maze;
			@Override
			public void createWithSize(int x, int y, int z) throws MazeDataException {
				maze = new MazeData(x, y, z);
			}

			@Override
			public void setObjectAtLocation(int x, int y, int z, MazeStorageObjectOptions opts) {
				maze.setLocation(x, y, z, EditorObject.createFromCode(opts));
			}

			@Override
			public void setName(String name) {
				maze.setName(name);
			}

			@Override
			public MazeData getResult() {
				return maze;
			}

			@Override
			public void close() {}

		}, name);
	}

	/**
	 * Gets the start position of the maze
	 * @param maze the MazeData object to extract the start position from
	 * @return a vector containing the start position of the maze
	 */
	private static Vector3f getMazeStart(MazeData maze) {
		int xSize = maze.getXSize();
		int ySize = maze.getYSize();
		int zSize = maze.getZSize();

		for (int x = 0; x < xSize; x++) {
			for (int y = 0; y < ySize; y++) {
				for (int z = 0; z < zSize; z++) {

					EditorObject p = maze.getLocation(x, y, z);
					if (p != null && (p.getOptions().pickup & PickupTypes.PICKUP_START) != 0) {
						return new Vector3f(x, y, z);
					}
				}
			}
		}
		return new Vector3f(0, 0, 0);
	}

	/**
	 * Gets the start rotation of the maze
	 * @param maze the MazeData object to extract the start position from
	 * @return a vector containing the start rotation of the maze
	 */
	private static AxisAngle4f getMazeRotation(MazeData maze) {
		int xSize = maze.getXSize();
		int ySize = maze.getYSize();
		int zSize = maze.getZSize();

		for (int x = 0; x < xSize; x++) {
			for (int y = 0; y < ySize; y++) {
				for (int z = 0; z < zSize; z++) {

					EditorObject p = maze.getLocation(x, y, z);
					if (p != null && (p.getOptions().pickup & PickupTypes.PICKUP_START) != 0) {
						int[] rotVector = p.getOptions().rotationVector;
						float rotation = p.getOptions().rotation;

						return new AxisAngle4f(rotVector[0], rotVector[1], rotVector[2], rotation);
					}
				}
			}
		}
		return new AxisAngle4f(0, 0, 0, 0);
	}

	/**
	 * Serializes the maze into a string
	 * @param maze the maze to serialize
	 * @return the maze as string
	 */
	private static String serializeMaze(MazeData maze) {

		String res = "";
		int xSize = maze.getXSize();
		int ySize = maze.getYSize();
		int zSize = maze.getZSize();

		res += "u";
		for (int x = 0; x < xSize; x++) {
			res += "v";
			for (int y = 0; y < ySize; y++) {
				res += "w";
				for (int z = 0; z < zSize; z++) {

					EditorObject p = maze.getLocation(x, y, z);
					if (p == null) {
						res += " ";
					}
					else {
						res += p.getOptions().serialize();
					}
					res += "z";
				}
				res += "y";
			}
			res += "x";
		}

		return res;
	}
}

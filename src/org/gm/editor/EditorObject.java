/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

import java.util.ArrayList;

import javax.media.opengl.GL;

import org.VisibleObject;
import org.gm.BlockTypes;
import org.gm.MazeStorageObjectOptions;
import org.shapes2d.Point2d;
import org.shapes2d.Shape2d;
import org.shapes2d.Tetragon;

public class EditorObject implements VisibleObject {

	private final Shape2d shape;
	private final MazeStorageObjectOptions opts;
	private ArrayList<Tetragon> pickupShapes = null;

	/**
	 * Creates a new EditorObject with the given shape
	 * @param shape The shape to associate
	 */
	public EditorObject(Shape2d shape, MazeStorageObjectOptions opts) {
		this.shape = shape;
		this.opts = opts;
		this.setPickups(opts.pickup);
	}

	@Override
	public void display(GL gl, int dt) {
		shape.display(gl, dt);
		if (this.pickupShapes != null) {
			for (Tetragon pickupShape : pickupShapes) {
				pickupShape.display(gl, dt);
			}
		}
	}

	/**
	 * Rotates the object by a quarter
	 */
	public void rotate() {
		EditorObject.rotateShape(this.shape);
		this.opts.rotation += 90;
	}

	/**
	 * sets the pickups to this object
	 * @param pickupsCode the id of the pickup to connect with this shape
	 */
	public void setPickups(int pickupsCode) {
		this.opts.pickup = pickupsCode;
		this.pickupShapes = getPickupShapes(pickupsCode);
	}

	/**
	 * Toggle a pickup to this object
	 * @param pickupId the id of the pickup to toggle with this shape
	 */
	public void togglePickup(int pickupId) {
		this.opts.pickup ^= pickupId;
		this.pickupShapes = getPickupShapes(this.opts.pickup);
	}

	/**
	 * @return The options for this object
	 */
	public MazeStorageObjectOptions getOptions() {
		return opts;
	}

	/**
	 * Gets the shape associated with the EditorObject
	 * @return the current shape
	 */
	public Shape2d getShape() {
		return this.shape;
	}

	public ArrayList<Tetragon> getPickupShapes() {
		return this.pickupShapes;
	}

	/**
	 * Creates an EditorObject that matches an integer value with code
	 * @param opts The options to create an object from
	 * @return The newly created editorObject
	 */
	public static EditorObject createFromCode(MazeStorageObjectOptions opts) {

		Tetragon eShape = Tetragon.rectangle(0, 0, 100, 100);

		eShape.setTexture(BlockTypes.getInstance().getByCode(opts.type).texture);

		int r = (int) (opts.rotation / 90);
		for (int i = 0; i < r; i++) {
			rotateShape(eShape);
		}
		return new EditorObject(eShape, opts);
	}

	/**
	 * Creates an EditorObject with a given block type code
	 * @param type The code that must match
	 * @return The newly created editorObject
	 */
	public static EditorObject createFromCode(int type) {
		return EditorObject.createFromCode(new MazeStorageObjectOptions(type));
	}

	/**
	 * Rotates the shape by 90 degrees
	 * @param shape The shape to rotate
	 */
	private static void rotateShape(Shape2d shape) {
		if (shape instanceof Tetragon) {
			Tetragon s = (Tetragon) shape;
			Point2d p0 = new Point2d(s.getPoint(0));
			for (int i = 0; i < 3; i++) {
				s.setPoint(i, new Point2d(s.getPoint(i + 1)));
			}
			s.setPoint(3, p0);
		}
		else {
			shape.rotate(Math.PI / 2, shape.getCenter());
		}
	}

	/**
	 * @param pickupCode the int that represent the pickup
	 * @return a list of shapes that represents the pickups of this shape
	 */
	private static ArrayList<Tetragon> getPickupShapes(int pickupCode) {

		ArrayList<Tetragon> res = new ArrayList<Tetragon>();
		ArrayList<PickupTypes.PickupType> pickups = PickupTypes.getInstance().getList();
		for (PickupTypes.PickupType pickup : pickups) {

			if ((pickupCode & pickup.code) != 0) {

				Tetragon shape = Tetragon.rectangle(100, 100, 100, 100);
				shape.setTexture(pickup.texture);
				res.add(shape);
			}

		}
		return res;
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

import org.VisibleObjectList;
import org.shapes2d.Line;
import org.shapes2d.Shape2d;
import org.shapes2d.Tetragon;

public class GridRenderer extends VisibleObjectList<Shape2d> implements GridChangeListener {

	private static final long serialVersionUID = 2607639445635741061L;
	private SnapGrid grid;

	/**
	 * Creates a gridRenderer
	 * @param grid the SnapGrid to render
	 */
	public GridRenderer(SnapGrid grid) {
		this.grid = grid;
		grid.addChangeListener(this);
		gridChanged(grid);
	}

	/**
	 * Disables the grid renderer.
	 * It should be GC'ed after calling this method
	 */
	public void close() {
		this.clear();
		this.grid.removeChangeListener(this);
	}

	@Override
	public void gridChanged(SnapGrid grid) {
		this.clear();

		float xInc = grid.getWidth() / grid.getSnapXCount();
		float yInc = grid.getHeight() /grid.getSnapYCount();

		float xStart = grid.getX();
		float yStart = grid.getY();
		float xEnd = grid.getWidth() + grid.getX();
		float yEnd = grid.getHeight() + grid.getY();

		Shape2d back = Tetragon.rectangle(grid).setColor(1f, 1f, 1f, 0.6f);
		this.add(back);

		for (float x = xStart; x < xEnd; x += xInc) {
			this.add(new Line(x, yStart, x, yEnd));
		}

		for (float y = yStart; y < yEnd; y += yInc) {
			this.add(new Line(xStart, y, xEnd, y));
		}


	}
}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/

package org.gm.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.net.MalformedURLException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Shortcuts extends JFrame {

	private static final long serialVersionUID = -7136039897361957094L;

	public Shortcuts() {
		super("Shortcuts for the editor");

		try {

			String ds = System.getProperty("file.separator");
			String basedir = System.getProperty("user.dir") + ds + "assets" + ds + "images" + ds;
			String image = "shortcuts.png".replace("/", ds);

			this.setResizable(false);
			this.setBounds(0, 0, 850, 700);



			Dimension size = new Dimension();
			final Image help = new ImageIcon(new File(basedir + image).toURI().toURL()).getImage();
			size.width = help.getWidth(null);
	        size.height = help.getHeight(null);

	        JPanel jimage = new JPanel() {
				private static final long serialVersionUID = -8964756221573741728L;

				@Override
				public void paint(Graphics g) {

	                Graphics2D g2d = (Graphics2D) g;
	                g2d.drawImage(help, 0, 0, null);
	            }
	        };
	        jimage.setPreferredSize(size);

	        JScrollPane scrollbar = new JScrollPane(jimage);
	        scrollbar.setBackground(new Color(0, 0, 0));
	        this.getContentPane().add(scrollbar, BorderLayout.CENTER);

	        this.setVisible(true);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}


	}

	public static void main(String[] args) {
		new Shortcuts();
	}


}

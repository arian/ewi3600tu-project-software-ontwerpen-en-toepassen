/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

import org.shapes2d.Rectangle2d;
import org.shapes2d.Shape2d;

/**
 * @author Groep03
 * Helper class that connects a SnapGrid with MazeData object so
 * that the drawing states and sizes are synchronized
 */
public class GridDataConnector implements GridChangeListener {

	private int zPosition;

	private final MazeData maze;
	private final SnapGrid grid;

	/**
	 * Constructs the GridDataConnector
	 * @param maze The MazeData to connect
	 * @param grid The SnapGrid to connect
	 */
	public GridDataConnector(MazeData maze, SnapGrid grid) {

		this.grid = grid;
		this.maze = maze;

		grid.setSnapXCount(maze.getXSize());
		grid.setSnapYCount(maze.getYSize());

		grid.addChangeListener(this);
	}

	/**
	 * Constructs the GridDataConnector with an grid matching the rect
	 * @param maze The maze to connect
	 * @param rect The boundaries of the Grid to construct
	 */
	public GridDataConnector(MazeData maze, Rectangle2d rect) {
		this(maze, new SnapGrid(maze.getXSize(), maze.getYSize(), rect));
	}

	/* (non-Javadoc)
	 * @see org.gm.editor.GridChangeListener#gridChanged()
	 */
	@Override
	public void gridChanged(SnapGrid grid) {
		this.alignGridShapes();
	}

	/**
	 * Aligns all maze objects to the current connected grid
	 * @return The GridDataConnector
	 */
	public GridDataConnector alignGridShapes() {
		if (this.grid.getWidth() == 0 || this.grid.getHeight() == 0) {
			return this;
		}

		for (int x = 0; x < this.grid.getSnapXCount(); x++) {
			for (int y = 0; y < this.grid.getSnapYCount(); y++) {

				EditorObject obj = this.maze.getLocation(x, y, zPosition);
				if (obj != null) {
					Rectangle2d region = grid.getRectangle(x, y);
					obj.getShape().fit(region);

					// Draw the pickups
					if (obj.getPickupShapes() != null) {
						for (Shape2d pick : obj.getPickupShapes()) {
							pick.fit(region);
						}
					}

				}
			}
		}

		return this;
	}

	/**
	 * @return The current Z-Position of the grid
	 */
	public int getZPosition() {
		return zPosition;
	}

	/**
	 * Will change the zPosition and silently fail if the zPosition was invalid
	 * @param zPosition The new Z-position of the grid
	 * @return The current GridDataConnector
	 */
	public GridDataConnector setZPosition(int zPosition) {
		// Silently do nothing on invalid values
		if (zPosition < 0 || zPosition >= maze.getZSize()) {
			return this;
		}
		this.zPosition = zPosition;
		this.alignGridShapes();
		return this;
	}
}

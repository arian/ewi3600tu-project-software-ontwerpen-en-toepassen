/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

import java.util.ArrayList;

import org.gm.MazeDataException;

/**
 * @author Joan
 *
 */
public class MazeData {

	private EditorObject[][][] data;
	private String name;

	/**
	 * @param xSize The X size of the MazeData to set
	 * @param ySize The Y size of the MazeData to set
	 * @param zSize The Z size of the MazeData to set
	 * @throws MazeDataException
	 */
	private void createMazeWithSize(int xSize, int ySize, int zSize) throws MazeDataException {
		if (xSize < 1) {
			throw new MazeDataException("xSize too small");
		}
		if (ySize < 1) {
			throw new MazeDataException("ySize too small");
		}
		if (zSize < 1) {
			throw new MazeDataException("zSize too small");
		}

		data = new EditorObject[zSize][ySize][xSize];
	}

	/**
	 * @param xSize The X size of the MazeData to set
	 * @param ySize The Y size of the MazeData to set
	 * @param zSize The Z size of the MazeData to set
	 * @throws MazeDataException
	 */
	public MazeData(int xSize, int ySize, int zSize) throws MazeDataException {
		createMazeWithSize(xSize, ySize, zSize);
	}

	/**
	 * @param grid The grid to fetch the x and y sizes from
	 * @param depth The depth of the grid, the Z size
	 * @throws MazeDataException
	 */
	public MazeData(SnapGrid grid, int depth) throws MazeDataException {
		createMazeWithSize(grid.getSnapXCount(), grid.getSnapYCount(), depth);
	}

	/**
	 * @param maze The data of an maze to clone
	 * @throws MazeDataException
	 */
	public MazeData(MazeData maze) throws MazeDataException {
		int xSize = maze.getXSize();
		int ySize = maze.getYSize();
		int zSize = maze.getZSize();

		createMazeWithSize(xSize, ySize, zSize);

		int x, y, z;
		for (z = 0; z < zSize; z++) {
			for (y = 0; y < ySize; y++) {
				for (x = 0; x < xSize; x++) {
					this.setLocation(x, y, z, maze.getLocation(x, y, z));
				}
			}
		}
	}

	/**
	 * @param x The x plane to look at
	 * @return A list with all object at the given x-plane
	 */
	public ArrayList<EditorObject> getXPlane(int x) {
		ArrayList<EditorObject> res = new ArrayList<EditorObject>();

		int y, z;
		for (y = 0; y < this.getYSize(); y++) {
			for (z = 0; z < this.getZSize(); z++) {
				if (this.data[z][y][x] != null) {
					res.add(this.data[z][y][x]);
				}
			}
		}

		return res;
	}

	/**
	 * @param y The y plane to look at
	 * @return A list with all objects at the given y-plane
	 */
	public ArrayList<EditorObject> getYPlane(int y) {
		ArrayList<EditorObject> res = new ArrayList<EditorObject>();
		int x, z;

		for (x = 0; y < this.getYSize(); x++) {
			for (z = 0; z < this.getZSize(); z++) {
				if (this.data[z][y][x] != null) {
					res.add(this.data[z][y][x]);
				}
			}
		}

		return res;
	}

	/**
	 * @param z The z plane to look at
	 * @return A list with all object at the given z-plane
	 */
	public ArrayList<EditorObject> getZPlane(int z) {
		ArrayList<EditorObject> res = new ArrayList<EditorObject>();

		int x, y;
		for (x = 0; x < this.getXSize(); x++) {
			for (y = 0; y < this.getYSize(); y++) {
				if (this.data[z][y][x] != null) {
					res.add(this.data[z][y][x]);
				}
			}
		}

		return res;
	}

	/**
	 * @return The X size of the maze
	 */
	public int getXSize() {
		return data[0][0].length;
	}

	/**
	 * @return The Y size of the maze
	 */
	public int getYSize() {
		return data[0].length;
	}

	/**
	 * @return The Z Size of the maze
	 */
	public int getZSize() {
		return data.length;
	}

	/**
	 * Sets the editorObject at a given point
	 * @param x The x coordinate to set
	 * @param y The y coordinate to set
	 * @param z The z coordinate to set
	 * @param instance The instance to set at given point
	 * @return The MazeData Object
	 */
	public MazeData setLocation(int x, int y, int z, EditorObject instance) {
		this.data[z][y][x] = instance;
		return this;
	}

	/**
	 * Gets the editorObject at the given point
	 * @param x The x coordinate to get
	 * @param y The y coordinate to get
	 * @param z The z coordinate to get
	 * @return The object at given point (null if nothing is at the given point)
	 */
	public EditorObject getLocation(int x, int y, int z) {
		return this.data[z][y][x];
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

import org.gm.editor.PickupTypes.PickupType;

/**
 * This class handles the various block types of the editor
 * TODO 2.0: get the data externally =)
 */
public class PickupTypes extends TypeList<PickupType> {

	final public static int PICKUP_START      = 1;
	final public static int PICKUP_FINISH     = 2;
	final public static int PICKUP_TORPEDO    = 4;
	final public static int PICKUP_WALL       = 8;
	final public static int PICKUP_SWARMENTER = 16;
	final public static int PICKUP_SWARMLEAVE = 32;
	final public static int PICKUP_SWARM      = 64;
	final public static int PICKUP_SPEEDUP	  = 128;
	final public static int PICKUP_SPEEDDOWN  = 256;
	final public static int PICKUP_LOSECONTROL = 512;
	final public static int PICKUP_FLIPCONTROL = 1024;

	public static class PickupType extends Type {

		public String texture;

		/**
		 * Creates a new pickup type
		 * @param code the code of the pickup type
		 * @param texture the editor texture to associate it with
		 */
		public PickupType(int code, String texture) {
			super(code);
			this.texture = texture;
		};
	}

	public static PickupTypes instance;

	/**
	 * Creates a new PickupTypes object. And initializes all data associated
	 * with it.
	 */
	private PickupTypes() {
		types.add(new PickupType(PICKUP_START,      "start-flag-start.png"));
		types.add(new PickupType(PICKUP_FINISH,     "start-flag-finish.png"));
		types.add(new PickupType(PICKUP_TORPEDO,    "rocket.png"));
		types.add(new PickupType(PICKUP_WALL,       "editor-wall.png"));
		types.add(new PickupType(PICKUP_SWARMENTER, "swarm-enter.png"));
		types.add(new PickupType(PICKUP_SWARMLEAVE, "swarm-leave.png"));
		types.add(new PickupType(PICKUP_SWARM,      "swarm.png"));
		types.add(new PickupType(PICKUP_SPEEDUP,	"speedup.png"));
		types.add(new PickupType(PICKUP_SPEEDDOWN,  "speeddown.png"));
		types.add(new PickupType(PICKUP_LOSECONTROL, "steering-wheel-losecontrol.png"));
		types.add(new PickupType(PICKUP_FLIPCONTROL, "steering-wheel-flipcontrol.png"));
	}

	/**
	 * Gets the instance of PickupTypes. If none exists one will be created.
	 * @return the PickupTypes object
	 */
	public static PickupTypes getInstance() {
		if (instance == null) {
			instance = new PickupTypes();
		}
		return instance;
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

public abstract class Type {

	public int code;

	/**
	 * Creates a type with a given code
	 * @param code the code to  set
	 */
	public Type(int code) {
		this.code = code;
	}
}

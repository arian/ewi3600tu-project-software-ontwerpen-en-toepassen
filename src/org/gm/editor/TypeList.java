/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

import java.util.ArrayList;


public abstract class TypeList<T extends Type> {

	protected ArrayList<T> types = new ArrayList<T>();

	/**
	 * Constructs the typelist
	 */
	protected TypeList() {
		types = new ArrayList<T>();
	};

	/**
	 * Gets the arraylist of types
	 * @return the arraylist of types
	 */
	public ArrayList<T> getList() {
		return types;
	}

	/**
	 * Gets the type of the object by the code
	 * @param code
	 * @return A object of the matched Type
	 */
	public T getByCode(int code) {
		for (T type : types) {
			if (type.code == code) {
				return type;
			}
		}
		return null;
	}

}

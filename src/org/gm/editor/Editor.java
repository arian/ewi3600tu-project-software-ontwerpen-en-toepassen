/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.swing.JOptionPane;

import org.ModalFrame;
import org.ResponseListener;
import org.TextureManager;
import org.TunnelRunner;
import org.VisibleObject;
import org.VisibleObjectList;
import org.gm.BackgroundSound;
import org.gm.BlockTypes;
import org.gm.Button;
import org.gm.ButtonList;
import org.gm.Factory;
import org.gm.FactoryException;
import org.gm.FactoryOptions;
import org.gm.GameMode;
import org.gm.MazeDataException;
import org.shapes2d.Color;
import org.shapes2d.FontManager;
import org.shapes2d.Point2d;
import org.shapes2d.Rectangle2d;
import org.shapes2d.Shape2d;
import org.shapes2d.Tetragon;

public class Editor extends GameMode implements MouseListener, MouseMotionListener, KeyListener {

	private static abstract class EditorButtonEvents extends Button.EventShim  {

		public float vAlign = 0;
		public float verticalOffset = 0;
		public float colorAdaptAmount = 0.3f;
		public Color color;

		public EditorButtonEvents(float vAlign, float verticalOffset, Color color) {
			this.vAlign = vAlign;
			this.verticalOffset = verticalOffset;
			this.color = color;
		};

		@Override
		public void mouseEnter(Button button) {
			((Tetragon) button.getShape()).setTexture("button-active.png");
			FontManager.getInstance().clearRenderers();
			//getColor().lighten(colorAdaptAmount);
		}

		@Override
		public void mouseLeave(Button button) {
			((Tetragon) button.getShape()).setTexture("button-inactive.png");
			FontManager.getInstance().clearRenderers();
			//button.getLabel().setColor(color);
			//button.getShape().setColor(new Color(color));
		}

		@Override
		public void realign(Button button, GLCanvas canvas) {
			float y = (vAlign == 0) ? y = -verticalOffset : (vAlign * canvas.getHeight()) - verticalOffset;
			button.getShape().fit(canvas.getWidth() - 190, y, 180, 40);
		}
	}

	// Screen size.
	private int screenWidth = 800, screenHeight = 600;
	private final VisibleObjectList<VisibleObject> visibleObjects = new VisibleObjectList<VisibleObject>();
	private ButtonList buttons;
	private Shape2d cursor;
	private SnapGrid grid;
	private MazeData maze;
	private Tetragon background;
	private GridDataConnector mazeGrid;
	private int putType = 1;
	private int doClearFonts;

	private SnapGrid.XY lastPosition;
	private drawModes drawMode;
	private layModes layMode;


	public enum drawModes {
		IDLE,
		ADD,
		REMOVE,
	}
	public enum layModes {
		OBJECTS,
		PICKUPS
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {

		GL gl = drawable.getGL();

		// Set the new screen size and adjusting the viewport
		screenWidth = width;
		screenHeight = height;

		gl.glViewport(0, 0, screenWidth, screenHeight);

		// Update the projection to an orthogonal projection using the new screen size
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(0, screenWidth, 0, screenHeight, -1, 1);

		if (grid != null) {
			grid.setWidth(width - 220);
			grid.setHeight(height - 20);
		}
		if (buttons != null) {
			buttons.realign();
		}
		if (background != null) {
			background.setPoints(new Point2d(0, 0), new Point2d(width, height));
		}
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);

		GL gl = drawable.getGL();
		// Set the clear color and clear the screen.
		gl.glClearColor(0.95f, 0.95f, 0.95f, 1);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT);

		visibleObjects.display(gl, deltaTime);

		if (mazeGrid != null) {
			ArrayList<EditorObject> editorObjects = maze.getZPlane(mazeGrid.getZPosition());
			for (EditorObject obj : editorObjects) {
				obj.display(gl, deltaTime);
			}
		}

		// to get fonts at the 5th frame
		if (doClearFonts > 0 && --doClearFonts == 0) {
			FontManager.getInstance().clearRenderers();
			gl.glEnable(GL.GL_BLEND);
			gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		}

		gl.glFlush();
	}

	@Override
	public void initMode(GLAutoDrawable drawable, FactoryOptions options) {
		super.initMode(drawable, options);

		doClearFonts = 5;

		options.getOwner().setResizable(false);
		options.getOwner().setSize(1000, 800);

		screenWidth = 1000;
		screenHeight = 800;

		// Retrieve the OpenGL handle, this allows us to use OpenGL calls.
		GL gl = drawable.getGL();

		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glViewport(0, 0, drawable.getWidth(), drawable.getHeight());

		gl.glOrtho(0, screenWidth, 0, screenHeight, -1, 1);

		// Set the matrix mode to GL_MODELVIEW, allowing us to manipulate the
		// model-view matrix.
		gl.glMatrixMode(GL.GL_MODELVIEW);
		// we view the world 'looking forward' from the origin.
		gl.glLoadIdentity();
		// We have a simple 2D application, so we do not need to check for depth
		// when rendering.
		gl.glDisable(GL.GL_DEPTH_TEST);

		TextureManager.getInstance(drawable.getGL()).setEnvMode(GL.GL_REPLACE);

		// Attach the mouse events
		options.getCanvas().addMouseListener(this);
		options.getCanvas().addMouseMotionListener(this);
		options.getCanvas().addKeyListener(this);

		if (buttons != null) {
			buttons.attach();
		}

		if (TunnelRunner.soundEnabled) {
			BackgroundSound.getInstance().play("assets/sounds/starwars.mp3");
		}

	}

	@Override
	public void deInitMode(GLAutoDrawable drawable, FactoryOptions options) {
		super.deInitMode(drawable, options);
		buttons.detach();
		// Detach the mouse events
		options.getCanvas().removeMouseListener(this);
		options.getCanvas().removeMouseMotionListener(this);
		options.getCanvas().removeKeyListener(this);

		// Make window resizable again
		options.getOwner().setResizable(true);
	}

	@Override
	public void initDisplay(GLAutoDrawable drawable, FactoryOptions options){
		super.initDisplay(drawable, options);

		grid = new SnapGrid(10, 10,
				initOptions.getCanvas().getWidth() - 220,
				initOptions.getCanvas().getHeight() - 20, 10, 10);

		// The maze as a new empty maze
		setMaze(-1);

		// Background
		background = Tetragon.rectangle(0, 0, screenWidth, screenHeight);
		background.setColor(1, 1, 1, 1f);
		background.setTexture("editor-background.jpg");
		visibleObjects.add(background);

		// Create button list
		buttons = new ButtonList(options.getCanvas());
		visibleObjects.add(buttons);

		// Z axis +1 button
		Button nBut = new Button(0, 0, 100, 100);
		nBut.setLabel("Depth -1").getLabel()
			.setSize(20)
			.setColor(1f, 1f, 1f);
		nBut.setEventListener(new EditorButtonEvents(0, -10, new Color(0, 0, 1)) {

			@Override
			public void mouseClick(Button button) {
				mazeGrid.setZPosition(mazeGrid.getZPosition() - 1);
			}

		});
		nBut.triggerInit(this.initOptions.getCanvas());
		buttons.addButton(nBut);

		// Z axis -1 button
		nBut = new Button(0, 0, 100, 100);
		nBut.setLabel("Depth +1").getLabel()
			.setSize(20)
			.setColor(1f, 1f, 1f);
		nBut.setEventListener(new EditorButtonEvents(0, -60, new Color(0, 0, 1)){

			@Override
			public void mouseClick(Button button) {
				mazeGrid.setZPosition(mazeGrid.getZPosition() + 1);
			}
		});
		nBut.triggerInit(this.initOptions.getCanvas());
		buttons.addButton(nBut);

		// Shortcuts button
		nBut = new Button(0, 0, 100, 100);
		nBut.setLabel("Shortcuts").getLabel()
			.setSize(20)
			.setColor(1f, 1f, 1f);
		nBut.setEventListener(new EditorButtonEvents(0, -260, new Color(0, 0, 1)){

			@Override
			public void mouseClick(Button button) {
				new Shortcuts();
			}

		});
		nBut.triggerInit(this.initOptions.getCanvas());
		buttons.addButton(nBut);


		// Shortcuts button
		nBut = new Button(0, 0, 100, 100);
		nBut.setLabel("Set name").getLabel()
			.setSize(20)
			.setColor(1f, 1f, 1f);
		nBut.setEventListener(new EditorButtonEvents(0, -210, new Color(0, 0, 1)){

			@Override
			public void mouseClick(Button button) {
				maze.setName(JOptionPane.showInputDialog(new ModalFrame("Give level name"), "Please enter level name:", maze.getName()));
			}

		});
		nBut.triggerInit(this.initOptions.getCanvas());
		buttons.addButton(nBut);

		// Load level button
		nBut = new Button(0, 0, 100, 100);
		nBut.setLabel("Load").getLabel()
			.setSize(20)
			.setColor(1f, 1f, 1f);
		nBut.setEventListener(new EditorButtonEvents(0, -160, new Color(0, 0, 1)){

			@Override
			public void mouseClick(Button button) {
				new LoadFrame(new ResponseListener<Integer>() {

					@Override
					public void respond(Integer res) {
						setMaze(res);
					}

				}, true);
			}

		});
		nBut.triggerInit(this.initOptions.getCanvas());
		buttons.addButton(nBut);

		// Save button
		nBut = new Button(0, 0, 100, 100);
		nBut.setLabel("Save").getLabel()
			.setSize(20)
			.setColor(1f, 1f, 1f);
		nBut.setEventListener(new EditorButtonEvents(0, -110, new Color(0, 0, 1)) {

			@Override
			public void mouseClick(Button button) {
				new LoadFrame(new ResponseListener<Integer>() {

					@Override
					public void respond(Integer res) {
						EditorMazeStorage.storeMaze(res, maze);
					}
				}, true);
			}

		});
		nBut.triggerInit(this.initOptions.getCanvas());
		buttons.addButton(nBut);

		// Back to menu button
		nBut = new Button(0, 0, 100, 100);
		nBut.setLabel("Menu").getLabel()
			.setSize(20)
			.setColor(1f, 1f, 1f);
		nBut.setEventListener(new EditorButtonEvents(1, 50, new Color(0, 0, 1)) {

			@Override
			public void mouseClick(Button button) {
				try {
					Factory.getInstance().set("Menu");
				} catch (FactoryException e) {
					e.printStackTrace();
				}
			}
		});
		nBut.triggerInit(this.initOptions.getCanvas());
		buttons.addButton(nBut);

		int pos = 0;
		for (BlockTypes.BlockType type : BlockTypes.getInstance().getList()) {
			addObjectTypeButton(type, pos++);
		}
		for (PickupTypes.PickupType type : PickupTypes.getInstance().getList()) {
			addPickupTypeButton(type, pos++);
		}

		// Cursor
		cursor = Tetragon.square(0, 0, 50);
		cursor.setColor(new Color(0xe08c34, 0.8f));
		visibleObjects.add(cursor);
		visibleObjects.add(new GridRenderer(grid));
	}

	/**
	 * Set the maze by a Database ID. If the ID equals -1 the maze will be empty
	 * @param id
	 */
	public void setMaze(int id) {
		try {
			if (id <= 0) {
				maze = new MazeData(10, 10, 10);
			} else {
				maze = EditorMazeStorage.getMaze(id);
			}
			grid.removeChangeListener();
			mazeGrid = new GridDataConnector(maze, grid);
			mazeGrid.alignGridShapes();
		} catch (MazeDataException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds a button for a specific block type
	 * @param type the type of the block
	 * @param pos the position
	 */
	public void addObjectTypeButton(final BlockTypes.BlockType type, final int pos) {
		final Tetragon butShape = Tetragon.rectangle(0, 0, 100, 100);
		butShape.setColor(0, 0, 0);
		butShape.setTexture(type.texture);

		final Tetragon back = new Tetragon(butShape).setTexture("editor-button.png");

		Button newButton = new Button(butShape).addShape(back);
		newButton.setEventListener(new Button.EventShim(){

			@Override
			public void mouseEnter(Button button) {
				back.setTexture("editor-button-active.png");
			}

			@Override
			public void mouseLeave(Button button) {
				back.setTexture("editor-button.png");
			}

			@Override
			public void mouseClick(Button button) {
				putType = type.code;
				layMode = layModes.OBJECTS;
			}

			@Override
			public void realign(Button button, GLCanvas canvas) {

				int x = pos % 3;
				int y = pos / 3;

				butShape.fit(
						(canvas.getWidth() - 190) + (x * 65),
						(canvas.getHeight() - 150) - (y * 65),
						60f, 60f
				);
			}

		}).triggerInit(this.initOptions.getCanvas());

		buttons.addButton(newButton);
	}

	/**
	 * Adds a button for a specific pickup type
	 * @param type the type of the pickup
	 * @param pos the position
	 */
	public void addPickupTypeButton(final PickupTypes.PickupType type, final int pos) {
		final Tetragon butShape = Tetragon.rectangle(0, 0, 100, 100);
		butShape.setColor(0, 0, 0);
		butShape.setTexture(type.texture);

		final Tetragon back = new Tetragon(butShape).setTexture("editor-button.png");

		Button newButton = new Button(butShape).addShape(back);
		newButton.setEventListener(new Button.EventShim(){

			@Override
			public void mouseEnter(Button button) {
				back.setTexture("editor-button-active.png");
			}

			@Override
			public void mouseLeave(Button button) {
				back.setTexture("editor-button.png");
			}

			@Override
			public void mouseClick(Button button) {
				putType = type.code;
				layMode = layModes.PICKUPS;
			}

			@Override
			public void realign(Button button, GLCanvas canvas) {

				int x = pos % 3;
				int y = pos / 3;

				butShape.fit(
						(canvas.getWidth() - 190) + (x * 65),
						(canvas.getHeight() - 150) - (y * 65),
						60f, 60f
				);
			}

		}).triggerInit(this.initOptions.getCanvas());

		buttons.addButton(newButton);
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		int x = event.getX();
		int y = super.initOptions.getCanvas().getHeight() - event.getY();
		int but = event.getButton();

		SnapGrid.XY xy = grid.snapInt(x, y);

		if (layMode == layModes.OBJECTS && xy != null) {
			EditorObject objAtXYZ = maze.getLocation(xy.x, xy.y, mazeGrid.getZPosition());

			if (but == MouseEvent.BUTTON1) {

				// Add at maze position
				if (objAtXYZ == null || objAtXYZ.getOptions().type != putType) {

					Tetragon square = Tetragon.rectangle(grid.snapRectangle(x, y));
					square.setColor(0.5f, 0.4f, 0.4f, 0.5f);

					//visibleObjects.add(square);
					maze.setLocation(xy.x, xy.y, mazeGrid.getZPosition(), EditorObject.createFromCode(putType));
					mazeGrid.alignGridShapes();

				}
				else {
					//visibleObjects.remove(maze[xy[0]][xy[1]]);
					maze.setLocation(xy.x, xy.y, mazeGrid.getZPosition(), null);
				}
			}
			if (but == MouseEvent.BUTTON3) {
				if (objAtXYZ != null) {
					objAtXYZ.rotate();
				}
			}
		}
		else if (layMode == layModes.PICKUPS && xy != null) {
			EditorObject objAtXYZ = maze.getLocation(xy.x, xy.y, mazeGrid.getZPosition());

			if (but == MouseEvent.BUTTON1 && objAtXYZ != null) {
				objAtXYZ.togglePickup(putType);
				mazeGrid.alignGridShapes();
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent event) {
	}

	@Override
	public void mouseReleased(MouseEvent event) {
		lastPosition = null;
		drawMode = drawModes.IDLE;
	}

	@Override
	public void mouseDragged(MouseEvent event) {

		int x = event.getX();
		int y = super.initOptions.getCanvas().getHeight() - event.getY();

		SnapGrid.XY xy = grid.snapInt(x, y);
		if (xy == null) {
			return;
		}

		if (lastPosition != null && lastPosition.x == xy.x && lastPosition.y == xy.y) {
			return;
		}
		if (layMode != layModes.OBJECTS) {
			return;
		}

		lastPosition = xy;

		EditorObject objAtXYZ = maze.getLocation(xy.x, xy.y, mazeGrid.getZPosition());

		if (drawMode == drawModes.IDLE) {
			if (objAtXYZ == null || objAtXYZ.getOptions().type != putType) {
				drawMode = drawModes.ADD;
			}
			else {
				drawMode = drawModes.REMOVE;
			}
		}

		// Add at maze position
		if (drawMode == drawModes.ADD) {

			Tetragon square = Tetragon.rectangle(grid.snapRectangle(x, y));
			square.setColor(0.5f, 0.4f, 0.4f, 0.5f);

			//visibleObjects.add(square);
			maze.setLocation(xy.x, xy.y, mazeGrid.getZPosition(), EditorObject.createFromCode(putType));
			mazeGrid.alignGridShapes();

		}
		else {
			//visibleObjects.remove(maze[xy[0]][xy[1]]);
			maze.setLocation(xy.x, xy.y, mazeGrid.getZPosition(), null);
		}
	}

	@Override
	public void mouseMoved(MouseEvent event) {

		int x = event.getX();
		int y = super.initOptions.getCanvas().getHeight() - event.getY();

		Rectangle2d rect = grid.snapRectangle(x, y);
		if (rect != null) {
			cursor.fit(rect);
			cursor.getColor().setA(0.8f);
		}
		else {
			cursor.getColor().setA(0f);
		}
		buttons.realign();
	}

	@Override
	public void keyPressed(KeyEvent event) {
	}

	@Override
	public void keyReleased(KeyEvent event) {

	}

	@Override
	public void keyTyped(KeyEvent event) {
		switch (event.getKeyChar()) {
		case '+': case '=': mazeGrid.setZPosition(mazeGrid.getZPosition() + 1); break;
		case '-': mazeGrid.setZPosition(mazeGrid.getZPosition() - 1); break;
		case 'c': putType = 2; layMode = layModes.OBJECTS; break; // Shortcut for corner
		case 'h': putType = 1; layMode = layModes.OBJECTS; break; // Shortcut for straight track horizontal
		case 'v': putType = 6; layMode = layModes.OBJECTS; break; // Shortcut for straight track vertical
		case 'u': putType = 4; layMode = layModes.OBJECTS; break; // Shortcut for straight corner up
		case 'd': putType = 5; layMode = layModes.OBJECTS; break; // Shortcut for straight corner down
		case 's': layMode = layModes.PICKUPS; putType = PickupTypes.PICKUP_START; break; // Shortcut for start
		case 'f': layMode = layModes.PICKUPS; putType = PickupTypes.PICKUP_FINISH; break; // Shortcut for finish
		case 't': layMode = layModes.PICKUPS; putType = PickupTypes.PICKUP_TORPEDO; break; // Shortcut for torpedo
		case 'w': layMode = layModes.PICKUPS; putType = PickupTypes.PICKUP_WALL; break; // Shortcut for wall
		case 'n': layMode = layModes.PICKUPS; putType = PickupTypes.PICKUP_LOSECONTROL; break;
		case 'i': layMode = layModes.PICKUPS; putType = PickupTypes.PICKUP_FLIPCONTROL; break;
		case 'e': layMode = layModes.PICKUPS; putType = PickupTypes.PICKUP_SWARMENTER; break;
		case 'l': layMode = layModes.PICKUPS; putType = PickupTypes.PICKUP_SWARMLEAVE; break;
		case 'z': layMode = layModes.PICKUPS; putType = PickupTypes.PICKUP_SWARM; break;
		case '>': case '.': layMode = layModes.PICKUPS; putType = PickupTypes.PICKUP_SPEEDUP; break;
		case '<': case ',': layMode = layModes.PICKUPS; putType = PickupTypes.PICKUP_SPEEDDOWN; break;

		case '1': mazeGrid.setZPosition(0); break;
		case '2': mazeGrid.setZPosition(1); break;
		case '3': mazeGrid.setZPosition(2); break;
		case '4': mazeGrid.setZPosition(3); break;
		case '5': mazeGrid.setZPosition(4); break;
		case '6': mazeGrid.setZPosition(5); break;
		case '7': mazeGrid.setZPosition(6); break;
		case '8': mazeGrid.setZPosition(7); break;
		case '9': mazeGrid.setZPosition(8); break;
		case '0': mazeGrid.setZPosition(9); break;
		}
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

import java.util.ArrayList;

import org.shapes2d.Point2d;
import org.shapes2d.Rectangle2d;

public class SnapGrid extends Rectangle2d {

		/**
		 * This class represents a X-Y coordinate in the grid
		 */
		protected static class XY {
			public int x;
			public int y;

			/**
			 * Creates an XY representation
			 * @param x the x to set of the XY object
			 * @param y the y to set of the XY object
			 */
			public XY(int x, int y) {
				this.x = x;
				this.y = y;
			}
		}

		private int snapXCount;
		private int snapYCount;
		private final ArrayList<GridChangeListener> changeListeners = new ArrayList<GridChangeListener>();

		/**
		 * @param snapXCount The amount of snappable positions in the x direction
		 * @param snapYCount The amount of snappable positions in the y direction
		 * @param width The width of the snappable region
		 * @param height The height of the snappable region
		 * @param x The lower left point of the snappable region
		 * @param y The upper right point of the snappable region
		 */
		public SnapGrid(int snapXCount, int snapYCount, float width, float height, float x, float y) {
			super(x, y, width, height);
			this.snapXCount = snapXCount;
			this.snapYCount = snapYCount;
		}

		/**
		 * @param snapXCount snapXCount The amount of snappable positions in the x direction
		 * @param snapYCount snapYCount The amount of snappable positions in the y direction
		 * @param width width The width of the snappable region
		 * @param height height The height of the snappable region
		 */
		public SnapGrid(int snapXCount, int snapYCount, float width, float height) {
			this(snapXCount, snapYCount, width, height, 0, 0);
		}


		/**
		 * @param snapXCount snapXCount The amount of snappable positions in the x direction
		 * @param snapYCount snapYCount The amount of snappable positions in the y direction
		 * @param rect The rectangle of the snappable region
		 */
		public SnapGrid(int snapXCount, int snapYCount, Rectangle2d rect) {
			super(rect);
			this.snapXCount = snapXCount;
			this.snapYCount = snapYCount;
		}


		/**
		 * @param x The x point of the SnapGrid
		 * @param y the y point of the SnapGrid
		 * @return The rectangle in which the values are snapped back to xy
		 */
		public Rectangle2d getRectangle(int x, int y) {
			float snapW = this.getWidth() / snapXCount;
			float snapH = this.getHeight() / snapYCount;

			return new Rectangle2d(
				(x * snapW) + this.getX(),
				(y * snapH) + this.getY(),
				snapW,
				snapH
			);
		}

		/**
		 * @param xy An array of integers with as values the x and y values respectively
		 * @return The rectangle in which the values are snapped back to xy
		 */
		public Rectangle2d getRectangle(XY xy) {
			return this.getRectangle(xy.x, xy.y);
		}

		/**
		 * @param x The x coordinate of the point to snap
		 * @param y The y coordinate of the point to snap
		 * @return A rectangle of the region where the point should be snapped at and null if x and y are out of bounds
		 */
		public Rectangle2d snapRectangle (float x, float y) {
			if (!super.contains(x, y)) {
				return null;
			}

			float nX = x-this.getX();
			float nY = y-this.getY();

			float snapW = (this.getWidth()+1) / snapXCount;
			float snapH = (this.getHeight()+1) / snapYCount;

			return new Rectangle2d(
				((float) Math.floor(nX / snapW) * snapW) + this.getX(),
				((float) Math.floor(nY / snapH) * snapH) + this.getY(),
				snapW,
				snapH
			);
		}

		/**
		 * @param point The point to be snapped
		 * @return A rectangle of the region where the point should be snapped at and null if x and y are out of bounds
		 */
		public Rectangle2d snapRectangle (Point2d point) {
			return snapRectangle(point.getX(), point.getY());
		}

		/**
		 * @param x The x coordinate of the point to snap
		 * @param y The y coordinate of the point to snap
		 * @return The center of the region where the point is snapped at and null if x and y are out of bounds
		 */
		public Point2d snapPoint(float x, float y) {
			Rectangle2d center = snapRectangle(x, y);
			if (center == null) {
				return null;
			}
			return center.getCenter();
		}

		/**
		 * @param point The point to be snapped
		 * @return The center of the region where the point is snapped at and null if x and y are out of bounds
		 */
		public Point2d snapPoint(Point2d point) {
			return snapPoint(point.getX(), point.getY());
		}

		/**
		 * @param x The x coordinate of the point to snap
		 * @param y The y coordinate of the point to snap
		 * @return a XY object with the matching XY of the grid
		 */
		public XY snapInt(float x, float y) {
			if (!super.contains(x, y)) {
				return null;
			}

			float nX = x-this.getX();
			float nY = y-this.getY();

			float snapW = (this.getWidth()+1) / snapXCount;
			float snapH = (this.getHeight()+1) / snapYCount;

			int rX = (int) (nX / snapW);
			int rY = (int) (nY / snapH);

			return new XY(rX, rY);
		}

		/**
		 * @param point the point to snap
		 * @return a XY object with the matching XY point of the grid
		 */
		public XY snapInt(Point2d point) {
			return snapInt(point.getX(), point.getY());
		}

		/**
		 * Adds a change listener to the grid
		 * @param listener The listener object to add
		 * @return True on success, false otherwise
		 */
		public boolean addChangeListener(GridChangeListener listener) {
			return this.changeListeners.add(listener);
		}

		/**
		 * Removes a change listener from the grid
		 * @param listener The listener object to remove
		 * @return True on success, false otherwise
		 */
		public boolean removeChangeListener(GridChangeListener listener) {
			return this.changeListeners.remove(listener);
		}

		/**
		 * Removes all change listeners of the grid
		 */
		public void removeChangeListener() {
			this.changeListeners.clear();
		}

		/**
		 * Triggers all change events on the listeners
		 */
		private void triggerChange() {
			for (GridChangeListener listener : changeListeners) {
				listener.gridChanged(this);
			}
		}

		// Getters and Setters
		public int getSnapXCount() {
			return snapXCount;
		}

		/**
		 * @param snapXCount
		 * @return this SnapeGrid object
		 */
		public SnapGrid setSnapXCount(int snapXCount) {
			triggerChange();
			this.snapXCount = snapXCount;
			return this;
		}

		public int getSnapYCount() {
			return snapYCount;
		}

		public SnapGrid setSnapYCount(int snapYCount) {
			this.snapYCount = snapYCount;
			triggerChange();
			return this;
		}

		@Override
		public SnapGrid setX(float x) {
			super.setY(x);
			triggerChange();
			return this;
		}

		@Override
		public SnapGrid setY(float y) {
			super.setY(y);
			triggerChange();
			return this;
		}

		@Override
		public SnapGrid setWidth(float width) {
			super.setWidth(width);
			triggerChange();
			return this;
		}

		@Override
		public SnapGrid setHeight(float height) {
			super.setHeight(height);
			triggerChange();
			return this;
		}

}

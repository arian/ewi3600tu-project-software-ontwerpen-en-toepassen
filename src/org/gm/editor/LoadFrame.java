/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.ModalFrame;
import org.ResponseListener;

public class LoadFrame extends ModalFrame {

	private static final long serialVersionUID = -6828115248164419007L;
	protected final ResponseListener<Integer> respListener;

	/**
	 * The abstract listener class to catch selections on a JTable
	 */
	public abstract class SelectionListener implements ListSelectionListener {
		protected JTable table;

		/**
		 * Constructs a SelectionListener class
		 * @param table the JTable to attach this class to
		 */
		SelectionListener(JTable table) {
			this.table = table;
			table.getSelectionModel().addListSelectionListener(this);
			table.getColumnModel().getSelectionModel()
			    .addListSelectionListener(this);
		}

		@Override
		public abstract void valueChanged(ListSelectionEvent e);
	}

	/**
	 * Creates a popup frame with a response listener that notifies of the result of this frame
	 * @param respListener The listener for responses of this frame.
	 * @param addable True adds an extra result to the rowset, to catch new items with
	 */
	public LoadFrame(final ResponseListener<Integer> respListener, final boolean addable) {
		this(respListener, addable, "New");
	}

	/**
	 * Creates a popup frame with a response listener that notifies of the result of this frame
	 * @param respListener The listener for responses of this frame.
	 * @param addable True adds an extra result to the rowset, to catch new items with
	 * @param addableName sets the name of the addable row
	 */
	public LoadFrame(final ResponseListener<Integer> respListener, final boolean addable, final String addableName) {
		super("Level laden");
		final LoadFrame me = this;
		this.respListener = respListener;

		JPanel nPanel = new JPanel(new GridLayout(1,0));
		nPanel.setOpaque(true);
		this.setContentPane(nPanel);

		try {
			String query = "SELECT id, name FROM levels";
			DefaultTableModel tableData = org.db.Util.toTableModel(org.db.Connector.getInstance().query(query));

			if (addable) {
				Object[] newRow = {-1, addableName};
				tableData.addRow(newRow);
			}

			JTable table = new JTable(tableData);

			// Stackoverflow biedt altijd antwoorden
			// http://stackoverflow.com/questions/818287/changing-jtable-cell-color
			table.setBackground(new Color(0, 0, 0));
			table.setForeground(new Color(255, 255, 255));
			table.setGridColor(new Color(127, 127, 127));

			table.setPreferredScrollableViewportSize(new Dimension(500, 70));
			table.setFillsViewportHeight(true);
			table.getModel();

			new SelectionListener(table){

				@Override
				public void valueChanged(ListSelectionEvent e) {
					// If cell selection is enabled, both row and column change events are fired
					if (e.getSource() == table.getSelectionModel()
							&& table.getRowSelectionAllowed() && !e.getValueIsAdjusting()) {

						Object fIndex = table.getModel().getValueAt(e.getFirstIndex(), 0);
						int val;
						if (fIndex instanceof Integer) {
							val = (Integer) fIndex;
						} else {
							val = Integer.parseInt(fIndex.toString());
						}

						respListener.respond(val);
						me.setVisible(false);

					} else if (e.getSource() == table.getColumnModel().getSelectionModel()
							&& table.getColumnSelectionAllowed()){
						// TODO I don't think this does something right?
					}
				}
			};

			nPanel.add(new JScrollPane(table));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		this.setBounds(100, 100, 640, 480);
		this.pack();
		this.setVisible(true);
	}

}

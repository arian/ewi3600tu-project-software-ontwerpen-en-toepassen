/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.ModalFrame;
import org.ResponseListener;

public class NameDialog extends ModalFrame {

	private static final long serialVersionUID = -4273090633717291120L;
	private ResponseListener<String> respListener;

	public NameDialog(String startname, ResponseListener<String> respListener) {
		super("Give a name");
		final NameDialog me = this;
		this.respListener = respListener;


		final JLabel 		label = new JLabel("Please enter a name for the level:");
		final JTextField 	input = new JTextField();
		final JButton 		but = new JButton();

		but.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent ev) {
				me.respListener.respond(input.getText());
			}

		});
		input.setText(startname);

		label.setBounds(10, 10, 300, 60);
		input.setBounds(10, 70, 300, 60);
		but.setBounds(10, 130, 300, 60);

		this.getContentPane().add(label);
		this.getContentPane().add(input);
		this.getContentPane().add(but);

		this.setBounds(100, 100, 300, 300);
		this.pack();
		this.setVisible(true);

	}

}

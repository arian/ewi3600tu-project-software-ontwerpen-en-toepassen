/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm;

import java.util.HashMap;
import java.util.Map.Entry;

public class MazeStorageObjectOptions {

	public int type = 0;
	public int[] rotationVector = {0, 0, 1};
	public float rotation = 0;
	public HashMap<String, String> attributes;
	public int pickup = 0;

	/**
	 * Creates a MazeStorageObjectOptions object.
	 */
	public MazeStorageObjectOptions() {
		attributes = new HashMap<String, String>();
	}

	/**
	 * Creates a MazeStorageObjectOptions object, with a type
	 * @param type The type of object to create
	 */
	public MazeStorageObjectOptions(int type) {
		this();
		this.type = type;
	}

	/**
	 * Creates a MazeStorageObjectOptions object, with a type and rotation
	 * @param type Type to create the object with
	 * @param rotVector the vector to rotate around
	 * @param rotation the rotation around the rotation vector in radians
	 */
	public MazeStorageObjectOptions(int type, int[] rotVector, float rotation) {
		this(type);
		if (this.rotationVector.length >= 3) {
			this.rotationVector = rotVector;
			this.rotation = rotation;
		}
	}

	/**
	 * Creates a MazeStorageObjectOptions object from a serialized string
	 * @param serialized The object to create the options from
	 */
	public MazeStorageObjectOptions(String serialized) {
		// Removing starting and possibly ending parentheses
		if (serialized.charAt(0) == '(') {
			serialized = serialized.substring(1);
		}
		if (serialized.charAt(serialized.length() - 1) == ')') {
			serialized = serialized.substring(0, serialized.length()-1);
		}

		// Splitting the object info into a HashMap
		String[] splitUp = serialized.split(",");
		attributes = new HashMap<String, String>();
		for (int i = 0; i < splitUp.length; i++) {
			String[] pair = splitUp[i].split("=", 2);

			if (pair.length == 1) {
				attributes.put(pair[0], null);
			}
			else {
				attributes.put(pair[0], pair[1]);
			}
		}

		// Reading the HashMap
		if (attributes.containsKey("T")) {
			this.type = Integer.parseInt(attributes.get("T").trim());
		}
		if (attributes.containsKey("R")) {
			this.rotation = Float.parseFloat(attributes.get("R").trim());
		}
		if (attributes.containsKey("P")) {
			this.pickup = Integer.parseInt(attributes.get("P").trim());
		}

		if (attributes.containsKey("RV")) {
			String[] vO = attributes.get("RV").split(";");
			if (vO.length == 3) {
				this.rotationVector[0] = Integer.parseInt(vO[0].trim());
				this.rotationVector[1] = Integer.parseInt(vO[1].trim());
				this.rotationVector[2] = Integer.parseInt(vO[2].trim());
			}
		}

	}

	/**
	 * Serializes the object
	 * @return a string representing the object
	 */
	public String serialize() {

		HashMap<String, String> attrs = new HashMap<String, String>(attributes);
		attrs.put("T", type + "");
		attrs.put("RV", rotationVector[0] + ";" + rotationVector[1] + ";" + rotationVector[2]);
		attrs.put("R", (rotation % 360) + "");
		attrs.put("P", pickup + "");

		String ser = "";
		for (Entry<String, String> entry : attrs.entrySet()) {
			ser += "," + entry.getKey() + "=" + entry.getValue();
		}

		return "(" + ser.substring(1) + ")";
	}
}

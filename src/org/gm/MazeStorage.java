/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm;

import java.sql.ResultSet;
import java.util.ArrayList;

import org.db.Connector;


public abstract class MazeStorage {

	/**
	 * Gets an list of all maze id's
	 * @return An array list with the id's of all mazes
	 */
	public static ArrayList<Integer> listMazes() {
		try {
			Connector db = Connector.getInstance();
			ResultSet queryRes = db.query("SELECT id FROM levels");
			ArrayList<Integer> result = new ArrayList<Integer>();
			while (queryRes.next()) {
				result.add(queryRes.getInt(1));
			}
			queryRes.close(); // Free up resources

			return result;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Deserializes the maze using the MazeBuilder
	 * @param <T>
	 * @param dataString the maze to deserialize
	 * @param maze the MazeBuilder instance that creates the maze
	 * @return the newly created maze
	 * @throws MazeDataException
	 */
	public static <T> T deserializeMaze(String dataString, MazeBuilder<T> maze, String name) throws MazeDataException {
		maze.createWithSize(10, 10, 10);

		int x = 0;
		int y = 0;
		int z = 0;

		for (int i = 0; i < dataString.length(); i++) {
			switch (dataString.charAt(i)) {
				case 'x': x++; break;
				case 'y': y++; break;
				case 'z': z++; break;
				case 'u': x=0; break;
				case 'v': y=0; break;
				case 'w': z=0; break;
				case '(':
					int j = 0;
					for (; dataString.charAt(i+j) != ')'; j++) {};
					String oString = dataString.substring(i, i + j);
					MazeStorageObjectOptions options = new MazeStorageObjectOptions(oString);
					maze.setObjectAtLocation(x, y, z, options);
				case ' ': break;
				default: break;
			}
		}

		maze.setName("name");
		T res = maze.getResult();
		maze.close();
		return res;
	}

}

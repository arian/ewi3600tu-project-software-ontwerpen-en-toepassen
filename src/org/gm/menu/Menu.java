/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.menu;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;

import org.TextureManager;
import org.TunnelRunner;
import org.VisibleObject;
import org.VisibleObjectList;
import org.gm.BackgroundSound;
import org.gm.Button;
import org.gm.ButtonList;
import org.gm.Factory;
import org.gm.FactoryException;
import org.gm.FactoryOptions;
import org.gm.GameMode;
import org.shapes2d.Point2d;
import org.shapes2d.Tetragon;
import org.shapes2d.Text;
import org.sound.SoundPlayer;

public class Menu extends GameMode {

	// Screen size.
	private int screenWidth = 800, screenHeight = 600;
	private final VisibleObjectList<VisibleObject> visibleObjects = new VisibleObjectList<VisibleObject>();

	private Tetragon background;
	private ButtonList buttons;
	private Text title;

	private SoundPlayer soundPlayer;

	@Override
	public void initMode(GLAutoDrawable drawable, FactoryOptions options) {
		super.initMode(drawable, options);

		screenWidth = options.getOwner().getWidth();
		screenHeight = options.getOwner().getHeight();

		// Retrieve the OpenGL handle, this allows us to use OpenGL calls.
		GL gl = drawable.getGL();
		// Set the matrix mode to GL_PROJECTION, allowing us to manipulate the
		// projection matrix
		gl.glMatrixMode(GL.GL_PROJECTION);

		// Always reset the matrix before performing transformations, otherwise
		// those transformations will stack with previous transformations!
		gl.glLoadIdentity();
		/*
		 * glOrtho performs an "orthogonal projection" transformation on the
		 * active matrix. In this case, a simple 2D projection is performed,
		 * matching the viewing frustum to the screen size.
		 */
		gl.glOrtho(0, screenWidth, 0, screenHeight, -1, 1);

		// Set the matrix mode to GL_MODELVIEW, allowing us to manipulate the
		// model-view matrix.
		gl.glMatrixMode(GL.GL_MODELVIEW);

		// We leave the model view matrix as the identity matrix. As a result,
		// we view the world 'looking forward' from the origin.
		gl.glLoadIdentity();

		// We have a simple 2D application, so we do not need to check for depth
		// when rendering.
		gl.glDisable(GL.GL_DEPTH_TEST);
		TextureManager.getInstance(drawable.getGL()).setEnvMode(GL.GL_REPLACE);

		// re-attach events
		if (buttons != null) {
			buttons.attach();
		}

		if (TunnelRunner.soundEnabled) {
			BackgroundSound.getInstance().play("assets/sounds/starwars.mp3");
		}

		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
	}

	@Override
	public void initDisplay(GLAutoDrawable drawable, FactoryOptions options) {
		super.initDisplay(drawable, options);

		// Background
		background = Tetragon.rectangle(0, 0, screenWidth, screenHeight);
		background.setColor(1, 1, 1, 1f);
		background.setTexture("menu-background.jpg");
		visibleObjects.add(background);

		buttons = new ButtonList(options.getCanvas());
		visibleObjects.add(buttons);

		Button buttonBackground = new Button(100, 100, 100, 100);
		((Tetragon) buttonBackground.getShape()).setTexture("menu-buttonback.png");
		buttonBackground.setEventListener(new Button.EventShim() {

			@Override
			public void realign(Button button, GLCanvas canvas) {
				button.getShape().fit((canvas.getWidth() - 400) / 2, (canvas.getHeight() - 500) / 2, 400, 400);
			}

		}).realign(options.getCanvas());

		// Game button
		Button gameButton = new Button(100, 100, 100, 100);
		gameButton.setLabel("Game").getLabel()
			.setSize(30)
			.setColor(1f, 1f, 1f, 1f);
		gameButton.getShape().setColor(0.0f, 0.5f, 0f, 0f);
		gameButton.setEventListener(new Button.EventShim(){

			@Override
			public void mouseEnter(Button button) {
				button.getLabel().getColor().darken(0.3f);
			}

			@Override
			public void mouseLeave(Button button) {
				button.getLabel().getColor().lighten(0.3f);
			}

			@Override
			public void mouseClick(Button button) {
				try {
					Factory.getInstance().set("Game");
				} catch (FactoryException e) {
					e.printStackTrace();
				}
			}
			@Override
			public void realign(Button button, GLCanvas canvas) {
				button.getShape().fit(
						(canvas.getWidth() - 300) / 2,
						(canvas.getHeight() + 10) / 2,
						300, 50);
			}
		}).realign(options.getCanvas());

		// Editor button
		Button editorButton = new Button(200, 200, 100, 100);
		editorButton.setLabel("Editor").getLabel()
			.setSize(30)
			.setColor(1f, 1f, 1f, 1f);
		editorButton.getShape().setColor(0.0f, 0.5f, 0.5f, 0f);
		editorButton.setEventListener(new Button.EventShim(){

			@Override
			public void mouseEnter(Button button) {
				button.getLabel().getColor().darken(0.3f);
			}

			@Override
			public void mouseLeave(Button button) {
				button.getLabel().getColor().lighten(0.3f);
			}

			@Override
			public void mouseClick(Button button) {
				try {
					Factory.getInstance().set("Editor");
				} catch (FactoryException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void realign(Button button, GLCanvas canvas) {
				button.getShape().fit(
						(canvas.getWidth() - 300) / 2,
						(canvas.getHeight() - 110) / 2,
						300, 50);
			}
		}).realign(options.getCanvas());


		// Highscores button
		Button highscoresButton = new Button(100, 200, 200, 100, "Highscores");
		highscoresButton.realign(options.getCanvas());
		highscoresButton.getLabel()
			.setSize(30)
			.setColor(1f, 1f, 1f, 1f);
		highscoresButton.getShape()
			.setColor(0.0f, 0.5f, 1f, 0f);
		highscoresButton.setEventListener(new Button.EventShim(){

			@Override
			public void mouseEnter(Button button) {
				button.getLabel().getColor().darken(0.3f);
			}

			@Override
			public void mouseLeave(Button button) {
				button.getLabel().getColor().lighten(0.3f);
			}

			@Override
			public void mouseClick(Button button) {
				try {
					Factory.getInstance().set("Highscores");
				} catch (FactoryException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void realign(Button button, GLCanvas canvas) {
				button.getShape().fit((canvas.getWidth() - 300) / 2, (canvas.getHeight() - 230) / 2, 300, 50);
			}
		});
		highscoresButton.realign(options.getCanvas());

		// Close button
		Button exitButton = new Button(100, 200, 200, 100, "Quit");
		exitButton.getLabel()
			.setSize(30)
			.setColor(1f, 1f, 1f, 1f);
		exitButton.getShape()
			.setColor(0.0f, 0.5f, 1f, 0f);
		exitButton.setEventListener(new Button.EventShim(){

			@Override
			public void mouseEnter(Button button) {
				button.getLabel().getColor().darken(0.3f);
			}

			@Override
			public void mouseLeave(Button button) {
				button.getLabel().getColor().lighten(0.3f);
			}

			@Override
			public void mouseClick(Button button) {
				System.exit(0);
			}

			@Override
			public void realign(Button button, GLCanvas canvas) {
				button.getShape().fit((canvas.getWidth() - 300) / 2, (canvas.getHeight() - 360) / 2, 300, 50);
			}
		}).realign(options.getCanvas());

		buttons.addButton(buttonBackground);
		buttons.addButton(exitButton);
		buttons.addButton(gameButton);
		buttons.addButton(editorButton);
		buttons.addButton(highscoresButton);

		title = (Text) (new Text(
				new Point2d(
						options.getCanvas().getWidth()/2,
						options.getCanvas().getHeight()-100
				), "Tunnel Runner"))
			.setSize(40)
			.setColor(1, 1, 1);
		visibleObjects.add(title);
	}

	@Override
	public void deInitMode(GLAutoDrawable drawable, FactoryOptions options) {
		super.deInitMode(drawable, options);

		// Explicit destruct
		buttons.detach();

		if (soundPlayer != null) {
			soundPlayer.stopPlayer();
		}

	}

	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);

		GL gl = drawable.getGL();

		// Set the clear color and clear the screen.
		gl.glClearColor(0.95f, 0.95f, 0.95f, 1);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT);

		// Draw the buttons.
		// Display all the visible objects of MazeRunner.
		visibleObjects.display(gl, deltaTime);

		gl.glFlush();
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {

		GL gl = drawable.getGL();

		// Set the new screen size and adjusting the viewport
		screenWidth = width;
		screenHeight = height;
		gl.glViewport(0, 0, screenWidth, screenHeight);

		// Update the projection to an orthogonal projection using the new screen size
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(0, screenWidth, 0, screenHeight, -1, 1);

		if (title != null && buttons != null) {
			title.setPoint(0, new Point2d(
					width/2,
					height-100
			));
			buttons.realign();
		}

		if (background != null) {
			background.setPoints(new Point2d(0, 0), new Point2d(width, height));
		}

	}

}

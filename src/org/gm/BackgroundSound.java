package org.gm;

import org.sound.SoundPlayer;

public class BackgroundSound {

	protected String playing;
	protected SoundPlayer player;
	private static BackgroundSound instance;

	private BackgroundSound() {
	}

	public static BackgroundSound getInstance() {
		if (instance == null) {
			instance = new BackgroundSound();
		}
		return instance;
	}

	public void play(String file) {

		if (file.equals(playing)) {
			return;
		}

		this.stop();

		player = new SoundPlayer(file).setLoop(true).startPlayer();
		playing = file;

	}

	public void stop() {
		if (player != null) {
			player.stopPlayer();
		}
	}

}

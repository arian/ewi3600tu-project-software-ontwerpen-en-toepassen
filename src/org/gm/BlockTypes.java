/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm;

import javax.vecmath.AxisAngle4f;

import org.gm.BlockTypes.BlockType;
import org.gm.editor.Type;
import org.gm.editor.TypeList;
import org.shapes.holeobstacle.HoleObstacle;
import org.shapes.squarecorner.SquareCorner;
import org.shapes.squaretube.SquareTube;

/**
 * This class handles the various block types of the editor
 * TODO 2.0: get the data externally =)
 */
public class BlockTypes extends TypeList<BlockType> {

	public class BlockType extends Type {

		public String texture;
		public Class<?> className;
		public AxisAngle4f preRotation = new AxisAngle4f(0, 0, 1, 0);

		/**
		 * Creates a new BlockType
		 * @param code the code of the new BlockType
		 * @param texture the editor texture of the BlockType
		 * @param cls the game world class of the object
		 * @param prerotation the initial rotation of that object in the real world
		 */
		public BlockType(int code, String texture, Class<?> cls, AxisAngle4f prerotation) {
			super(code);
			this.texture = texture;
			this.preRotation = prerotation;
			this.className = cls;
		};

	}

	public static BlockTypes instance;

	/**
	 * Creates a new BlockTypes  object. And initializes all data associated
	 * with it.
	 */
	private BlockTypes() {
		super();

		float pi = (float) Math.PI;
		float pi2 = pi / 2;

		types.add(new BlockType(1, "editor-tube.png"     , SquareTube.class  , new AxisAngle4f(1, 0, 0, pi2)));
		types.add(new BlockType(2, "editor-corner.png"   , SquareCorner.class, new AxisAngle4f(0, 1, 0, pi2)));
		types.add(new BlockType(3, "editor-obstacle.png" , HoleObstacle.class, new AxisAngle4f(0, 0, 1, pi2)));
		types.add(new BlockType(4, "editor-up.png"       , SquareCorner.class, new AxisAngle4f(0, 1, 0, pi)));
		types.add(new BlockType(5, "editor-down.png"     , SquareCorner.class, new AxisAngle4f(0, 0, 1, 0)));
		types.add(new BlockType(6, "editor-tubeup.png"   , SquareTube.class,   new AxisAngle4f(0, 0, 1, 0)));
	}

	/**
	 * Gets the instance of BlockTypes. If none exists one will be created.
	 * @return the BlockTypes object
	 */
	public static BlockTypes getInstance() {
		if (instance == null) {
			instance = new BlockTypes();
		}
		return instance;
	}

}

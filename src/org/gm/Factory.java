/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm;

import java.util.HashMap;

import javax.media.opengl.GLAutoDrawable;

import org.gm.editor.Editor;
import org.gm.game.Game;
import org.gm.highscores.Highscores;
import org.gm.menu.Menu;

public class Factory {

	private GLAutoDrawable drawable;
	private final HashMap<String, GameMode> modes;
	private GameMode mode;
	private final FactoryOptions options;
	private static Factory instance;

	/**
	 * Singleton, createInstance only once
	 * @return the Factory instance
	 */
	public static Factory getInstance() {
		return instance;
	}

	/**
	 * Returns the Factory instance, and creates it if it doesn't exist yet.
	 * @param options
	 * @return the Factory instance
	 */
	public static Factory createInstance(FactoryOptions options) {
		if (instance == null) {
			instance = new Factory(options);
		}
		return instance;
	}

	private Factory(FactoryOptions options) {
		this.options = options;
		this.modes = new HashMap<String, GameMode>();

		this.mode = new GameMode(){
			@Override
			public void reshape(GLAutoDrawable drawable, int width, int height,
					int x, int y) {
			}
		};
	}

	public void setDrawable(GLAutoDrawable drawable) {
		this.drawable = drawable;
	}

	/**
	 * Set the game mode (state) to another mode.
	 * @param modeName
	 * @throws FactoryException
	 */
	public void set(String modeName) throws FactoryException {
		modeName = modeName.toLowerCase();
		GameMode _mode = modes.get(modeName);

		// de-initialize the current mode
		if (mode != null) {
			mode.deInitMode(drawable, options);
		}

		// There is a instance of this game mode already
		if (_mode != null) {
			_mode.initMode(drawable, options);
			mode = _mode;
			return;
		}

		// Create a new instance of mode
		if ("game".equals(modeName)) {
			_mode = new Game();
		} else if ("menu".equals(modeName)) {
			_mode = new Menu();
		} else if ("highscores".equals(modeName)) {
			_mode = new Highscores();
		} else if ("editor".equals(modeName)) {
			_mode = new Editor();
		} else {
			throw new FactoryException("Y U NO WANNA HAVE GOOD GAME MODEZ@!@@");
		}

		// Initialize
		_mode.initMode(drawable, options);

		// Save instance of this mode, so it can be reused.
		modes.put(modeName, _mode);
		mode = _mode;

	}

	/**
	 *
	 * @return the currently active GameMode
	 * @throws FactoryException
	 */
	public GameMode get() throws FactoryException {

		if (mode == null) {
			throw new FactoryException("No mode set yet");
		}

		return mode;

	}

	/**
	 * @return the FactoryOptions object for the Factory instance
	 */
	public FactoryOptions getOptions() {
		return options;
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm;

import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;

import org.TunnelRunner;
import org.shapes2d.FontManager;
import org.util.Events;
import org.util.Time;

public abstract class GameMode extends Events implements GLEventListener {

	protected boolean displayedInitialized = false;
	protected FactoryOptions initOptions = null;
	protected boolean active = false;
	protected boolean clearedFonts = false;

	// Used to calculate elapsed time.
	private long previousTime = Time.getMillis();
	protected int deltaTime;

	@Override
	public void display(GLAutoDrawable drawable) {

		// Calculating time since last frame.
		long currentTime = Time.getMillis();
		deltaTime = (int) (currentTime - previousTime);
		previousTime = currentTime;

		if (!displayedInitialized) {
			displayedInitialized = true;
			initDisplay(drawable, initOptions);
		}

	}

	/**
	 * initMode is called each time the mode is activated
	 * @param drawable
	 * @param options
	 */
	public void initMode(GLAutoDrawable drawable, FactoryOptions options) {
		initOptions = options;
		// Call the reshape method, in case the window was resized in the other
		// GameMode
		TunnelRunner owner = options.getOwner();
		this.reshape(drawable,
				owner.getX(), owner.getY(),
				owner.getWidth(), owner.getHeight());
		FontManager.getInstance().clearRenderers();
		active = true;
	}

	/**
	 * Called each time when the mode is deactivated
	 * @param drawable
	 * @param options
	 */
	public void deInitMode(GLAutoDrawable drawable, FactoryOptions options) {
		active = false;
	}

	/**
	 * @return the current GameMode class name
	 */
	public String getName(){
		return this.getClass().getName();
	}

	// Prevent that these methods have to be implemented in sub-classes

	/**
	 * initDisplay is called once on the first call of the display method
	 * @param options
	 * @param drawable
	 */
	public void initDisplay(GLAutoDrawable drawable, FactoryOptions options){
	}

	@Override
	public final void init(GLAutoDrawable drawable) {
	}

	@Override
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

}

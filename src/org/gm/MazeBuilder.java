/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm;


public interface MazeBuilder<T> {

	/**
	 * Creates an internal maze with the given sizes
	 * @param x the x size
	 * @param y the y size
	 * @param z the z size
	 */
	public void createWithSize(int x, int y, int z) throws MazeDataException;

	/**
	 * Sets an object at the specified location
	 * @param x the x position
	 * @param y the y position
	 * @param z the z position
	 * @param options The options for the object at the given location
	 */
	public void setObjectAtLocation(int x, int y, int z, MazeStorageObjectOptions options);

	/**
	 * Sets the name of the maze
	 * @param name the name of the maze to set
	 */
	public void setName(String name);

	/**
	 * Gets the just built maze
	 * @return The just built maze
	 */
	public T getResult();

	/**
	 * Allows to free up resources when needed
	 */
	public void close();

}

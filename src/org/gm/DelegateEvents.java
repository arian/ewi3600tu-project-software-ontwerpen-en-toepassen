/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm;

import javax.media.opengl.GLCanvas;

public interface DelegateEvents {

	/**
	 * Triggers local mouse events based on global mouseMoved information
	 * @param x The current global x position of the pointer
	 * @param y The current global y position of the pointer
	 * @return The current object (to make the events chainable)
	 */
	public DelegateEvents mouseMoved(int x, int y);

	/**
	 * Triggers local mouse events based on global mouseClicked information
	 * @param x The current global x position of the pointer
	 * @param y The current global y position of the pointer
	 * @return The current object (to make the events chainable)
	 */
	public DelegateEvents mouseClicked(int x, int y);

	/**
	 * Triggers a realign event based on the global realign event
	 * @return The current object (to make the events chainable)
	 */
	public DelegateEvents realign(GLCanvas canvas);

}

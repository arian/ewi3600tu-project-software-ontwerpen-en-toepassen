/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm;

public class MazeDataException extends Exception {

	private static final long serialVersionUID = -840294338069473177L;

	/**
	 * Constructs a MazeDataException with the given message
	 * @param message The message of the current exception
	 */
	public MazeDataException(String message) {
		super(message);
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm;

import java.util.HashMap;

import javax.media.opengl.GLCanvas;

import org.TunnelRunner;

/**
 * Simple class to pass data through other game modes, for example the level ID
 * or the time in which the player finished the level.
 */
public class FactoryOptions extends HashMap<String, Object> {

	private static final long serialVersionUID = -2936331456514655559L;

	private GLCanvas canvas;
	private TunnelRunner owner;

	public GLCanvas getCanvas() {
		return canvas;
	}

	public void setCanvas(GLCanvas canvas) {
		this.canvas = canvas;
	}

	public TunnelRunner getOwner() {
		return owner;
	}

	public void setOwner(TunnelRunner owner) {
		this.owner = owner;
	}

}

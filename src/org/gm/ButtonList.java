/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Iterator;

import javax.media.opengl.GL;
import javax.media.opengl.GLCanvas;

import org.VisibleObject;
import org.VisibleObjectList;
import org.util.Attachable;

public class ButtonList implements VisibleObject, MouseListener, MouseMotionListener, Iterable<Button>, Attachable {

	private final VisibleObjectList<Button> buttons = new VisibleObjectList<Button>();
	private final GLCanvas canvas;

	/**
	 * Creates a ButtonList on the canvas, and let them listen to mouse events
	 * @param canvas
	 */
	public ButtonList(GLCanvas canvas) {
		this.canvas = canvas;
		attach();
	}

	/**
	 * add  the event handlers of this object
	 */
	@Override
	public void attach() {
		canvas.addMouseListener(this);
		canvas.addMouseMotionListener(this);
	}

	/**
	 * Removes the event handlers from this object
	 */
	@Override
	public void detach() {
		canvas.removeMouseListener(this);
		canvas.removeMouseMotionListener(this);
	}

	/**
	 * Adds a button to the list
	 * @param button The button to add
	 * @return The index of the just added button
	 */
	public int addButton(Button button) {
		buttons.add(button);
		return buttons.indexOf(button);
	}

	/**
	 * Removes a button from the list
	 * @param button The button to remove
	 * @return true on success otherwise false
	 */
	public boolean removeButton(Button button) {
		return buttons.remove(button);
	}

	/**
	 * Removes a button from the list
	 * @param index The index of the button to remove
	 * @return The button that was removed from the list
	 */
	public Button removeButton(int index) {
		return buttons.remove(index);
	}

	/**
	 * Removes all buttons from the list
	 */
	public void clear() {
		buttons.clear();
	}

	/**
	 * Triggers a realign event at all buttons
	 */
	public void realign() {
		for (DelegateEvents but : buttons) {
			but.realign(this.canvas);
		}
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		int x = event.getX();
		// invert y axis to match draw coordinates
		int y = canvas.getHeight() - event.getY();

		for (DelegateEvents but : buttons) {
			but.mouseClicked(x, y);
		}
	}

	@Override
	public void mouseMoved(MouseEvent event) {
		int x = event.getX();
		// invert y axis to match draw coordinates
		int y = canvas.getHeight() - event.getY();

		for (DelegateEvents but : buttons) {
			but.mouseMoved(x, y);
		}
	}

	@Override
	public void display(GL gl, int dt) {
		buttons.display(gl, dt);
	}

	@Override
	public Iterator<Button> iterator() {
		return buttons.iterator();
	}

	@Override
	public void mouseEntered(MouseEvent event) {}

	@Override
	public void mouseExited(MouseEvent event) {}

	@Override
	public void mousePressed(MouseEvent event) {}

	@Override
	public void mouseReleased(MouseEvent event) {}

	@Override
	public void mouseDragged(MouseEvent event) {}

}

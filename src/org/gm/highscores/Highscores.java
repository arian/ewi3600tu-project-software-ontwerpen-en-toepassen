/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.gm.highscores;

import java.awt.Canvas;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;

import org.ResponseListener;
import org.TextureManager;
import org.TunnelRunner;
import org.VisibleObject;
import org.VisibleObjectList;
import org.db.Connector;
import org.gm.BackgroundSound;
import org.gm.Button;
import org.gm.ButtonList;
import org.gm.Factory;
import org.gm.FactoryException;
import org.gm.FactoryOptions;
import org.gm.GameMode;
import org.gm.editor.LoadFrame;
import org.shapes2d.FontManager;
import org.shapes2d.Point2d;
import org.shapes2d.Tetragon;
import org.shapes2d.Text;
import org.sound.SoundPlayer;
import org.util.ScreenPixels;
import org.util.Time;

public class Highscores extends GameMode implements KeyListener {

	// Screen size.
	private int screenWidth = 800, screenHeight = 600;
	private final VisibleObjectList<VisibleObject> visibleObjects = new VisibleObjectList<VisibleObject>();

	private Tetragon background;
	private ButtonList buttons;

	private Text title;
	private Text level;
	private Text newRowText;
	private String newRowFormat;
	private String newNameString = "";

	private int time = -1;
	private int levelID;

	private VisibleObjectList<VisibleObject> list = new VisibleObjectList<VisibleObject>();
	private Button mBut;
	private Connector db;

	private SoundPlayer soundPlayer;
	private Canvas canvas;

	private ScreenPixels sp;
	private int doClearFonts;

	public Highscores() {
		try {
			db = Connector.getInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initMode(GLAutoDrawable drawable, FactoryOptions options) {
		super.initMode(drawable, options);

		canvas = options.getCanvas();
		canvas.addKeyListener(this);

		// set ScreenPixels for easy y-setting (i.e.: insert y-value from top of
		// screen instead of from the bottom);
		sp = new ScreenPixels(options);

		screenWidth = options.getOwner().getWidth();
		screenHeight = options.getOwner().getHeight();


		// Retrieve the OpenGL handle, this allows us to use OpenGL calls.
		GL gl = drawable.getGL();
		// Set the matrix mode to GL_PROJECTION, allowing us to manipulate the
		// projection matrix
		gl.glMatrixMode(GL.GL_PROJECTION);

		// Always reset the matrix before performing transformations, otherwise
		// those transformations will stack with previous transformations!
		gl.glLoadIdentity();

		/*
		 * glOrtho performs an "orthogonal projection" transformation on the
		 * active matrix. In this case, a simple 2D projection is performed,
		 * matching the viewing frustum to the screen size.
		 */
		gl.glOrtho(0, screenWidth, 0, screenHeight, -1, 1);

		// Set the matrix mode to GL_MODELVIEW, allowing us to manipulate the
		// model-view matrix.
		gl.glMatrixMode(GL.GL_MODELVIEW);

		// We leave the model view matrix as the identity matrix. As a result,
		// we view the world 'looking forward' from the origin.
		gl.glLoadIdentity();

		// We have a simple 2D application, so we do not need to check for depth
		// when rendering.
		gl.glDisable(GL.GL_DEPTH_TEST);
		TextureManager.getInstance(drawable.getGL()).setEnvMode(GL.GL_REPLACE);

		// re-attach events
		if (buttons != null) {
			buttons.attach();
		}


		// Background sound
		if (TunnelRunner.soundEnabled) {
			BackgroundSound.getInstance().play("assets/sounds/starwars.mp3");
		}

		// Time of previous game
		time = -1;
		if (options.get("trackTime") != null) {
			try {
				time = Integer.parseInt(options.get("trackTime").toString());
			} catch (NumberFormatException nfe) { }
		}

		// refresh when the new highscore entry was inserted
		boolean refresh = new Boolean(true).equals(options.get("refresh"));
		if (refresh) {
			options.remove("refresh");
			time = -1;
		}

		// Select level. Always ask which level, except when we just played
		// the game.
		if (time >= 0 || refresh) {
			Object lvl = options.get("level");
			if (lvl != null && lvl instanceof Integer) {
				levelID = (Integer) lvl;
			}
		}

		// ask for the level ID
		if (levelID == 0) {
			new LoadFrame(new ResponseListener<Integer>() {

				@Override
				public void respond(Integer res) {
					levelID = res;
				}

			}, true);
		}


		// Select the highscores of this level
		ResultSet results;
		if (db != null) {
			try {

				int row = 0;
				results = db.query("SELECT name, time " +
									"FROM highscores " +
									"WHERE level_id = ?" +
									"ORDER BY time " +
									"LIMIT 6",
									new Object[]{levelID});

				boolean hasNext;

				while ((hasNext = results.next()) || (newRowText == null && time >= 0)) {

					int rowTime = Integer.MAX_VALUE;
					if (hasNext) {
						rowTime = results.getInt(2);
					}

					if (newRowText == null && time >= 0 && time < rowTime) {
						// this is new inserted row
						newRowFormat = (row + 1) + ". %s (" + Time.formatLong(time) + ")";
						newRowText = new Text(new Point2d(
								options.getCanvas().getWidth() / 2,
								sp.yFromTop(180 + (40 * row++)) ),
								String.format(newRowFormat, "..."));
						newRowText.setColor(0, 1, 0);
						list.add(newRowText);

						// Use previously entered name
						if (!newNameString.isEmpty()) {
							newRowText.setText(String.format(newRowFormat, newNameString));
						}

					}

					if (hasNext) {
						// row from the database
						String formattedTime = Time.formatLong(rowTime);
						Text scores = new Text(new Point2d(
								options.getCanvas().getWidth() / 2,
								sp.yFromTop(180 + (40 * row++)) ),
								row + ". " + results.getString(1) + " (" + formattedTime + ")");
						scores.setColor(1, 1, 1);
						list.add(scores);
					}

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		visibleObjects.add(list);

		if (level != null) {
			level.setText("Level " + levelID);
		}

	}

	@Override
	public void initDisplay(GLAutoDrawable drawable, FactoryOptions options) {
		super.initDisplay(drawable, options);

		doClearFonts = 5;

		// Background
		background = Tetragon.rectangle(0, 0, screenWidth, screenHeight);
		background.setColor(1, 1, 1, 1f);
		background.setTexture("menu-background.jpg");
		visibleObjects.add(background);

		// Title
		title = new Text(new Point2d(
				options.getCanvas().getWidth() / 2,
				sp.yFromTop(100) ),
				"Highscores");
		title.setColor(1, 1, 1);
		visibleObjects.add(title);

		// Subtitle, being Level x
		level = new Text(new Point2d(
				options.getCanvas().getWidth() / 2,
				sp.yFromTop(130)),
				"Level " + levelID);
		level.setColor(1, 0, 0);
		visibleObjects.add(level);

		// Scores
		visibleObjects.add(list);

		// Buttons
		buttons = new ButtonList(options.getCanvas());
		visibleObjects.add(buttons);

		mBut = new Button(0, 0, 100, 100, "Menu");
		mBut.getLabel().setColor(1f, 1f, 1f, 1f);
		mBut.setEventListener(new Button.EventShim() {

			@Override
			public void mouseEnter(Button button) {
				((Tetragon) button.getShape()).setTexture("button-active.png");
			}

			@Override
			public void mouseLeave(Button button) {
				((Tetragon) button.getShape()).setTexture("button-inactive.png");
			}

			@Override
			public void mouseClick(Button button) {
				try {
					Factory.getInstance().set("Menu");
				} catch (FactoryException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void realign(Button button, GLCanvas canvas) {
				int mButWidth = 180;
				int mButHeight = 40;

				button.getShape().fit(
						(canvas.getWidth() / 2) - (mButWidth / 2),
						150, mButWidth, mButHeight);
			}

		} );
		mBut.triggerInit(this.initOptions.getCanvas());
		buttons.addButton(mBut);
	}

	@Override
	public void deInitMode(GLAutoDrawable drawable, FactoryOptions options) {
		super.deInitMode(drawable, options);

		// Explicit destruct
		buttons.detach();

		if (soundPlayer != null) {
			soundPlayer.stopPlayer();
		}

		levelID = 0;

		newRowText = null;
		list.clear();
		visibleObjects.remove(list);
		options.getCanvas().removeKeyListener(this);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);

		GL gl = drawable.getGL();

		// Set the clear color and clear the screen.
		gl.glClearColor(0.95f, 0.95f, 0.95f, 1);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT);

		// Display all the visible objects.
		visibleObjects.display(gl, deltaTime);

		if (doClearFonts > 0 && --doClearFonts == 0) {
			FontManager.getInstance().clearRenderers();
		}

		gl.glFlush();
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {

		GL gl = drawable.getGL();

		// Set the new screen size and adjusting the viewport
		screenWidth = width;
		screenHeight = height;
		gl.glViewport(0, 0, screenWidth, screenHeight);

		// Update the projection to an orthogonal projection using the new screen size
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(0, screenWidth, 0, screenHeight, -1, 1);

		if (title != null) {
			title.setPoint(0, new Point2d(width / 2, sp.yFromTop(100) ));
		}

		if (level != null) {
			level.setPoint(0, new Point2d(width / 2, sp.yFromTop(130) ));
		}

		if (buttons != null) {
			buttons.realign();
		}

		if (background != null) {
			background.setPoints(new Point2d(0, 0), new Point2d(width, height));
		}

	}

	private void saveNewHighscore() {
		if (db != null) {
			boolean success = db.noResultQuery("INSERT INTO highscores " +
					"(name, time, level_id) " +
					"VALUES(?, ?, ?)", new Object[]{newNameString, time, levelID});
			if (success) {
				try {
					// refresh highscores screen
					Factory gmFactory = Factory.getInstance();
					gmFactory.getOptions().put("trackTime", null);
					gmFactory.getOptions().put("refresh", true);
					gmFactory.set("highscores");
				} catch (FactoryException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (newRowText != null) {
			switch (e.getKeyCode()) {
				case KeyEvent.VK_BACK_SPACE:
					if (newNameString.length() > 0) {
						newNameString = newNameString.substring(0, newNameString.length() - 1);
						newRowText.setText(String.format(newRowFormat, newNameString));
					}
					break;
				case KeyEvent.VK_ENTER:
					saveNewHighscore();
					break;
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (newRowText != null) {
			// keyTyped returns the actual keys, so shift+a => A
			char c = e.getKeyChar();
			if (c >= 0x20 && c < 0x7F) { // 0x20 is 'space', characters in ASCII-table upto 'del' are allowed
				if (newNameString.length() < 20) { // Luuk voted for max. of 6 characters, but got veto'd
					newNameString += c;
					newRowText.setText(String.format(newRowFormat, newNameString));
				}
			}
		}
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.swing.JFrame;

import org.gm.Factory;
import org.gm.FactoryException;
import org.gm.FactoryOptions;
import org.shapes2d.Text;

import com.sun.opengl.util.Animator;

public class TunnelRunner extends JFrame implements GLEventListener {

	static final long serialVersionUID = 7526471155622776147L;

	public static boolean soundEnabled = true;
	public static boolean resetDB = false;

	private GLCanvas canvas;
	private org.gm.Factory gameModeFactory;

	// Screen size.
	public int screenWidth = 900;
	public int screenHeight = 800;

	public String defaultGameMode;
	public HashMap<String, String> arguments;

	/**
	 * Initializes the complete MazeRunner game.
	 * <p>
	 * MazeRunner extends Java AWT Frame, to function as the window. It creates
	 * a canvas on  itself where JOGL will be able to paint the OpenGL graphics.
	 * It then initializes all game components and initializes JOGL, giving it
	 * the proper settings to accurately display MazeRunner. Finally, it adds
	 * itself as the OpenGL event listener, to be able to function as the view
	 * controller.
	 */

	public TunnelRunner() {
		this("menu", new HashMap<String, String>());
	}

	public TunnelRunner(String mode, HashMap<String, String> opts) {
		// Make a new window.
		super("Tunnel Runner");

		// overwrite default mode
		defaultGameMode = mode;
		arguments = opts;

		// Let's change the window to our liking.
		setBounds(100, 100, screenWidth, screenHeight);
		setBackground(Color.white);

		// set the defaultfont.
		Text.defaultFont = "space age.ttf";

		// The window also has to close when we want to.
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		ModalFrame.setOwnerFrame(this);

		// First, we set up JOGL. We start with the default settings.
		GLCapabilities caps = new GLCapabilities();
		// Then we make sure that JOGL is hardware accelerated and uses double buffering.
		caps.setDoubleBuffered(true);
		caps.setHardwareAccelerated(true);

		// Now we add the canvas, where OpenGL will actually draw for us. We'll use settings we've just defined.
		canvas = new GLCanvas(caps);
		add(canvas);
		/* We need to add a GLEventListener to interpret OpenGL events for us. Since MazeRunner implements
		 * GLEventListener, this means that we add the necesary init(), display(), displayChanged() and reshape()
		 * methods to this class.
		 * These will be called when we are ready to perform the OpenGL phases of MazeRunner.
		 */
		canvas.addGLEventListener(this);

		/* We need to create an internal thread that instructs OpenGL to continuously repaint itself.
		 * The Animator class handles that for JOGL.
		 */
		Animator anim = new Animator(canvas);
		anim.start();

		// Set the frame to visible. This automatically calls upon OpenGL to prevent a blank screen.
		setVisible(true);

	}

	// All the methods implemented by GLEventListener are propagated to the
	// currently active GameMode.

	/**
	 * init(GLAutodrawable) is called to initialize the OpenGL context, giving
	 * it the proper parameters for viewing.
	 *
	 * <p>Implemented through GLEventListener.<p>
	 *
	 * It is <b>very important</b> to realize that there should be no drawing
	 * at all in this method.
	 */
	@Override
	public void init(GLAutoDrawable drawable) {

		// http://wiki.tankaar.com/index.php?title=Displaying_an_Image_in_JOGL_(Part_1)
		// http://www.gamedev.net/page/resources/_/technical/opengl/opengl-texture-mapping-an-introduction-r947
		// http://www.flipcode.com/archives/Advanced_OpenGL_Texture_Mapping.shtml

		// Set game mode
		FactoryOptions options = new FactoryOptions();
		options.setCanvas(canvas);
		options.setOwner(this);
		for (Entry<String, String> arg : arguments.entrySet()) {
			options.put(arg.getKey(), arg.getValue());
		}

		gameModeFactory = Factory.createInstance(options);
		gameModeFactory.setDrawable(drawable);
		try {
			gameModeFactory.set(defaultGameMode);
		} catch (FactoryException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}

	/**
	 * display(GLAutoDrawable) is called upon whenever OpenGL is ready to draw
	 * a new frame and handles all of the drawing.
	 *
	 * <p>Implemented through GLEventListener.</p>
	 */
	@Override
	public void display(GLAutoDrawable drawable) {
		try {
			this.gameModeFactory.get().display(drawable);
		} catch (FactoryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * displayChanged(GLAutoDrawable, boolean, boolean) is called upon whenever
	 * the display mode changes.
	 *
	 * <p>Implemented through GLEventListener.</p>
	 */
	@Override
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
		try {
			this.gameModeFactory.get().displayChanged(drawable, modeChanged, deviceChanged);
		} catch (FactoryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * reshape(GLAutoDrawable, int, int, int, int, int) is called upon whenever
	 * the viewport changes shape, to update the viewport setting accordingly.
	 * <p>
	 * Implemented through GLEventListener.
	 * This mainly happens when the window changes size, thus changing the
	 * canvas (and the viewport that OpenGL associates with it). It adjust the
	 * projection matrix to accommodate the new shape.
	 * </p>
	 */
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		try {
			this.gameModeFactory.get().reshape(drawable, x, y, width, height);
		} catch (FactoryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Program entry point
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		String createMode = "Menu";
		HashMap<String, String> opts = new HashMap<String, String>();

		int length = args.length;
		for (int i = 0; i < length; i++) {

			if ("--start".equals(args[i]) && ((i + 1) < length)) {
				createMode = args[++i];
				System.out.println("Starting in gamemode: " + args[i]);
				continue;
			}

			if ("--no-sound".equals(args[i])) {
				TunnelRunner.soundEnabled = false;
				continue;
			}

			if ("--reset-db".equals(args[i])) {
				TunnelRunner.resetDB = true;
				continue;
			}

			// Gets the other arguments
			if (args[i].startsWith("--")) {
				if ((i + 1) < length) {
					if (!args[i + 1].startsWith("--")) {
						opts.put(args[i].substring(2), args[++i]);
					} else {
						// a truthy value
						opts.put(args[i].substring(2), "1");
					}
				}
				continue;
			}

		}

		// Create and run MazeRunner.
		new TunnelRunner(createMode, opts);
	}

}

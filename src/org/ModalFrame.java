/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org;

import java.awt.Frame;

import javax.swing.JDialog;

public class ModalFrame extends JDialog {

	private static final long serialVersionUID = 151733363307492108L;

	public ModalFrame(String title) {
		super(mainWindow, title, true);
	}

	private static Frame mainWindow;

	public static void setOwnerFrame(Frame frame) {
		mainWindow = frame;
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.util;

/**
 * Math helpers
 */
public class MathUtil {

	public static double randomBetween(double low, double up) {
		return Math.random() * (up - low) + low;
	}

	public static float randomBetween(float[] bounds) {
		return (float) randomBetween(bounds[0], bounds[1]);
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.util;

/**
 * Attachable is an interface for classes that can add and remove events to
 * some window or canvas. When switching to other game modes or destructing
 * instances it's important to remove events (detach).
 */
public interface Attachable {

	public void attach();
	public void detach();

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.util;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Simple Events class
 */
public class Events {

	/**
	 * Interface for the listeners. This will make sure every listener has a
	 * update method that accepts an Event object.
	 */
	public interface Listener {
		public void update(Event evt);
	}

	/**
	 * Event class, will be passed to the Listener::update methods
	 */
	public class Event {
		public HashMap<String,Object> data = new HashMap<String,Object>();
		public void onAdd() {}
	};

	private final ArrayList<Listener> listeners = new ArrayList<Listener>();

	/**
	 * Add a event listener, usually something like events.addEventListener(this);
	 * @param listener
	 * @return this object
	 */
	public Events addEventListener(Listener listener) {
		listeners.add(listener);
		return this;
	}

	/**
	 * Removes the listener
	 * @param listener
	 * @return this object
	 */
	public Events removeEventListener(Listener listener) {
		listeners.remove(listener);
		return this;
	}

	/**
	 * Fires a event. It's often something like:
	 *
	 * <pre>
	 * this.fireEvent(new Event() {
	 *     public void onAdd() {
	 *         this.data.put("some", "thing");
	 *     }
	 * });
	 * </pre>
	 *
	 * @param evt
	 * @return this object
	 */
	public Events fireEvent(Event evt) {
		evt.onAdd();
		for (Listener listener : listeners) {
			listener.update(evt);
		}
		return this;
	}

}

/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/

package org.util;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class Time {

	/**
	 * @return the current time in milliseconds since the UNIX Epoch
	 */
	public static long getMillis() {
		return Calendar.getInstance().getTimeInMillis();
	}

	/**
	 * Formats the time as hh:mm:ss
	 * @param ms
	 * @return a formatted string representing the time
	 */
	public static String format(long ms) {
		long seconds = TimeUnit.MILLISECONDS.toSeconds(ms) % 60;
		long minutes = TimeUnit.MILLISECONDS.toMinutes(ms) % 60;
		long hours   = TimeUnit.MILLISECONDS.toHours(ms);
		if (hours > 0) {
			// lets hope this doesn't happen :)
			return String.format("%02d:%02d:%02d", hours, minutes, seconds);
		} else {
			return String.format("%02d:%02d", minutes, seconds);
		}
	}

	/**
	 * Formats the time as hh:mm:ss:hh (last hh are hundreds of seconds
	 * @param ms
	 * @return a formatted string representing the time
	 */
	public static String formatLong(long ms) {
		long seconds = TimeUnit.MILLISECONDS.toSeconds(ms) % 60;
		long minutes = TimeUnit.MILLISECONDS.toMinutes(ms) % 60;
		long hours   = TimeUnit.MILLISECONDS.toHours(ms);
		if (hours > 0) {
			// lets hope this doesn't happen :)
			return String.format("%02d:%02d:%02d", hours, minutes, seconds);
		} else {
			long hundreds = (int) (TimeUnit.MILLISECONDS.toMillis(ms) % 1000 / 10 + 0.5);
			return String.format("%02d:%02d:%02d", minutes, seconds, hundreds);
		}
	}

}

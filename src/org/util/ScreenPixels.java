/*
*  _____ ____  ____  _     ____  ____ ____ftw.
* /  __//  __\/  _ \/ \ /\/  __\/  _ \\__  \
* | |  _|  \/|| / \|| | |||  \/|| / \|  /  |
* | |_//|    /| \_/|| \_/||  __/| \_/| _\  |
* \____\\_/\_\\____/\____/\_/   \____//____/
*/
package org.util;

import java.awt.Canvas;

import org.gm.FactoryOptions;

/**
 * utility class which helps to position items relative to the top of the screen
 * instead of from the bottom.
 */
public class ScreenPixels {

	private Canvas canvas;
	private int height;

	private int yB;
	private int yT;

	public ScreenPixels(FactoryOptions options) {
		canvas = options.getCanvas();
	}

	public int yFromTop(int yBottom) {
		yB = yBottom;
		height = canvas.getHeight();

		yT = height - yB;

		return yT;
	}
}

package org.gm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.gm.editor.EditorObject;
import org.gm.editor.MazeData;
import org.junit.Test;

public class MazeStorageTest {

	@Test
	public void testDeserialize() {

		String dataString = "uvw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw(T=2,RV=0;0;1,P=0,R=90.0)z z z z z z z z z " +
				"zyw(T=1,RV=0;0;1,P=2,R=90.0)z z z z z z z z z " +
				"zyw(T=1,RV=0;0;1,P=0,R=90.0)z z z z z z z z z " +
				"zyw(T=2,RV=0;0;1,P=0,R=0.0)z z z z z z z z z " +
				"zyxvw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw(T=1,RV=0;0;1,P=0,R=0.0)z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw(T=1,RV=0;0;1,P=1,R=0.0)z z z z z z z z z " +
				"zyxvw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw(T=1,RV=0;0;1,P=0,R=0.0)z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw(T=1,RV=0;0;1,P=0,R=0.0)z z z z z z z z z " +
				"zyxvw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw(T=2,RV=0;0;1,P=0,R=180.0)z z z z z z z z z " +
				"zyw(T=1,RV=0;0;1,P=0,R=90.0)z z z z z z z z z " +
				"zyw(T=1,RV=0;0;1,P=12,R=90.0)z z z z z z z z z " +
				"zyw(T=2,RV=0;0;1,P=0,R=270.0)z z z z z z z z z " +
				"zyxvw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyxvw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyxvw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyxvw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyxvw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyxvw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyw z z z z z z z z z " +
				"zyx";

		try {

			MazeData data = MazeStorage.deserializeMaze(dataString, new MazeBuilder<MazeData>() {

				MazeData maze;
				@Override
				public void createWithSize(int x, int y, int z) throws MazeDataException {
					maze = new MazeData(x, y, z);
				}

				@Override
				public void setObjectAtLocation(int x, int y, int z, MazeStorageObjectOptions opts) {
					maze.setLocation(x, y, z, EditorObject.createFromCode(opts));
				}

				@Override
				public void setName(String name) {
				}

				@Override
				public MazeData getResult() {
					return maze;
				}

				@Override
				public void close() {}

			}, "");

			assertEquals(10, data.getXSize());
			assertEquals(10, data.getYSize());
			assertEquals(10, data.getZSize());

			assertNotNull(data.getLocation(0,6,0));
			assertNotNull(data.getLocation(0,7,0));
			assertNotNull(data.getLocation(0,8,0));
			assertNotNull(data.getLocation(0,9,0));
			assertNotNull(data.getLocation(1,6,0));
			assertNotNull(data.getLocation(1,9,0));
			assertNotNull(data.getLocation(2,6,0));
			assertNotNull(data.getLocation(2,9,0));
			assertNotNull(data.getLocation(3,6,0));
			assertNotNull(data.getLocation(3,7,0));
			assertNotNull(data.getLocation(3,8,0));
			assertNotNull(data.getLocation(3,9,0));

			assertNull(data.getLocation(3,9,1));

			MazeStorageObjectOptions options1 = data.getLocation(0,7,0).getOptions();
			MazeStorageObjectOptions options2 = data.getLocation(1,9,0).getOptions();
			MazeStorageObjectOptions options3 = data.getLocation(3,8,0).getOptions();

			assertEquals( 2, options1.pickup);
			assertEquals( 1, options2.pickup);
			assertEquals(12, options3.pickup);

		} catch (MazeDataException e) {
			fail("Exception: " + e.getMessage());
		}

	}

}
